package com.javiersolercanto.batoimobile.model;

import java.io.Serializable;

public class Belong implements Serializable {

    private int idgroup;
    private int iduser;
    private boolean confirmed;

    public Belong(int idgroup, int iduser, boolean confirmed) {
        this.idgroup = idgroup;
        this.iduser = iduser;
        this.confirmed = confirmed;
    }

    public int getIdgroup() {
        return idgroup;
    }

    public void setIdgroup(int idgroup) {
        this.idgroup = idgroup;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }
}
