package com.javiersolercanto.batoimobile.ui.marketplace;

import android.os.Build;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PostHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PutHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.category.GetCategoryHttpDataTask;
import com.javiersolercanto.batoimobile.model.Category;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Product;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class NewProductActivity extends AppCompatActivity {

    private TextView tvHead;
    private EditText etName;
    private EditText etDescription;
    private EditText etPrice;
    private Spinner spCategory;
    private Button btnAccept;
    private Button btnCancel;

    private static boolean mode;
    private Product product;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_product);

        // Miramos si se trata de edición o inserción
        if (getIntent() != null) mode = (product = (Product) getIntent().getSerializableExtra("product")) != null;
        else mode = false;

        setUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setUI() {
        tvHead = findViewById(R.id.tvHead);
        etName = findViewById(R.id.etName);
        etDescription = findViewById(R.id.etDescription);
        etPrice = findViewById(R.id.etPrice);
        spCategory = findViewById(R.id.spCategory);
        btnAccept = findViewById(R.id.btnAccept);
        btnCancel = findViewById(R.id.btnCancel);

        loadSpinner();

        if (mode) fillData();

        btnAccept.setOnClickListener(v -> addProduct());
        btnCancel.setOnClickListener(v -> finish());
    }

    private void fillData() {
        tvHead.setText(R.string.edit_product);
        etName.setText(product.getName());
        etDescription.setText(product.getDescription());
        etPrice.setText(String.valueOf(product.getPrice()));
        spCategory = findViewById(R.id.spCategory);
    }

    /**
     * Cargamos todas las categorias disponibles en la base de datos
     */
    private void loadSpinner() {
        GetCategoryHttpDataTask getCategoryHttpDataTask = new GetCategoryHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    if (!output.isEmpty() && output.get(0) != null) {
                        ArrayAdapter<Category> spinnerAdapter = new ArrayAdapter<Category>(getApplicationContext(), android.R.layout.simple_list_item_1, (ArrayList<Category>) output);
                        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spCategory.setAdapter(spinnerAdapter);

                        if (product != null) {
                            for (int i = 0; i < output.size(); i++ ) {
                                if (product.getCategory().getName().equals(((Category) output.get(i)).getName())) {
                                    spCategory.setSelection(i);
                                }
                            }
                        }
                    }
                }
            }
        });
        try {
            getCategoryHttpDataTask.execute(new URL(ConnectionDB.url + "category/"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Dependiendo del modo insertaremos o editaremos el producto
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void addProduct() {
        if (checkFields()) {
            if (!mode) {
                PostHttpDataTask postHttpDataTask = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(Integer output) {
                        if (output != -1) finish();
                        else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                    }
                });
                try {
                    postHttpDataTask.execute(new URL(ConnectionDB.url + "product/" + etName.getText().toString() + "/" + etPrice.getText().toString() + "/" + etDescription.getText().toString() + "/" + ((Category) spCategory.getSelectedItem()).getIdcategory() + "/" + CurrentUser.getCurrentUser().getIduser()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                PutHttpDataTask putHttpDataTask = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(Boolean output) {
                        if (output) finish();
                        else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                    }
                });
                try {
                    putHttpDataTask.execute(new URL(ConnectionDB.url + "product/" + etName.getText().toString() + "/" + etPrice.getText().toString() + "/" + etDescription.getText().toString() + "/" + ((Category) spCategory.getSelectedItem()).getIdcategory() + "/" + product.getIdproduct()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Comprobamos que los datos que se han rellenado correctamente
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private boolean checkFields() {
        etName.setBackground(getDrawable(R.drawable.et_rounded_login));
        etDescription.setBackground(getDrawable(R.drawable.et_rounded_login));
        etPrice.setBackground(getDrawable(R.drawable.et_rounded_login));
        spCategory.setBackground(getDrawable(R.drawable.et_rounded_login));

        if (etName.getText().length() > 3 && etName.length() <= 25) {
            if (etDescription.getText().length() > 5 && etDescription.getText().length() <= 200) {
                if (etPrice.getText().length() > 0 && Integer.parseInt(etPrice.getText().toString()) > 0) {
                    if (spCategory.getSelectedItem() != null && !((Category) spCategory.getSelectedItem()).getName().equals(getString(R.string.head_spinner_category))) {
                        return true;
                    } else {
                        spCategory.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                        Toast.makeText(this, R.string.category_error, Toast.LENGTH_LONG).show();
                    }
                } else {
                    etPrice.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                    Toast.makeText(this, R.string.not_0_price, Toast.LENGTH_LONG).show();
                }
            } else {
                etDescription.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                Toast.makeText(this, R.string.description_length_error, Toast.LENGTH_LONG).show();
            }
        } else {
            etName.setBackground(getDrawable(R.drawable.et_rounded_login_error));
            Toast.makeText(this, R.string.err_product_name_lenght, Toast.LENGTH_SHORT).show();
        }

        return false;
    }
}
