package com.javiersolercanto.batoimobile.model;

import java.io.Serializable;

public class User implements Serializable {

    private int iduser;
    private String name;
    private String nickaname;
    private String password;
    private String email;
    private String role;

    public User(int iduser, String name, String nickaname, String password, String email, String role) {
        this.iduser = iduser;
        this.name = name;
        this.nickaname = nickaname;
        this.password = password;
        this.email = email;
        this.role = role;
    }

    public User(String name, String nickaname, String password, String email) {
        this.name = name;
        this.nickaname = nickaname;
        this.password = password;
        this.email = email;
        this.role = "ROLE_USER";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickaname;
    }

    public void setNickname(String nickaname) {
        this.nickaname = nickaname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return this.name + " (" + this.nickaname + ")";
    }
}
