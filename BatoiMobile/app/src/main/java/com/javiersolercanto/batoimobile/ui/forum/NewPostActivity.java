package com.javiersolercanto.batoimobile.ui.forum;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PostHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PutHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.category.GetCategoryHttpDataTask;
import com.javiersolercanto.batoimobile.model.Category;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Post;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class NewPostActivity extends AppCompatActivity {

    private TextView tvHead;
    private EditText etSubject;
    private EditText etBody;
    private Spinner spCategory;
    private Button btnAccept;
    private Button btnCancel;

    private PostHttpDataTask postHttpDataTask;
    private PutHttpDataTask putHttpDataTask;
    private GetCategoryHttpDataTask getCategoryHttpDataTask;

    private static Post post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Primero comprobamos si se trata de un nuevo post o es una edición
        if (getIntent().getSerializableExtra("post") != null) {
            post = (Post) getIntent().getSerializableExtra("post");
        }

        setContentView(R.layout.activity_new_post);

        setUI();
    }

    /**
     * Si es una edición cambiaremos el nombre de la activity y rellenaremos los datos
     */
    private void setUI() {
        tvHead = findViewById(R.id.tvHead);
        etSubject = findViewById(R.id.etSubject);
        etBody = findViewById(R.id.etBody);
        spCategory = findViewById(R.id.spCategory);
        btnAccept = findViewById(R.id.btnAccept);
        btnCancel = findViewById(R.id.btnCancel);

        if (post != null) {
            tvHead.setText(R.string.edit_post);
            etSubject.setText(post.getSubject());
            etBody.setText(post.getBody());
        }

        // Cargamos el spinner
        getCategoryHttpDataTask = new GetCategoryHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    if (!output.isEmpty() && output.get(0) != null) {
                        ArrayAdapter<Category> spinnerAdapter = new ArrayAdapter<Category>(getApplicationContext(), android.R.layout.simple_list_item_1, (ArrayList<Category>) output);
                        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spCategory.setAdapter(spinnerAdapter);

                        if (post != null) {
                            for (int i = 0; i < output.size(); i++ ) {
                                if (post.getCategory().getName().equals(((Category) output.get(i)).getName())) {
                                    spCategory.setSelection(i);
                                }
                            }
                        }
                    }
                }
            }
        });
        try {
            getCategoryHttpDataTask.execute(new URL(ConnectionDB.url + "category/"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        // Ejecutamos las sentencias correspondientes según el modo (Edición o Insertado)
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                if (checkFields()) {
                    if (post != null) { //EDIT POST
                        putHttpDataTask = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                            @Override
                            public void processFinish(Boolean output) {
                                if (output) {
                                    PostDetailActivity.post = new Post(post.getId(), etSubject.getText().toString(), etBody.getText().toString(), post.isResolved(), post.getAuthor(), (Category) spCategory.getSelectedItem(), post.getReplies());
                                    post = null;
                                    finish();
                                } else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_LONG).show();
                            }
                        });
                        try {
                            putHttpDataTask.execute(new URL(ConnectionDB.url + "post/" + etSubject.getText().toString() + "/" + etBody.getText().toString() + "/" + ((Category) spCategory.getSelectedItem()).getIdcategory() + "/" + post.getId()));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    } else {  //INSERT POST
                        postHttpDataTask = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                            @Override
                            public void processFinish(Integer output) {
                                if (output != -1) {
                                    finish();
                                } else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_LONG).show();
                            }
                        });
                        try {
                            postHttpDataTask.execute(new URL(ConnectionDB.url + "post/" + etSubject.getText().toString() + "/" + etBody.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser() + "/" + ((Category) spCategory.getSelectedItem()).getIdcategory()));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post = null;
                finish();
            }
        });
    }

    /**
     * Comprobamos que los campos han sido rellenados correctamente
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private boolean checkFields() {
        etBody.setBackground(getDrawable(R.drawable.et_rounded_login));
        etSubject.setBackground(getDrawable(R.drawable.et_rounded_login));
        spCategory.setBackground(getDrawable(R.drawable.et_rounded_login));

        if (etSubject.getText().toString().length() > 5 && etSubject.getText().toString().length() <= 45) {
            if (etBody.getText().toString().length() > 5 && etBody.getText().toString().length() <= 300) {
                if (spCategory.getSelectedItem() != null && !((Category) spCategory.getSelectedItem()).getName().equals(getString(R.string.head_spinner_category))) {
                    return true;
                } else {
                    spCategory.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                    Toast.makeText(this, R.string.category_error, Toast.LENGTH_LONG).show();
                }
            } else {
                etBody.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                Toast.makeText(this, R.string.body_error, Toast.LENGTH_LONG).show();
            }
        } else {
            etSubject.setBackground(getDrawable(R.drawable.et_rounded_login_error));
            Toast.makeText(this, R.string.subject_error, Toast.LENGTH_LONG).show();
        }
        return false;
    }
}
