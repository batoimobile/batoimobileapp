package com.javiersolercanto.batoimobile.ui.forum;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.model.Post;

import java.util.ArrayList;

public class PostsActivity extends AppCompatActivity implements PostAdapter.OnItemClickListener, View.OnClickListener{

    private RecyclerView recyclerView;
    private PostAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private SwipeRefreshLayout swipeRefreshPosts;

    private static final int RESULT_CODE = 0;
    public static boolean isFiltering = false;
    private static TextView backText;
    public static ArrayList<Post> myDataset;

    private static boolean mode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.setTitle(getString(R.string.forum_title));

        setContentView(R.layout.activity_posts);

        setUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_menu, menu);
        return true;
    }

    private void setUI() {
        backText = findViewById(R.id.tvNotFound);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

        isFiltering = false;
        myDataset = new ArrayList<>();

        swipeRefreshPosts = findViewById(R.id.swipeRefreshPosts);
        swipeRefreshPosts.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshPosts.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.loadList();
                Toast.makeText(getApplicationContext(), R.string.post_refreshed, Toast.LENGTH_SHORT).show();
            }
        });

        mAdapter = new PostAdapter(myDataset, this, getApplicationContext(), swipeRefreshPosts);
        recyclerView.setAdapter(mAdapter);
        FloatingActionButton fab = findViewById(R.id.fab);

        // El fab nos permite abrir la activity que nos dejará crear un nuevo post
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NewPostActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isFiltering)
            mAdapter.loadList();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       if (item.getItemId() == R.id.filter){
           startActivityForResult(new Intent(this, PostSearchActivity.class), RESULT_CODE);
           return true;
       } else if (item.getItemId() == R.id.myPosts) {
           if (mode) {
               mAdapter.restoreList();
               item.setIcon(getDrawable(R.drawable.ic_forum_white_48dp));
               item.setTitle(R.string.my_posts);
           } else {
               mAdapter.findByUser();
               item.setIcon(getDrawable(R.drawable.ic_all_inclusive_white_48dp));
               item.setTitle(R.string.all_posts);
           }
           mode = !mode;

           return true;
       }
       else return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        isFiltering = true;
        if (requestCode == RESULT_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                myDataset = (ArrayList<Post>) data.getSerializableExtra("posts");
                mAdapter.setData(myDataset);
            }
        }
    }

    @Override
    public void onItemClick(Post post) {
        Intent intent = new Intent(this, PostDetailActivity.class);
        intent.putExtra("post", post);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static void refreshView() {
        if (myDataset.size() == 0) {
            backText.setVisibility(View.VISIBLE);

        } else {
            backText.setVisibility(View.INVISIBLE);
        }
    }
}