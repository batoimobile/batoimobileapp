package com.javiersolercanto.batoimobile;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class SplashActivity extends AppCompatActivity {
    private static final int SPLASH_TIME = 2500;
    private static final int INTERNET_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Making the activity full screen without notification bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        // Hiding the action bar
        ActionBar ab = getSupportActionBar();
        if(ab!=null)
            ab.hide();

        // Hiding also the navigation bar
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        Thread timer = new Thread()
        {
            @Override
            public void run()
            {
                super.run();
                try {
                    Thread.sleep(SPLASH_TIME); }
                catch (InterruptedException ex)
                {
                    Log.i("error",ex.toString());
                }
                finally
                {
                    if (checkPermission()) {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                        boolean firstTime = prefs.getBoolean(getString(R.string.firstTime), true);
                        if(firstTime) {
                            SharedPreferences.Editor edit = prefs.edit();
                            edit.putBoolean(getString(R.string.firstTime), Boolean.FALSE);
                            edit.commit();
                            startActivity(new Intent(getApplicationContext(), VerificationActivity.class));

                        } else {
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        }
                    }

                    finish();
                }
            }
        };

        timer.start();
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, INTERNET_PERMISSION);
            return false;
        } else return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == INTERNET_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                Toast.makeText(this, "OKEY", Toast.LENGTH_LONG).show();
            else Toast.makeText(this, "NOT", Toast.LENGTH_LONG).show();
        }
    }
}
