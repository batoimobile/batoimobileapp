package com.javiersolercanto.batoimobile.ui.routes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.model.Route;

import java.util.ArrayList;

public class RoutesActivity extends AppCompatActivity implements RouteAdapter.OnItemClickListener, View.OnClickListener{

    private RecyclerView recyclerView;
    private RouteAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private static final int RESULT_CODE = 0;
    private static boolean isFiltering;
    private static boolean filterMode = false;
    private static boolean priceMode = false;
    private static TextView backText;
    public static ArrayList<Route> myDataset;

    private SwipeRefreshLayout swipeRefreshRoutes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.setTitle("Routes");

        setContentView(R.layout.activity_routes);

        setUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.route_menu, menu);
        return true;
    }

    private void setUI() {
        backText = findViewById(R.id.tvNotFound);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        swipeRefreshRoutes = findViewById(R.id.swipeRefreshRoutes);
        swipeRefreshRoutes.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshRoutes.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.loadList();
                mAdapter.notifyDataSetChanged();
                refreshView();
                Toast.makeText(getApplicationContext(), "The routes are refresh", Toast.LENGTH_SHORT).show();
                swipeRefreshRoutes.setRefreshing(false);
            }
        });

        layoutManager = new LinearLayoutManager(this, recyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        isFiltering = false;
        myDataset = new ArrayList<>();

        mAdapter = new RouteAdapter(myDataset, this, getApplicationContext(), swipeRefreshRoutes);
        recyclerView.setAdapter(mAdapter);

        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NewRouteActivity.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                // User chose the "Settings" item, show the app settings UI...
                startActivityForResult(new Intent(this, RouteSearchActivity.class), RESULT_CODE);
                return true;

            case R.id.price:
                mAdapter.filterByPrice(priceMode);
                priceMode = !priceMode;
                return true;

            case R.id.myRoutes:
                if (!filterMode) {
                    mAdapter.findByUser();
                    item.setTitle(R.string.all_routes);
                    item.setIcon(R.drawable.ic_all_inclusive_white_48dp);
                } else {
                    mAdapter.loadList();
                    item.setTitle(R.string.my_routes);
                    item.setIcon(R.drawable.ic_directions_car_black_48dp);
                }
                filterMode = !filterMode;
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        isFiltering = true;
        if (requestCode == RESULT_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                myDataset = (ArrayList<Route>) data.getSerializableExtra("routes");
                mAdapter.setData(myDataset);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isFiltering)
            mAdapter.loadList();
    }

    @Override
    public void onItemClick(Route route) {
        Intent intent = new Intent(this, RouteDetailActivity.class);
        intent.putExtra("route", route);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }

    public static void refreshView() {
        Log.i("cicle", String.valueOf(myDataset.size())+ " on refresh");
        if (myDataset.size() == 0) {
            backText.setVisibility(View.VISIBLE);

        } else {
            backText.setVisibility(View.INVISIBLE);
        }
    }
}