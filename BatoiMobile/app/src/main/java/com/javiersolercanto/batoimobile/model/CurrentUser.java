package com.javiersolercanto.batoimobile.model;

public class CurrentUser {

    static User currentUser;

    public static User getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(User user) {
        currentUser = user;
    }
}
