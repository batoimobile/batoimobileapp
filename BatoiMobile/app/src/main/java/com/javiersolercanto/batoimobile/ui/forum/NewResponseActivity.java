package com.javiersolercanto.batoimobile.ui.forum;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.PostHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PutHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Post;
import com.javiersolercanto.batoimobile.model.Reply;

import java.net.MalformedURLException;
import java.net.URL;

public class NewResponseActivity extends AppCompatActivity {

    private PostHttpDataTask postHttpDataTask;
    private PutHttpDataTask putHttpDataTask;

    private EditText etResponse;
    private Button btnAccept;
    private Button btnCancel;

    private static Post post;
    private static Reply reply;

    private static boolean mode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_response);

        // Comprobando si es una inserción una edición
        if (getIntent().getSerializableExtra("reply") != null) {
            reply = (Reply) getIntent().getSerializableExtra("reply");
            mode = true;
        } else mode = false;

        // Guardamos el post padre
        if (getIntent().getSerializableExtra("post") != null) post = (Post) getIntent().getSerializableExtra("post");

        setUI();
    }

    private void setUI() {
        etResponse = findViewById(R.id.etResponse);
        btnAccept = findViewById(R.id.btnAccept);
        btnCancel = findViewById(R.id.btnCancel);

        if (mode) etResponse.setText(reply.getResponse());

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etResponse.getText().length() > 5) {
                    if (mode) { // EDICIÓN
                        putHttpDataTask = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                            @Override
                            public void processFinish(Boolean output) {
                                if (output) {
                                    finish();
                                } else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                            }
                        });
                        try {
                            putHttpDataTask.execute(new URL(ConnectionDB.url + "reply/edit/" + etResponse.getText().toString() + "/" + reply.getIdreply()));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    } else { // INSERTADO
                        postHttpDataTask = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                            @Override
                            public void processFinish(Integer output) {
                                if (output != -1) {
                                    finish();
                                }
                                else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                            }
                        });
                        try {
                            postHttpDataTask.execute(new URL(ConnectionDB.url + "reply/" + etResponse.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser() + "/" + post.getId()));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                } else Toast.makeText(getApplicationContext(), R.string.type_response, Toast.LENGTH_SHORT).show();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mode) {
                    Intent intent = new Intent(getApplicationContext(), PostDetailActivity.class);
                    intent.putExtra("post", post);
                    startActivity(intent);
                }
                finish();
            }
        });
    }
}
