package com.javiersolercanto.batoimobile;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.RSSAlert;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.tools.Tools;
import com.javiersolercanto.batoimobile.ui.forum.PostsActivity;
import com.javiersolercanto.batoimobile.ui.groups.GroupsActivity;
import com.javiersolercanto.batoimobile.ui.marketplace.ProductsActivity;
import com.javiersolercanto.batoimobile.ui.rentals.RentalsActivity;
import com.javiersolercanto.batoimobile.ui.routes.RoutesActivity;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView tvAlert;

    private static final String MOODLE_URL = "https://moodle19.cipfpbatoi.es/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (CurrentUser.getCurrentUser() == null) finish();

        setTitle("Home");

        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        tvAlert = findViewById(R.id.tvAlert);
        fillData();
    }

    public void openMoodle(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(MOODLE_URL));

        startActivity(intent);
    }

    public void openRoutes(View view) {
        startActivity(new Intent(getApplicationContext(), RoutesActivity.class));
    }

    public void openProducts(View view) {
        startActivity(new Intent(getApplicationContext(), ProductsActivity.class));
    }

    public void openForum(View view) {
        startActivity(new Intent(getApplicationContext(), PostsActivity.class));
    }

    public void openGroups(View view) {
        startActivity(new Intent(getApplicationContext(), GroupsActivity.class));
    }

    public void openRentals(View view) {
        startActivity(new Intent(getApplicationContext(), RentalsActivity.class));
    }

    /**
     * Este método nos permite cargar la alerta gracias a la RSS
     */
    public void fillData() {
        RSSAlert rssAlert = new RSSAlert(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    if (!output.isEmpty() && output.get(0) != null) {
                        tvAlert.setText((String) output.get(0));
                    } else Toast.makeText(getApplicationContext()
                            , R.string.error_during_action, Toast.LENGTH_SHORT).show();
                } else Toast.makeText(getApplicationContext(),
                        R.string.server_lost, Toast.LENGTH_SHORT).show();
            }
        });
        try {
            rssAlert.execute(new URL("https://e00-elmundo.uecdn.es/elmundo/rss/portada.xml"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cargamos el menú
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * Tratamos las opciones del menú seleccionadas
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.powerOff){
            deleteRememberedUser();
            startActivity(new Intent(this, LoginActivity.class));
            return true;
        } else if (item.getItemId() == R.id.profile) {
            startActivity(new Intent(this, ProfileActivity.class));
            return true;
        }
        else return super.onOptionsItemSelected(item);

    }

    /**
     * Método que nos permite eliminar la id de usuario recordada
     */
    private void deleteRememberedUser() {
        Tools.rememberUser(getApplicationContext(), -1);
    }

    /**
     * Antes de cerrar la app nos preguntará si queremos cerrar
     */
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.exit_message)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
