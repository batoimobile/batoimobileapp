package com.javiersolercanto.batoimobile.ui.rentals;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.rental.GetRentalHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Rental;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class RentalAdapter extends RecyclerView.Adapter<RentalAdapter.MyViewHolder> {

    public interface OnItemClickListener {

        void onItemClick(Rental rental);
    }

    private ArrayList<Rental> myDataset;
    private OnItemClickListener listener;
    private Context appContext;
    private SwipeRefreshLayout swipeRefreshLayout;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvAddressName;
        TextView tvKms;
        TextView tvPrice;
        TextView tvAuthor;

        public MyViewHolder(View view) {
            super(view);

            this.tvAddressName = view.findViewById(R.id.tvAddressName);
            this.tvKms = view.findViewById(R.id.tvKms);
            this.tvPrice = view.findViewById(R.id.tvPrice);
            this.tvAuthor = view.findViewById(R.id.tvAuthor);
        }

        public void bind(final Rental rental, final OnItemClickListener listener) {
            this.tvAddressName.setText(rental.getAddress().getName());
            this.tvPrice.setText(String.valueOf(rental.getPrice()));
            this.tvAuthor.setText(rental.getAuthor().getName());

            /**
             * Para calcular la distancia cogeremos la latitud y la longitud del alquiler y lo restaremos
             * a la loongitud y latitud del instituto
             */
            float[] distance = new float[1];
            Location.distanceBetween(
                    rental.getAddress().getLat(),
                    rental.getAddress().getLng(),
                    38.690825,
                    -0.496971,
                    distance);
            float kms = (float) (Math.round(distance[0]/1000 * 100.0) / 100.0);
            tvKms.setText(kms + "km");

            rental.setDistance(kms);

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(rental);
                }
            });
        }
    }

    RentalAdapter(ArrayList<Rental> myDataset, OnItemClickListener listener, Context appContext, SwipeRefreshLayout swipeRefreshLayout) {
        this.myDataset = myDataset;
        this.listener = listener;
        this.appContext = appContext;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    /**
     * Cargamos los datos de la base de datos
     */
    public void loadList() {
        myDataset.clear();
        swipeRefreshLayout.setRefreshing(true);
        GetRentalHttpDataTask getRentalHttpDataTask = new GetRentalHttpDataTask(appContext, new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    myDataset.addAll((ArrayList<Rental>) output);
                    notifyDataSetChanged();
                    RentalsActivity.refreshView();
                } else Toast.makeText(appContext, R.string.server_lost, Toast.LENGTH_LONG).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        try {
            getRentalHttpDataTask.execute(new URL(ConnectionDB.url + "rental"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Filtraremos los alquileres por su autor
     */
    public void findByUser() {
        ArrayList<Rental> newList = new ArrayList<>();
        for (Rental rental : myDataset) {
            if (rental.getAuthor().getIduser() == CurrentUser.getCurrentUser().getIduser()) {
                newList.add(rental);
            }
        }
        myDataset.clear();
        myDataset.addAll(newList);
        this.notifyDataSetChanged();
        RentalsActivity.refreshView();
    }

    /**
     * Usando otra vez nuestra clase ordenaremos por precio
     * @param mode
     */
    public void filterByPrice(boolean mode) {
        Collections.sort(myDataset, new MyPriceCompare(mode));

        notifyDataSetChanged();
        RentalsActivity.refreshView();
    }

    /**
     * Usaremos también el mismo sistema para ordenar por distancia
     * @param mode
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void filterByDistance(boolean mode) {
        Collections.sort(myDataset, new MyDistanceCompare(mode));

        notifyDataSetChanged();
        RentalsActivity.refreshView();
    }

    static class MyDistanceCompare implements Comparator<Rental> {

        private boolean mode;

        MyDistanceCompare(boolean mode) {
            this.mode = mode;
        }

        @Override
        public int compare(Rental r1, Rental r2) {
            if (mode) {
                if(r1.getDistance() > r2.getDistance()){
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if(r1.getDistance() < r2.getDistance()){
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    }

    static class MyPriceCompare implements Comparator<Rental> {

        private boolean mode;

        MyPriceCompare(boolean mode) {
            this.mode = mode;
        }

        @Override
        public int compare(Rental r1, Rental r2) {
            if (mode) {
                if(r1.getPrice() > r2.getPrice()){
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if(r1.getPrice() < r2.getPrice()){
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rental_item, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myDataset.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return myDataset.size();
    }
}
