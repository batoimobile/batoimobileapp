package com.javiersolercanto.batoimobile.ui.routes;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PostHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PutHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.address.GetAddressHttpDataTask;
import com.javiersolercanto.batoimobile.model.Address;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Route;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class NewRouteActivity extends AppCompatActivity {

    private TextView tvHead;
    private EditText etDate;
    private EditText etPrice;
    private EditText etSeats;
    private EditText etObservations;
    private Button btnAccept;
    private Button btnCancel;

    private AutocompleteSupportFragment fgTo;
    private AutocompleteSupportFragment fgFrom;

    private static boolean mode;
    private static Route route;
    private static Address from;
    private static Address to;
    private static String sDate;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_route);

        if (getIntent() != null) mode = (route = (Route) getIntent().getSerializableExtra("route")) != null;
        else mode = false;

        setUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setUI() {
        tvHead = findViewById(R.id.tvHead);
        etDate = findViewById(R.id.etDate);
        etPrice = findViewById(R.id.etPrice);
        etSeats = findViewById(R.id.spCategory);
        etObservations = findViewById(R.id.etObservations);
        btnAccept = findViewById(R.id.btnAccept);
        btnCancel = findViewById(R.id.btnCancel);

        // Initialize the AutocompleteSupportFragment.
        fgTo = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.fgTo);
        fgFrom = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.fgFrom);

        if (!Places.isInitialized()) Places.initialize(getApplicationContext(), "AIzaSyBFrnw6KbpN68Cv0odVA-AAAxPPll7_Oys");

        // Specify the types of place data to return.
        fgTo.setPlaceFields(Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME));
        fgTo.setHint(getString(R.string.to));
        fgTo.setCountry("ES");

        fgFrom.setPlaceFields(Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME));
        fgFrom.setHint(getString(R.string.from));
        fgFrom.setCountry("ES");

        // Set up a PlaceSelectionListener to handle the response.
        fgTo.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                to = new Address(place.getName(), place.getLatLng().latitude, place.getLatLng().longitude);
            }

            @Override
            public void onError(Status status) {
            }
        });

        fgFrom.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                from = new Address(place.getName(), place.getLatLng().latitude, place.getLatLng().longitude);
            }

            @Override
            public void onError(Status status) {
            }
        });

        if (mode) {
            tvHead.setText(R.string.edit_route);
            fillData();
        }

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                if (checkFields()) {
                    if (!mode) {
                        GetAddressHttpDataTask getFrom = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                            @Override
                            public void processFinish(ArrayList<?> output) {
                                if (output != null) {
                                    if (output.size() > 0 && output.get(0) != null) {
                                        from.setIdaddress(((Address) output.get(0)).getIdaddress());
                                        GetAddressHttpDataTask getTo = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                                            @Override
                                            public void processFinish(ArrayList<?> output) {
                                                if (output != null) {
                                                    if (output.size() > 0 && output.get(0) != null) {
                                                        to.setIdaddress(((Address) output.get(0)).getIdaddress());
                                                        PostHttpDataTask postRouteNoAddress = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                            @Override
                                                            public void processFinish(Integer output) {
                                                                if (output != -1) finish();
                                                                else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                            }
                                                        });
                                                        try {
                                                            postRouteNoAddress.execute(new URL(ConnectionDB.url + "route/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + sDate + "/" + etPrice.getText().toString() + "/" + etSeats.getText().toString() + "/" + etObservations.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                        } catch (MalformedURLException e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
                                                        PostHttpDataTask postTo = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                            @Override
                                                            public void processFinish(Integer output) {
                                                                if (output != -1) {
                                                                    to.setIdaddress(output);
                                                                    PostHttpDataTask postRoute = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                        @Override
                                                                        public void processFinish(Integer output) {
                                                                            if (output != -1) finish();
                                                                            else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    });
                                                                    try {
                                                                        postRoute.execute(new URL(ConnectionDB.url + "route/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + sDate + "/" + etPrice.getText().toString() + "/" + etSeats.getText().toString() + "/" + etObservations.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                                    } catch (MalformedURLException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }
                                                        });
                                                        try {
                                                            postTo.execute(new URL(ConnectionDB.url + "address/" + to.getName() + "/" + to.getLat() + "/" + to.getLng()));
                                                        } catch (MalformedURLException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        try {
                                            getTo.execute(new URL(ConnectionDB.url + "address/getByName/" + to.getName()));
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        PostHttpDataTask postFrom = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                            @Override
                                            public void processFinish(Integer output) {
                                                if (output != -1) {
                                                    from.setIdaddress(output);
                                                    GetAddressHttpDataTask getTo = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                                                        @Override
                                                        public void processFinish(ArrayList<?> output) {
                                                            if (output != null) {
                                                                if (output.size() > 0 && output.get(0) != null) {
                                                                    to.setIdaddress(((Address) output.get(0)).getIdaddress());
                                                                    PostHttpDataTask postRouteNoAddress = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                        @Override
                                                                        public void processFinish(Integer output) {
                                                                            if (output != -1) finish();
                                                                            else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    });
                                                                    try {
                                                                        postRouteNoAddress.execute(new URL(ConnectionDB.url + "route/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + sDate + "/" + etPrice.getText().toString() + "/" + etSeats.getText().toString() + "/" + etObservations.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                                    } catch (MalformedURLException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                } else {
                                                                    PostHttpDataTask postTo = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                        @Override
                                                                        public void processFinish(Integer output) {
                                                                            if (output != -1) {
                                                                                to.setIdaddress(output);
                                                                                PostHttpDataTask postRoute = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                                    @Override
                                                                                    public void processFinish(Integer output) {
                                                                                        if (output != -1) finish();
                                                                                        else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                                    }
                                                                                });
                                                                                try {
                                                                                    postRoute.execute(new URL(ConnectionDB.url + "route/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + sDate + "/" + etPrice.getText().toString() + "/" + etSeats.getText().toString() + "/" + etObservations.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                                                } catch (MalformedURLException e) {
                                                                                    e.printStackTrace();
                                                                                }
                                                                            }
                                                                        }
                                                                    });
                                                                    try {
                                                                        postTo.execute(new URL(ConnectionDB.url + "address/" + to.getName() + "/" + to.getLat() + "/" + to.getLng()));
                                                                    } catch (MalformedURLException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                                    try {
                                                        getTo.execute(new URL(ConnectionDB.url + "address/getByName/" + to.getName()));
                                                    } catch (MalformedURLException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        });
                                        try {
                                            postFrom.execute(new URL(ConnectionDB.url + "address/" + from.getName() + "/" + from.getLat() + "/" + from.getLng()));
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                            }
                        });
                        try {
                            getFrom.execute(new URL(ConnectionDB.url + "address/getByName/" + from.getName()));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    } else {
                        GetAddressHttpDataTask getFrom = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                            @Override
                            public void processFinish(ArrayList<?> output) {
                                if (output != null) {
                                    if (output.size() > 0 && output.get(0) != null) {
                                        from.setIdaddress(((Address) output.get(0)).getIdaddress());
                                        GetAddressHttpDataTask getTo = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                                            @Override
                                            public void processFinish(ArrayList<?> output) {
                                                if (output != null) {
                                                    if (output.size() > 0 && output.get(0) != null) {
                                                        to.setIdaddress(((Address) output.get(0)).getIdaddress());
                                                        PutHttpDataTask putRouteNoAddress = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                                            @Override
                                                            public void processFinish(Boolean output) {
                                                                if (output) finish();
                                                                else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                            }
                                                        });
                                                        try {
                                                            putRouteNoAddress.execute(new URL(ConnectionDB.url + "route/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + sDate + "/" + etPrice.getText().toString() + "/" + etSeats.getText().toString() + "/" + etObservations.getText().toString() + "/" + route.getIdroute()));
                                                        } catch (MalformedURLException e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
                                                        PostHttpDataTask postTo = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                            @Override
                                                            public void processFinish(Integer output) {
                                                                if (output != -1) {
                                                                    to.setIdaddress(output);
                                                                    PutHttpDataTask putRoute = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                                                        @Override
                                                                        public void processFinish(Boolean output) {
                                                                            if (output) finish();
                                                                            else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    });
                                                                    try {
                                                                        putRoute.execute(new URL(ConnectionDB.url + "route/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + sDate + "/" + etPrice.getText().toString() + "/" + etSeats.getText().toString() + "/" + etObservations.getText().toString() + "/" + route.getIdroute()));
                                                                    } catch (MalformedURLException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }
                                                        });
                                                        try {
                                                            postTo.execute(new URL(ConnectionDB.url + "address/" + to.getName() + "/" + to.getLat() + "/" + to.getLng()));
                                                        } catch (MalformedURLException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        try {
                                            getTo.execute(new URL(ConnectionDB.url + "address/getByName/" + to.getName()));
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        PostHttpDataTask postFrom = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                            @Override
                                            public void processFinish(Integer output) {
                                                if (output != -1) {
                                                    from.setIdaddress(output);
                                                    GetAddressHttpDataTask getTo = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                                                        @Override
                                                        public void processFinish(ArrayList<?> output) {
                                                            if (output != null) {
                                                                if (output.size() > 0 && output.get(0) != null) {
                                                                    to.setIdaddress(((Address) output.get(0)).getIdaddress());
                                                                    PutHttpDataTask putRouteNoAddress = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                                                        @Override
                                                                        public void processFinish(Boolean output) {
                                                                            if (output) finish();
                                                                            else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    });
                                                                    try {
                                                                        putRouteNoAddress.execute(new URL(ConnectionDB.url + "route/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + sDate + "/" + etPrice.getText().toString() + "/" + etSeats.getText().toString() + "/" + etObservations.getText().toString() + "/" + route.getIdroute()));
                                                                    } catch (MalformedURLException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                } else {
                                                                    PostHttpDataTask postTo = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                        @Override
                                                                        public void processFinish(Integer output) {
                                                                            if (output != -1) {
                                                                                to.setIdaddress(output);
                                                                                PutHttpDataTask putRoute = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                                                                    @Override
                                                                                    public void processFinish(Boolean output) {
                                                                                        if (output) finish();
                                                                                        else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                                    }
                                                                                });
                                                                                try {
                                                                                    putRoute.execute(new URL(ConnectionDB.url + "route/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + sDate + "/" + etPrice.getText().toString() + "/" + etSeats.getText().toString() + "/" + etObservations.getText().toString() + "/" + route.getIdroute()));
                                                                                } catch (MalformedURLException e) {
                                                                                    e.printStackTrace();
                                                                                }
                                                                            }
                                                                        }
                                                                    });
                                                                    try {
                                                                        postTo.execute(new URL(ConnectionDB.url + "address/" + to.getName() + "/" + to.getLat() + "/" + to.getLng()));
                                                                    } catch (MalformedURLException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                                    try {
                                                        getTo.execute(new URL(ConnectionDB.url + "address/getByName/" + to.getName()));
                                                    } catch (MalformedURLException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        });
                                        try {
                                            postFrom.execute(new URL(ConnectionDB.url + "address/" + from.getName() + "/" + from.getLat() + "/" + from.getLng()));
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                            }
                        });
                        try {
                            getFrom.execute(new URL(ConnectionDB.url + "address/getByName/" + from.getName()));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        btnCancel.setOnClickListener(v -> finish());
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private boolean checkFields() {
        etObservations.setBackground(getDrawable(R.drawable.et_rounded_login));
        etSeats.setBackground(getDrawable(R.drawable.et_rounded_login));
        etPrice.setBackground(getDrawable(R.drawable.et_rounded_login));
        etDate.setBackground(getDrawable(R.drawable.et_rounded_login));

        if (from != null && to != null && from.getName().length() > 0 && to.getName().length() > 0) {
            if (etDate.getText().toString().length() > 0) {
                if (etPrice.getText().toString().length() > 0 && Integer.parseInt(etPrice.getText().toString()) > 0) {
                    if (etSeats.getText().toString().length() > 0 && Integer.parseInt(etSeats.getText().toString()) > 0) {
                        if (etObservations.getText().toString().length() > 10 && etObservations.getText().toString().length() < 150) return true;
                        else {
                            etObservations.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                            Toast.makeText(getApplicationContext(), R.string.observations_lenght_error, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        etSeats.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                        Toast.makeText(getApplicationContext(), R.string.seats_no_0, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    etPrice.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                    Toast.makeText(getApplicationContext(), R.string.not_0_price, Toast.LENGTH_SHORT).show();
                }
            } else {
                etDate.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                Toast.makeText(getApplicationContext(), R.string.date_not_null, Toast.LENGTH_SHORT).show();
            }
        } else Toast.makeText(getApplicationContext(), R.string.address_necessary, Toast.LENGTH_SHORT).show();

        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void fillData() {
        fgFrom.setText(route.getFrom().getName());
        fgTo.setText(route.getTo().getName());
        etPrice.setText(String.valueOf(route.getPrice()));
        etSeats.setText(String.valueOf(route.getSeats()));
        etObservations.setText(route.getObservation());
    }

    public void showDateTimeDialog(View view) {
        final Calendar calendar=Calendar.getInstance();
        DatePickerDialog.OnDateSetListener sDateSetListener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                TimePickerDialog.OnTimeSetListener timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);

                        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("EEE d MMM HH:mm");
                        SimpleDateFormat f = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                        Date date = calendar.getTime();
                        sDate = f.format(date);

                        etDate.setText(simpleDateFormat.format(date));
                    }
                };

                new TimePickerDialog(NewRouteActivity.this,timeSetListener,calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE),false).show();
            }
        };

        DatePickerDialog dp = new DatePickerDialog(NewRouteActivity.this,sDateSetListener,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
        dp.getDatePicker().setMinDate(System.currentTimeMillis());
        dp.show();
    }
}
