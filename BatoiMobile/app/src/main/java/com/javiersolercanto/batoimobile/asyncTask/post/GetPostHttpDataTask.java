package com.javiersolercanto.batoimobile.asyncTask.post;

import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.model.Category;
import com.javiersolercanto.batoimobile.model.Post;
import com.javiersolercanto.batoimobile.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetPostHttpDataTask extends GetHttpDataTask {

    private WeakReference<Context> appContext;

    public GetPostHttpDataTask(Context appContext, ProgressBar progressBar, AsyncResponse asyncResponse) {
        super(appContext, progressBar, asyncResponse);
    }

    public GetPostHttpDataTask(Context appContext, AsyncResponse asyncResponse) {
        super(appContext,asyncResponse);
    }

    @Override
    protected ArrayList<Object> doInBackground(Object... objects) {
        HttpURLConnection urlConnection = null;
        ArrayList<Object> searchResult = null;
        try {
            if ((urlConnection = stablishConnection((URL) objects[0])) != null) {
                String resultStream = readStream(urlConnection.getInputStream());

                searchResult = new ArrayList<>();

                JSONObject response = new JSONObject(resultStream);
                //if (response.getBoolean("response")) {
                JSONArray posts = response.getJSONArray("posts");
                for (int i = 0; i < posts.length(); i++) {
                    JSONObject post = posts.getJSONObject(i);
                    searchResult.add(new Post(
                        post.getInt("idpost"),
                        post.getString("subject"),
                        post.getString("body"),
                        (post.getInt("resolved") != 0),
                        (new User(
                                post.getInt("iduser"),
                                post.getString("user_name"),
                                post.getString("nickname"),
                                post.getString("password"),
                                post.getString("email"),
                                post.getString("role"))
                        ),
                        (new Category(
                                post.getInt("idcategory"),
                                post.getString("category_name")
                        )),
                        post.getInt("replies"))
                    );
                    if (isCancelled()) break;
                }
            }
        } catch (IOException e) {
            Log.i("IOException", e.getMessage());
        } catch (JSONException e) {
            Log.i("JSONException", e.getMessage());
            e.printStackTrace();
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return searchResult;
    }
}
