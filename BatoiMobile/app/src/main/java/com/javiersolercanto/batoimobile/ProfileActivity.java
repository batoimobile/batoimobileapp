package com.javiersolercanto.batoimobile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.asyncTask.DeleteHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PutHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.group.GetGroupHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.post.GetPostHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.product.GetProductHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.rental.GetRentalHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.user.GetUserHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.User;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {

    private TextView tvName;
    private TextView tvPosts;
    private TextView tvGroups;
    private TextView tvProducts;
    private TextView tvRentals;
    private LinearLayout linearData;
    private LinearLayout linearPassword;
    private EditText etName;
    private EditText etNickname;
    private EditText etEmail;
    private EditText etCurrentPassword;
    private EditText etNewPassword1;
    private EditText etNewPassword2;
    private Button btnChangePassword;
    private Button btnSaveChanges;

    private static final int HIDE_PASSWORD = 0;
    private static final int SHOW_PASSWORD = 1;
    private static int MODE = HIDE_PASSWORD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle(getString(R.string.my_profile));

        setContentView(R.layout.activity_profile);

        setUI();
    }

    private void setUI() {
        tvPosts = findViewById(R.id.tvPosts);
        tvProducts = findViewById(R.id.tvProducts);
        tvGroups = findViewById(R.id.tvGroups);
        tvRentals = findViewById(R.id.tvRentals);
        linearData = findViewById(R.id.linearData);
        linearPassword = findViewById(R.id.linearPassword);
        tvName = findViewById(R.id.tvName);
        etName = findViewById(R.id.etName);
        etNickname = findViewById(R.id.etNickname);
        etEmail = findViewById(R.id.etEmail);
        etCurrentPassword = findViewById(R.id.etCurrentPassword);
        etNewPassword1 = findViewById(R.id.etNewPassword1);
        etNewPassword2 = findViewById(R.id.etNewPassword2);
        btnChangePassword = findViewById(R.id.btnChangePassword);
        btnSaveChanges = findViewById(R.id.btnSaveChanges);

        fillData();

        /**
         * Esto nos permite cambiar el formulario entre cambiar contrasña o los datos
         */
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (MODE == HIDE_PASSWORD) {
                    linearData.setVisibility(View.GONE);
                    linearPassword.setVisibility(View.VISIBLE);

                    btnChangePassword.setText(R.string.change_data);
                    MODE = SHOW_PASSWORD;

                } else {
                    linearData.setVisibility(View.VISIBLE);
                    linearPassword.setVisibility(View.GONE);

                    btnChangePassword.setText(R.string.change_password);
                    MODE = HIDE_PASSWORD;
                }
            }
        });

        /**
         * Dependiendo del modo ejecutará una sentencia u otra para guardar en la base de datos
         */
        btnSaveChanges.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                if (checkFields()) {
                    if (MODE == HIDE_PASSWORD) {    // SAVE DATA
                        if (!etNickname.getText().toString().equals(CurrentUser.getCurrentUser().getNickname())) {
                            GetUserHttpDataTask getUserHttpDataTask = new GetUserHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                                @Override
                                public void processFinish(ArrayList<?> output) {
                                    if (output != null) {
                                        if (output.isEmpty()) {
                                            PutHttpDataTask putHttpDataTask = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                                @Override
                                                public void processFinish(Boolean output) {
                                                    if (output) {
                                                        tvName.setText(etName.getText().toString());
                                                        Toast.makeText(getApplicationContext(), R.string.correct_changes, Toast.LENGTH_LONG).show();
                                                    }
                                                    else {
                                                        Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });
                                            try {
                                                putHttpDataTask.execute(new URL(ConnectionDB.url + "user/" + etName.getText().toString() + "/" + etNickname.getText().toString() + "/" + etEmail.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()));
                                            } catch (MalformedURLException e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), R.string.nickname_taken, Toast.LENGTH_LONG).show();
                                            etNickname.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                            try {
                                getUserHttpDataTask.execute(new URL(ConnectionDB.url + "user/nickname/" + etNickname.getText().toString()));
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        } else {
                            PutHttpDataTask putHttpDataTask = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                @Override
                                public void processFinish(Boolean output) {
                                    if (output) {
                                        tvName.setText(etName.getText().toString());
                                        Toast.makeText(getApplicationContext(), R.string.correct_changes, Toast.LENGTH_LONG).show();
                                        GetUserHttpDataTask getNewUser = new GetUserHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                                            @Override
                                            public void processFinish(ArrayList<?> output) {
                                                if (output != null && !output.isEmpty() && output.get(0) != null) CurrentUser.setCurrentUser((User) output.get(0));
                                            }
                                        });
                                        try {
                                            getNewUser.execute(new URL(ConnectionDB.url + "user/" + CurrentUser.getCurrentUser().getIduser()));
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    else {
                                        Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                            try {
                                putHttpDataTask.execute(new URL(ConnectionDB.url + "user/" + etName.getText().toString() + "/" + etNickname.getText().toString() + "/" + etEmail.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()));
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {    // SAVE PASS
                        GetUserHttpDataTask getUserHttpDataTask = new GetUserHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                            @Override
                            public void processFinish(ArrayList<?> output) {
                                if (output != null) {
                                    if (!output.isEmpty() && output.get(0) != null) {
                                        PutHttpDataTask putHttpDataTask = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                            @Override
                                            public void processFinish(Boolean output) {
                                                if (output) {
                                                    Toast.makeText(getApplicationContext(), R.string.correct_changes, Toast.LENGTH_LONG).show();
                                                    etCurrentPassword.getText().clear();
                                                    etNewPassword1.getText().clear();
                                                    etNewPassword2.getText().clear();
                                                }
                                                else {
                                                    Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_LONG).show();
                                                    etNewPassword2.getText().clear();
                                                }
                                            }
                                        });
                                        try {
                                            putHttpDataTask.execute(new URL(ConnectionDB.url + "user/" + etNewPassword1.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()));
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), getString(R.string.pass_incorrect), Toast.LENGTH_LONG).show();
                                        etCurrentPassword.getText().clear();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.server_lost), Toast.LENGTH_LONG).show();
                                    etCurrentPassword.getText().clear();
                                }
                            }
                        });
                        try {
                            getUserHttpDataTask.execute(new URL(ConnectionDB.url + "user/" + CurrentUser.getCurrentUser().getNickname() + "/" + etCurrentPassword.getText().toString()));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    /**
     * Comprobamos que los campos se han rellenado correctamente
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private boolean checkFields() {
        if (MODE == HIDE_PASSWORD) {
            etName.setBackground(getDrawable(R.drawable.et_rounded_login));
            etNickname.setBackground(getDrawable(R.drawable.et_rounded_login));
            etEmail.setBackground(getDrawable(R.drawable.et_rounded_login));
            if (etName.length() > 6 && etName.length() <= 25) {
                if (etNickname.length() > 3 && etNickname.length() <= 25) {
                    if (etEmail.length() > 9 && etEmail.length() <= 50) {
                        return true;
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.err_email_lenght, Toast.LENGTH_LONG).show();
                        etEmail.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.err_nickanme_length, Toast.LENGTH_LONG).show();
                    etNickname.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                }
            } else {
                Toast.makeText(getApplicationContext(), R.string.err_name_lenght, Toast.LENGTH_LONG).show();
                etName.setBackground(getDrawable(R.drawable.et_rounded_login_error));
            }
        } else {
            etCurrentPassword.setBackground(getDrawable(R.drawable.et_rounded_login));
            etNewPassword1.setBackground(getDrawable(R.drawable.et_rounded_login));
            etNewPassword2.setBackground(getDrawable(R.drawable.et_rounded_login));
            if (etNewPassword1.length() > 7 && etNewPassword1.length() <= 25) {
                if (etNewPassword1.getText().toString().equals(etNewPassword2.getText().toString())) {
                    return true;
                } else {
                    Toast.makeText(getApplicationContext(), R.string.err_pass_ident, Toast.LENGTH_LONG).show();
                    etNewPassword2.getText().clear();
                    etNewPassword2.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                }
            } else {
                Toast.makeText(getApplicationContext(), R.string.err_pass_lenght, Toast.LENGTH_LONG).show();
                etNewPassword1.setBackground(getDrawable(R.drawable.et_rounded_login_error));
            }
        }

        return false;
    }

    /**
     * Cargamos todos los datos del usuario en los respectivos textviews
     */
    private void fillData() {
        tvName.setText(CurrentUser.getCurrentUser().getName());
        etName.setText(CurrentUser.getCurrentUser().getName());
        etNickname.setText(CurrentUser.getCurrentUser().getNickname());
        etEmail.setText(CurrentUser.getCurrentUser().getEmail());

        GetPostHttpDataTask getPostHttpDataTask = new GetPostHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null && !output.isEmpty() && output.get(0) != null) tvPosts.setText(String.valueOf(output.size()));
                GetGroupHttpDataTask getGroupHttpDataTask = new GetGroupHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(ArrayList<?> output) {
                        if (output != null && !output.isEmpty() && output.get(0) != null) tvGroups.setText(String.valueOf(output.size()));
                        GetProductHttpDataTask getProductHttpDataTask = new GetProductHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                            @Override
                            public void processFinish(ArrayList<?> output) {
                                if (output != null && !output.isEmpty() && output.get(0) != null) tvProducts.setText(String.valueOf(output.size()));
                                GetRentalHttpDataTask getRentalHttpDataTask = new GetRentalHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                                    @Override
                                    public void processFinish(ArrayList<?> output) {
                                        if (output != null && !output.isEmpty() && output.get(0) != null) tvRentals.setText(String.valueOf(output.size()));
                                    }
                                });
                                try {
                                    getRentalHttpDataTask.execute(new URL(ConnectionDB.url + "rental/getByAuthor/" + CurrentUser.getCurrentUser().getIduser()));
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        try {
                            getProductHttpDataTask.execute(new URL(ConnectionDB.url + "product/getByAuthor/" + CurrentUser.getCurrentUser().getIduser()));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                });
                try {
                    getGroupHttpDataTask.execute(new URL(ConnectionDB.url + "group/getByAuthor/" + CurrentUser.getCurrentUser().getIduser()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            getPostHttpDataTask.execute(new URL(ConnectionDB.url + "post/getByAuthor/" + CurrentUser.getCurrentUser().getIduser()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.deleteAccount) {
            createDialog(getString(R.string.alert_delete_account), null, ProfileActivity.this);
            return true;
        } else return super.onOptionsItemSelected(item);

    }

    /**
     * Mostramos un dialog que nos permite avisar al usuario de que va a borrar su cuenta
     * @param title
     * @param message
     * @param activity
     */
    public void createDialog(String title, String message, Context activity) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        builder.setTitle(title);

        if (message != null) builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DeleteHttpDataTask deleteHttpDataTask = new DeleteHttpDataTask(getApplicationContext(), new DeleteHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(Boolean output) {
                        if (output) {
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            finish();
                        } else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_LONG).show();
                    }
                });
                try {
                    deleteHttpDataTask.execute(new URL(ConnectionDB.url + "user/" + CurrentUser.getCurrentUser().getIduser()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
