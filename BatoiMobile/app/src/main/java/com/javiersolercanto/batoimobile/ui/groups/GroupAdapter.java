package com.javiersolercanto.batoimobile.ui.groups;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.group.GetGroupHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Group;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.MyViewHolder> {

    public interface OnItemClickListener {

        void onItemClick(Group group);
    }

    private ArrayList<Group> myDataset;
    private GroupAdapter.OnItemClickListener listener;
    private Context appContext;
    private SwipeRefreshLayout swipeRefreshLayout;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvRoute;
        TextView tvShift;
        TextView tvDays;
        TextView tvPrice;
        TextView tvMembers;

        public MyViewHolder(View view) {
            super(view);

            this.tvName = view.findViewById(R.id.tvName);
            this.tvRoute = view.findViewById(R.id.tvRoute);
            this.tvShift = view.findViewById(R.id.tvShift);
            this.tvDays = view.findViewById(R.id.tvDays);
            this.tvPrice = view.findViewById(R.id.tvPrice);
            this.tvMembers = view.findViewById(R.id.tvMembers);
        }

        public void bind(final Group group, final OnItemClickListener listener) {
            this.tvName.setText(group.getName());
            this.tvRoute.setText(group.getFrom().getName() + " - " + group.getTo().getName());
            this.tvShift.setText(group.isShift() ? "Tarde" : "Mañana");
            this.tvDays.setText(setDays(group.getDays()));
            this.tvPrice.setText(String.valueOf(group.getPrice()));
            this.tvMembers.setText(String.valueOf(group.getMembers()));

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(group);
                }
            });
        }

        /**
         * Este método nos permite convertir esa semana "binaria" en días reales
         * @param days
         * @return
         */
        private String setDays(String days) {
            String[] sDays = days.split("");
            days = "";
            for (int i = 0; i < sDays.length; i++) {
                if (!sDays[i].equals("0")) {
                    String day = "";
                    switch (i) {
                        case 0: day = "L"; break;
                        case 1: day = "M"; break;
                        case 2: day = "X"; break;
                        case 3: day = "J"; break;
                        case 4: day = "V"; break;
                    }
                    days += day + " ";
                }
            }

            return days;
        }
    }

    GroupAdapter(ArrayList<Group> myDataset, OnItemClickListener listener, Context appContext, SwipeRefreshLayout swipeRefreshLayout) {
        this.myDataset = myDataset;
        this.listener = listener;
        this.appContext = appContext;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    /**
     * Cargamos la lista de la base de datos
     */
    public void loadList() {
        myDataset.clear();
        swipeRefreshLayout.setRefreshing(true);
        GetGroupHttpDataTask getGroupHttpDataTask = new GetGroupHttpDataTask(this.appContext, new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    myDataset.addAll((ArrayList<Group>) output);
                    notifyDataSetChanged();
                    GroupsActivity.refreshView();
                } else Toast.makeText(appContext, R.string.server_lost, Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        try {
            getGroupHttpDataTask.execute(new URL(ConnectionDB.url + "group"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Filtramos los grupos de los que el usuario actual es administrador
     */
    public void findByUser() {
        ArrayList<Group> newList = new ArrayList<>();
        for (Group group : myDataset) {
            if (group.getAdmin().getIduser() == CurrentUser.getCurrentUser().getIduser()) {
                newList.add(group);
            }
        }
        myDataset.clear();
        myDataset.addAll(newList);
        this.notifyDataSetChanged();
        GroupsActivity.refreshView();
    }

    /**
     * Ordenamos la lista según el precio
     * @param mode
     */
    public void filterByPrice(boolean mode) {
        Collections.sort(myDataset, new MyPriceCompare(mode));

        notifyDataSetChanged();
        GroupsActivity.refreshView();
    }

    /**
     * Tenemos nuestra clase comparadora para poder ordenar y comparar mediante los atributos de la clase
     * que nosotros queremos
     */
    static class MyPriceCompare implements Comparator<Group> {

        private boolean mode;

        // Dependiendo del modo será de menor a mayor o viceversa
        MyPriceCompare(boolean mode) {
            this.mode = mode;
        }

        @Override
        public int compare(Group g1, Group g2) {
            if (mode) {
                if(g1.getPrice() > g2.getPrice()){
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if(g1.getPrice() < g2.getPrice()){
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_item, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myDataset.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return myDataset.size();
    }
}
