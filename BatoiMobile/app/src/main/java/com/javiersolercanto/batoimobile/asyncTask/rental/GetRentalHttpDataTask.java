package com.javiersolercanto.batoimobile.asyncTask.rental;

import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.model.Address;
import com.javiersolercanto.batoimobile.model.Rental;
import com.javiersolercanto.batoimobile.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetRentalHttpDataTask extends GetHttpDataTask {

    public GetRentalHttpDataTask(Context appContext, ProgressBar progressBar, AsyncResponse asyncResponse) {
        super(appContext, progressBar, asyncResponse);
    }

    public GetRentalHttpDataTask(Context appContext, AsyncResponse asyncResponse) {
        super(appContext,asyncResponse);
    }

    @Override
    protected ArrayList<Object> doInBackground(Object... objects) {
        HttpURLConnection urlConnection = null;
        ArrayList<Object> searchResult = null;
        try {
            if ((urlConnection = stablishConnection((URL) objects[0])) != null) {
                String resultStream = readStream(urlConnection.getInputStream());

                searchResult = new ArrayList<>();

                JSONObject response = new JSONObject(resultStream);
                JSONArray replies = response.getJSONArray("rentals");
                for (int i = 0; i < replies.length(); i++) {
                    JSONObject rental = replies.getJSONObject(i);
                    searchResult.add(new Rental(
                            rental.getInt("idrental"),
                            rental.getString("description"),
                            rental.getInt("price"),
                            (new User(
                                rental.getInt("iduser"),
                                rental.getString("name"),
                                rental.getString("nickname"),
                                rental.getString("password"),
                                rental.getString("email"),
                                rental.getString("role")
                            )),
                            new Address(
                                rental.getInt("idaddress"),
                                rental.getString("address_name"),
                                rental.getDouble("lat"),
                                rental.getDouble("lng")
                            )
                        )
                    );
                    if (isCancelled()) break;
                }
            }
        } catch (IOException e) {
            Log.i("IOException", e.getMessage());
        } catch (JSONException e) {
            Log.i("JSONException", e.getMessage());
            e.printStackTrace();
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return searchResult;
    }
}
