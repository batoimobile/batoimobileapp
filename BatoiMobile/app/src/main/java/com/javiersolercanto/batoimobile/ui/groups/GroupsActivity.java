package com.javiersolercanto.batoimobile.ui.groups;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.model.Group;

import java.util.ArrayList;

public class GroupsActivity extends AppCompatActivity implements GroupAdapter.OnItemClickListener, View.OnClickListener{

    private RecyclerView recyclerView;
    private GroupAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private static boolean filterMode;
    private static boolean priceMode;
    private static TextView backText;
    private static ArrayList<Group> myDataset;
    private SwipeRefreshLayout swipeRefreshGroups;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.setTitle("Groups");

        setContentView(R.layout.activity_groups);

        setUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.group_menu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.loadList();
    }

    private void setUI() {
        backText = findViewById(R.id.tvNotFound);
        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

        myDataset = new ArrayList<>();

        swipeRefreshGroups = findViewById(R.id.swipeRefreshGroups);
        swipeRefreshGroups.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshGroups.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.loadList();
                Toast.makeText(getApplicationContext(), R.string.groups_refreshed, Toast.LENGTH_SHORT).show();
            }
        });

        mAdapter = new GroupAdapter(myDataset, this, getApplicationContext(), swipeRefreshGroups);
        recyclerView.setAdapter(mAdapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NewGroupActivity.class));
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                Toast.makeText(getApplicationContext(), "Option not implemented yet", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.price:
                mAdapter.filterByPrice(priceMode);
                priceMode = !priceMode;
                return true;

            case R.id.myGroups:
                if (filterMode) {
                    mAdapter.loadList();
                    item.setIcon(getDrawable(R.drawable.ic_group_white_48dp));
                    item.setTitle(R.string.my_groups);
                } else {
                    mAdapter.findByUser();
                    item.setIcon(getDrawable(R.drawable.ic_all_inclusive_white_48dp));
                    item.setTitle(R.string.all_posts);
                }
                filterMode = !filterMode;
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Si hacemos click abrimos la activity con más detalles
     * @param group
     */
    @Override
    public void onItemClick(Group group) {
        Intent intent = new Intent(this, GroupDetailActivity.class);

        intent.putExtra("group", group);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }

    public static void refreshView() {
        if (myDataset.size() == 0) {
            backText.setVisibility(View.VISIBLE);

        } else {
            backText.setVisibility(View.INVISIBLE);
        }
    }
}