package com.javiersolercanto.batoimobile.ui.groups;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.DeleteHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PutHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.Group;
import com.javiersolercanto.batoimobile.model.User;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class NotificationsActivity extends AppCompatActivity {

    private TextView tvHead;
    private ListView listView;
    private Button btnReject;
    private Button btnAccept;

    private boolean mode;
    private Group group;
    private ArrayList<User> allUsers;
    private ArrayList<User> selectedUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        // Comprobamos si tenemos que mirar las notificaciones o los miembros
        mode = getIntent().getBooleanExtra("mode", true);
        group = (Group) getIntent().getSerializableExtra("group");
        allUsers = (ArrayList<User>) getIntent().getSerializableExtra("users");

        setUI();
    }

    private void setUI() {
        tvHead = findViewById(R.id.tvHead);
        listView = findViewById(R.id.listView);
        btnAccept = findViewById(R.id.btnAccept);
        btnReject = findViewById(R.id.btnReject);

        selectedUsers = new ArrayList<>();

        // Dependiendo del modo cargamos unos botones u otros
        if (!mode) {
            btnReject.setText(R.string.cancel);
            btnAccept.setText(R.string.remove);
            tvHead.setText(R.string.members);
        }

        ArrayAdapter<User> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, allUsers);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        listView.setAdapter(adapter);

        // Aquí podemos seleccionar los usuarios y rodearlos con un aura azul
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (view.getBackground() == null || view.getBackground().equals(getDrawable(R.drawable.btn_rounded_cancel))) {
                    view.setBackground(getDrawable(R.drawable.btn_rounded_cancel));
                    selectedUsers.add(adapter.getItem(position));
                }
                else {
                    view.setBackground(null);
                    selectedUsers.remove(adapter.getItem(position));
                }
            }
        });

        // Si queremos realizar alguna acción deberemos haber seleccionado un usuario mínimo
        btnAccept.setOnClickListener(v -> {
            if (selectedUsers.size() > 0) {
                if (mode) {
                    for (User user : selectedUsers) {
                        acceptUser(user);
                    }
                } else {
                    for (User user : selectedUsers) {
                        deleteUser(user);
                    }
                }
                finish();
            } else Toast.makeText(getApplicationContext(), R.string.must_select_an_user, Toast.LENGTH_SHORT).show();
        });

        // Accion realizada desde la pantalla de miembros para rechazar su solicitud
        btnReject.setOnClickListener(v -> {
            if (mode) {
                if (selectedUsers.size() > 0) {
                    if (mode) {
                        for (User user : selectedUsers) {
                            deleteUser(user);
                        }
                    }
                } else Toast.makeText(getApplicationContext(), R.string.must_select_an_user, Toast.LENGTH_SHORT).show();
            }
            finish();
        });
    }

    /**
     * Si aceptamos un usuario cambiaremos su valor de confirmado a true en la tabla belong
     * @param user
     */
    private void acceptUser(User user) {
        PutHttpDataTask putHttpDataTask = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(Boolean output) {
                if (!output) Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
            }
        });
        try {
            putHttpDataTask.execute(new URL(ConnectionDB.url + "belong/" + group.getIdgroup() + "/" + user.getIduser() + "/" + 1));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Si eliminamos un usuario eliminaremos ese registro de la tabla belong
     * @param user
     */
    private void deleteUser(User user) {
        DeleteHttpDataTask deleteHttpDataTask = new DeleteHttpDataTask(getApplicationContext(), new DeleteHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(Boolean output) {
                if (!output) Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
            }
        });
        try {
            deleteHttpDataTask.execute(new URL(ConnectionDB.url + "belong/" + group.getIdgroup() + "/" + user.getIduser()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
