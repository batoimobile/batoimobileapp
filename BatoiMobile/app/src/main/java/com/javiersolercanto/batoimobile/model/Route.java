package com.javiersolercanto.batoimobile.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Route implements Serializable {

    private int idroute;
    private Address from;
    private Address to;
    private LocalDateTime date;
    private int price;
    private int seats;
    private String observation;
    private User author;

    public Route(int idroute, Address from, Address to, LocalDateTime date, int price, int seats, String observation, User author) {
        this.idroute = idroute;
        this.from = from;
        this.to = to;
        this.date = date;
        this.price = price;
        this.seats = seats;
        this.observation = observation;
        this.author = author;
    }

    public int getIdroute() {
        return idroute;
    }

    public void setIdroute(int idroute) {
        this.idroute = idroute;
    }

    public Address getFrom() {
        return from;
    }

    public void setFrom(Address from) {
        this.from = from;
    }

    public Address getTo() {
        return to;
    }

    public void setTo(Address to) {
        this.to = to;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Route{" +
                "idroute=" + idroute +
                ", from=" + from +
                ", to=" + to +
                ", date=" + date +
                ", price=" + price +
                ", seats=" + seats +
                ", observation='" + observation + '\'' +
                ", author=" + author +
                '}';
    }
}
