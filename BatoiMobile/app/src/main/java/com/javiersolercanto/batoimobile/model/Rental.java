package com.javiersolercanto.batoimobile.model;

import java.io.Serializable;

public class Rental implements Serializable {

    private int idrental;
    private String description;
    private int price;
    private float distance;
    private User author;
    private Address address;

    public Rental(int idrental, String description, int price, User author, Address address) {
        this.idrental = idrental;
        this.description = description;
        this.price = price;
        this.author = author;
        this.address = address;
    }

    public int getIdrental() {
        return idrental;
    }

    public void setIdrental(int idrental) {
        this.idrental = idrental;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Rental{" +
                "idrental=" + idrental +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", author=" + author.toString() +
                ", address=" + address.toString() +
                '}';
    }
}
