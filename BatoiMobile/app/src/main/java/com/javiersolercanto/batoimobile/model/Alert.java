package com.javiersolercanto.batoimobile.model;

public class Alert {

    private int id;
    private String title;
    private String content;
    private User creator;

    public Alert(int id, String title, String content, User creator) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.creator = creator;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }
}
