package com.javiersolercanto.batoimobile.asyncTask.product;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.model.Category;
import com.javiersolercanto.batoimobile.model.Product;
import com.javiersolercanto.batoimobile.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetProductHttpDataTask extends GetHttpDataTask {

    public GetProductHttpDataTask(Context appContext, AsyncResponse asyncResponse) {
        super(appContext,asyncResponse);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected ArrayList<Object> doInBackground(Object... objects) {
        HttpURLConnection urlConnection = null;
        ArrayList<Object> searchResult = null;
        try {
            if ((urlConnection = stablishConnection((URL) objects[0])) != null) {
                String resultStream = readStream(urlConnection.getInputStream());

                searchResult = new ArrayList<>();

                JSONObject response = new JSONObject(resultStream);
                JSONArray replies = response.getJSONArray("products");
                for (int i = 0; i < replies.length(); i++) {
                    JSONObject product = replies.getJSONObject(i);
                    searchResult.add(new Product(
                            product.getInt("idproduct"),
                            product.getString("name"),
                            product.getInt("price"),
                            product.getString("description"),
                            (new Category(
                                product.getInt("idcategory"),
                                product.getString("c_name")
                            )),
                            (new User(
                                product.getInt("iduser"),
                                product.getString("u_name"),
                                product.getString("nickname"),
                                product.getString("password"),
                                product.getString("email"),
                                product.getString("role")
                            )))
                    );
                    if (isCancelled()) break;
                }
            }
        } catch (IOException e) {
            Log.i("IOException", e.getMessage());
        } catch (JSONException e) {
            Log.i("JSONException", e.getMessage());
            e.printStackTrace();
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return searchResult;
    }
}
