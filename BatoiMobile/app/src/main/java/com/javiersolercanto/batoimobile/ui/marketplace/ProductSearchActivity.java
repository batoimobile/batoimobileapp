package com.javiersolercanto.batoimobile.ui.marketplace;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.category.GetCategoryHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.product.GetProductHttpDataTask;
import com.javiersolercanto.batoimobile.model.Category;
import com.javiersolercanto.batoimobile.model.ConnectionDB;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ProductSearchActivity extends AppCompatActivity {

    private EditText etName;
    private Spinner spCategory;
    private Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_search);

        setUI();
    }

    private void setUI() {
        etName = findViewById(R.id.etName);
        spCategory = findViewById(R.id.spCategory);
        btnSearch = findViewById(R.id.btnSearch);

        loadSpinner();
        btnSearch.setOnClickListener(v -> search());
    }

    /**
     * Dependiendo de los filtros seleccionados construiremos una URL u otra y lanzaremos la petición
     */
    private void search() {
        if (etName.getText().length() > 0 || !((Category) spCategory.getSelectedItem()).getName().equals(getString(R.string.head_spinner_category)) ) {
            URL url = null;
            if (etName.getText().length() > 0 && !((Category) spCategory.getSelectedItem()).getName().equals(getString(R.string.head_spinner_category))) {
                try {
                    url = new URL(ConnectionDB.url + "product/getByNameAndCategory/" + etName.getText().toString() + "/" + ((Category) spCategory.getSelectedItem()).getIdcategory());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else if (etName.getText().length() == 0 && !((Category) spCategory.getSelectedItem()).getName().equals(getString(R.string.head_spinner_category))) {
                try {
                   url = new URL(ConnectionDB.url + "product/getByCategory/" + ((Category) spCategory.getSelectedItem()).getIdcategory());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    url = new URL(ConnectionDB.url + "product/getByName/" + etName.getText().toString());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }

            /**
             * Devolvemos a la activity principal los datos
             */
            GetProductHttpDataTask getProductHttpDataTask = new GetProductHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                @Override
                public void processFinish(ArrayList<?> output) {
                    if (output != null) {
                        setResult(RESULT_OK, new Intent().putExtra("products", output));
                        finish();
                    } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                }
            });
            getProductHttpDataTask.execute(url);

        } else Toast.makeText(getApplicationContext(), R.string.no_filters_error, Toast.LENGTH_SHORT).show();
    }

    /**
     * Cargamos las categorias
     */
    private void loadSpinner() {
        GetCategoryHttpDataTask getCategoryHttpDataTask = new GetCategoryHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    if (!output.isEmpty() && output.get(0) != null) {
                        ArrayAdapter<Category> spinnerAdapter = new ArrayAdapter<Category>(getApplicationContext(), android.R.layout.simple_list_item_1, (ArrayList<Category>) output);
                        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spCategory.setAdapter(spinnerAdapter);
                    }
                }
            }
        });
        try {
            getCategoryHttpDataTask.execute(new URL(ConnectionDB.url + "category/"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
