package com.javiersolercanto.batoimobile.asyncTask;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public abstract class GetHttpDataTask extends AsyncTask<Object, Void, ArrayList<?>> {

    private final int CONNECTION_TIMEOUT = 5000;
    private final int READ_TIMEOUT = 10000;

    protected WeakReference<Context> appContext;
    private WeakReference<ProgressBar> progress;
    private AsyncResponse asyncResponse;

    public interface AsyncResponse {
        void processFinish(ArrayList<?> output);
    }

    protected GetHttpDataTask(Context context, ProgressBar pb, AsyncResponse asyncResponse) {
        this.appContext = new WeakReference<>(context);
        this.progress = new WeakReference<>(pb);
        this.asyncResponse = asyncResponse;
    }

    protected GetHttpDataTask(Context context, AsyncResponse asyncResponse) {
        this.appContext = new WeakReference<>(context);
        this.asyncResponse = asyncResponse;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (progress != null) progress.get().setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(ArrayList<?> objects) {
        super.onPostExecute(objects);
        if (progress != null) progress.get().setVisibility(View.GONE);
        asyncResponse.processFinish(objects);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (progress != null) progress.get().setVisibility(View.GONE);
    }

    /**
     * Método que nos permite establecer la conexión
     * @param url
     * @return
     */
    protected HttpURLConnection stablishConnection(URL url) {
        HttpURLConnection urlConnection = null;

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Connection", "close");
            urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
            urlConnection.setReadTimeout(READ_TIMEOUT);

            if (isNetworkAvailable(urlConnection)) return urlConnection;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Método que nos permite leer la respuesta de la petición
     * @param in
     * @return
     */
    protected String readStream(InputStream in) {
        StringBuilder sb = new StringBuilder();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            String nextLine = "";

            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);
            }

        } catch (IOException e) {
            Log.i("IOException-ReadStream", e.getMessage());
        }

        return sb.toString();
    }

    /**
     * Comprobamos si tenemos conexión disponible
     * @param urlConnection
     * @return
     */
    protected boolean isNetworkAvailable(HttpURLConnection urlConnection) {
        ConnectivityManager cm = (ConnectivityManager) this.appContext.get()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            try {
                urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                urlConnection.connect();

                return urlConnection.getResponseCode() == 200;

            } catch (MalformedURLException e1) {
                Log.i("conErr", "Connection error");
            } catch (IOException e) {
                Log.i("conErr", "Connection error");
            }
        }
        return false;
    }
}