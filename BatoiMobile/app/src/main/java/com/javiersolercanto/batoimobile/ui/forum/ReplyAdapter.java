package com.javiersolercanto.batoimobile.ui.forum;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.DeleteHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PostHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PutHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.reaction.GetReactionHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.reply.GetReplyHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Post;
import com.javiersolercanto.batoimobile.model.Reply;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class ReplyAdapter extends RecyclerView.Adapter<ReplyAdapter.MyViewHolder> {

    public interface OnItemClickListener {

        void onItemClick(Reply reply);
        void onItemLongClick(Reply reply);
    }

    private ArrayList<Reply> myDataset;
    private OnItemClickListener listener;
    private SwipeRefreshLayout swipeRefreshLayout;

    private static Context appContext;
    private static Post post;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        CardView item;
        ImageView bgCorrect;
        TextView tvResponse;
        TextView tvAuthor;
        TextView tvLikes;

        private GetReactionHttpDataTask getReactionHttpDataTask;
        private GetReplyHttpDataTask getReplyHttpDataTask;

        private PostHttpDataTask postHttpDataTask;
        private DeleteHttpDataTask deleteHttpDataTask;
        private PutHttpDataTask putHttpDataTask;

        private static int CONNECTION_PROBLEM = 2;
        private static int CORRECT = 0;

        public MyViewHolder(View view) {
            super(view);

            this.item = view.findViewById(R.id.item);
            this.bgCorrect = view.findViewById(R.id.bgCorrect);
            this.tvResponse = view.findViewById(R.id.tvResponse);
            this.tvAuthor = view.findViewById(R.id.tvAuthor);
            this.tvLikes = view.findViewById(R.id.tvLikes);
        }

        public void bind(final Reply reply, final OnItemClickListener listener) {
            this.tvResponse.setText(reply.getResponse());
            this.tvAuthor.setText(reply.getAuthor().getName());
            this.tvLikes.setText(String.valueOf(reply.getLikes()));

            reply.setPost(post);

            // Comprobamos si el usuario actual ha dado me gusta para poner el corazón azul
            getReactionHttpDataTask = new GetReactionHttpDataTask(appContext, new GetHttpDataTask.AsyncResponse() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
                @Override
                public void processFinish(ArrayList<?> output) {
                    if (output != null)
                        if (!output.isEmpty())
                            tvLikes.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0, R.drawable.ic_favorite_blue_24dp, 0);
                }
            });
            try {
                getReactionHttpDataTask.execute(new URL(ConnectionDB.url + "reaction/" + CurrentUser.getCurrentUser().getIduser() + "/" + reply.getIdreply()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            // Si es la respuesta correcta será de color azul
            if (reply.isCorrect())
                this.bgCorrect.setVisibility(View.VISIBLE);

            else
                this.bgCorrect.setVisibility(View.INVISIBLE);

            // Creamos el listener para poder dar me gusta o quitarlo
            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(reply);
                    getReactionHttpDataTask = new GetReactionHttpDataTask(appContext, new GetHttpDataTask.AsyncResponse() {
                        @Override
                        public void processFinish(ArrayList<?> output) {
                            if (output != null) {
                                if (output.isEmpty()) {     // LIKE
                                    postHttpDataTask = new PostHttpDataTask(appContext, new PostHttpDataTask.AsyncResponse() {
                                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
                                        @Override
                                        public void processFinish(Integer output) {
                                            if (output != CONNECTION_PROBLEM)
                                                if (output == CORRECT) {
                                                    tvLikes.setText(String.valueOf(Integer.parseInt(tvLikes.getText().toString()) + 1));
                                                    tvLikes.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                                            0,0, R.drawable.ic_favorite_blue_24dp, 0);
                                                }
                                        }
                                    });
                                    try {
                                        postHttpDataTask.execute(new URL(ConnectionDB.url + "reaction/" +
                                                CurrentUser.getCurrentUser().getIduser() + "/" + reply.getIdreply()));
                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                    }

                                } else {     //DISLIKE
                                    deleteHttpDataTask = new DeleteHttpDataTask(appContext, new DeleteHttpDataTask.AsyncResponse() {
                                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
                                        @Override
                                        public void processFinish(Boolean output) {
                                            if (output) {
                                                tvLikes.setText(String.valueOf(Integer.parseInt(tvLikes.getText().toString()) - 1));
                                                tvLikes.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                                        0,0, R.drawable.ic_favorite_border_blue_24dp, 0);
                                            } else Toast.makeText(appContext,
                                                    R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    try {
                                        deleteHttpDataTask.execute(new URL(ConnectionDB.url + "reaction/" +
                                                CurrentUser.getCurrentUser().getIduser() + "/" + reply.getIdreply()));
                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    });
                    try {
                        getReactionHttpDataTask.execute(new URL(ConnectionDB.url + "reaction/" +
                                CurrentUser.getCurrentUser().getIduser() + "/" + reply.getIdreply()));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            });

            // El long click nos permite si somos propietarios del post marcarla como correcta
            this.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onItemLongClick(reply);
                    if (post.getAuthor().getIduser() == CurrentUser.getCurrentUser().getIduser()) {
                        if (!reply.isCorrect()) {   // REPPLY CLICKED IS NOT CORRECT -> MAKE IT CORRECT
                            getReplyHttpDataTask = new GetReplyHttpDataTask(appContext, new GetHttpDataTask.AsyncResponse() {
                                @Override
                                public void processFinish(ArrayList<?> output) {
                                    if (output != null) {
                                        if (!output.isEmpty() && output.get(0) != null) { // REPLY CORRECT FOUNDED -> MAKE INCORRECT THAT REPLY
                                            putHttpDataTask = new PutHttpDataTask(appContext, new PutHttpDataTask.AsyncResponse() {
                                                @Override
                                                public void processFinish(Boolean output) {
                                                    if (output) {   // REPLY MAKE IT INCORRECT CORRECTLY -> MAKE REPLY CLICKED CORRECT
                                                        putHttpDataTask = new PutHttpDataTask(appContext, new PutHttpDataTask.AsyncResponse() {
                                                            @Override
                                                            public void processFinish(Boolean output) {
                                                                if (output) {
                                                                    bgCorrect.setVisibility(View.VISIBLE);
                                                                    reply.setCorrect(true);
                                                                } else Toast.makeText(appContext, R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                            }
                                                        });
                                                        try {
                                                            putHttpDataTask.execute(new URL(ConnectionDB.url + "reply/1/" + reply.getIdreply()));
                                                        } catch (MalformedURLException e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else Toast.makeText(appContext, R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                            try {
                                                putHttpDataTask.execute(new URL(ConnectionDB.url + "reply/0/" + ((Reply) output.get(0)).getIdreply()));
                                            } catch (MalformedURLException e) {
                                                e.printStackTrace();
                                            }
                                        } else { // NO REPLY CORRECT FOUNDED -> MAKE NEW REPLY CORRECT -> MAKE POST RESOLVED
                                            putHttpDataTask = new PutHttpDataTask(appContext, new PutHttpDataTask.AsyncResponse() {
                                                @Override
                                                public void processFinish(Boolean output) {
                                                    if (output) { // REPLY MAKE CORRECT SUCCESFUL -> MAKE POST CORRECT
                                                        putHttpDataTask = new PutHttpDataTask(appContext, new PutHttpDataTask.AsyncResponse() {
                                                            @Override
                                                            public void processFinish(Boolean output) {
                                                                bgCorrect.setVisibility(View.VISIBLE);
                                                                reply.setCorrect(true);
                                                                post.setResolved(true);
                                                            }
                                                        });
                                                        try {
                                                            putHttpDataTask.execute(new URL(ConnectionDB.url + "post/1/" + post.getId()));
                                                        } catch (MalformedURLException e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else Toast.makeText(appContext, R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                            try {
                                                putHttpDataTask.execute(new URL(ConnectionDB.url + "reply/1/" + reply.getIdreply()));
                                            } catch (MalformedURLException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            });
                            try {
                                getReplyHttpDataTask.execute(new URL(ConnectionDB.url + "reply/findCorrect/" + post.getId() + "/" + reply.getIdreply()));
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        } else {    // REPLY CLICKED IS CORRECT? -> MAKE INCORRECT REPLY AND POST NO RESOLVED
                            putHttpDataTask = new PutHttpDataTask(appContext, new PutHttpDataTask.AsyncResponse() {
                                @Override
                                public void processFinish(Boolean output) {
                                    if (output) {
                                        putHttpDataTask = new PutHttpDataTask(appContext, new PutHttpDataTask.AsyncResponse() {
                                            @Override
                                            public void processFinish(Boolean output) {
                                                if (output) {
                                                    bgCorrect.setVisibility(View.INVISIBLE);
                                                    reply.setCorrect(false);
                                                    post.setResolved(false);
                                                } else Toast.makeText(appContext, R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        try {
                                            putHttpDataTask.execute(new URL(ConnectionDB.url + "post/0/" + post.getId()));
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                    } else Toast.makeText(appContext, R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                }
                            });
                            try {
                                putHttpDataTask.execute(new URL(ConnectionDB.url + "reply/0/" + reply.getIdreply()));
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    return false;
                }
            });
        }
    }

    ReplyAdapter(Post post, ArrayList<Reply> myDataset, OnItemClickListener listener, Context context, SwipeRefreshLayout swipeRefreshLayout) {
        ReplyAdapter.post = post;

        this.myDataset = myDataset;
        this.listener = listener;
        appContext = context;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    /**
     * Cargamos la lista de la base de datos
     */
    public void loadList() {
        myDataset.clear();
        swipeRefreshLayout.setRefreshing(true);
        GetReplyHttpDataTask getReplyHttpDataTask = new GetReplyHttpDataTask(appContext, new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                myDataset.addAll((ArrayList<Reply>) output);
                notifyDataSetChanged();
                PostDetailActivity.refreshView();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        try {
            getReplyHttpDataTask.execute(new URL(ConnectionDB.url + "reply/getByPost/" + post.getId()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void deleteItem(Reply reply) {
        int size = myDataset.size();

        for (int i = 0; i < size; i++) {
            myDataset.remove(reply);
            this.notifyDataSetChanged();
        }
    }

    public void restoreList() {
        myDataset.clear();
        loadList();
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.reply_item, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {

        viewHolder.bind(myDataset.get(position), listener);
    }

    @Override
    public int getItemCount() {

        return myDataset.size();
    }
}
