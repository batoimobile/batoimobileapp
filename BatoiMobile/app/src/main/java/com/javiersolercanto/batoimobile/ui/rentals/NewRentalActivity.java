package com.javiersolercanto.batoimobile.ui.rentals;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PostHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PutHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.address.GetAddressHttpDataTask;
import com.javiersolercanto.batoimobile.model.Address;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Rental;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class NewRentalActivity extends AppCompatActivity {

    private GetAddressHttpDataTask getAddressHttpDataTask;
    private PostHttpDataTask postHttpDataTask;
    private PutHttpDataTask putHttpDataTask;

    private TextView tvHead;
    private EditText etDescription;
    private EditText etPrice;
    private Button btnAccept;
    private Button btnCancel;

    private AutocompleteSupportFragment autocompleteFragment;
    private Address address;
    private Rental rental;

    private static boolean mode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_rental);

        // Comprobamos si se trata de una edición o de una inserción
        if (getIntent() != null) mode = (rental = (Rental) getIntent().getSerializableExtra("rental")) != null;
        else mode = false;

        setUI();
    }

    private void setUI() {
        tvHead = findViewById(R.id.tvHead);
        etDescription = findViewById(R.id.etDescription);
        etPrice = findViewById(R.id.etPrice);
        btnAccept = findViewById(R.id.btnAccept);
        btnCancel = findViewById(R.id.btnCancel);

        // Initialize the AutocompleteSupportFragment.
        autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        if (!Places.isInitialized()) Places.initialize(getApplicationContext()
                , "AIzaSyBFrnw6KbpN68Cv0odVA-AAAxPPll7_Oys");

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME));
        autocompleteFragment.setHint(getString(R.string.search_address));
        autocompleteFragment.setCountry("ES");

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                address = new Address(place.getName(), place.getLatLng().latitude, place.getLatLng().longitude);
            }

            @Override
            public void onError(Status status) {
            }
        });

        if (mode) {
            tvHead.setText(R.string.edit_rental);
            fillData();
        }

        /**
         * Siempre comprobaremos si las direcciones ya están en la base de datos antes de guardar la inserción o la edición del producto
         */
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                if (checkFields()) {
                    if (!mode) {    // EDICIÓN
                        getAddressHttpDataTask = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                            @Override
                            public void processFinish(ArrayList<?> output) {
                                if (output != null) {
                                    if (output.size() > 0 && output.get(0) != null) {
                                        int idaddress = ((Address) output.get(0)).getIdaddress();
                                        postHttpDataTask = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                            @Override
                                            public void processFinish(Integer output) {
                                                if (output != -1) finish();
                                                else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        try {
                                            postHttpDataTask.execute(new URL(ConnectionDB.url+ "rental/" + etDescription.getText().toString() + "/" + etPrice.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser() + "/" + idaddress));
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        postHttpDataTask = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                            @Override
                                            public void processFinish(Integer output) {
                                                if (output != -1) {
                                                    int idaddress = output;
                                                    postHttpDataTask = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                        @Override
                                                        public void processFinish(Integer output) {
                                                            if (output != -1) finish();
                                                            else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                        }
                                                    });
                                                    try {
                                                        postHttpDataTask.execute(new URL(ConnectionDB.url+ "rental/" + etDescription.getText().toString() + "/" + etPrice.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser() + "/" + idaddress));
                                                    } catch (MalformedURLException e) {
                                                        e.printStackTrace();
                                                    }
                                                } else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        try {
                                            postHttpDataTask.execute(new URL(ConnectionDB.url + "address/" + address.getName() + "/" + address.getLat() + "/" + address.getLng()));
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                            }
                        });
                        try {
                            getAddressHttpDataTask.execute(new URL(ConnectionDB.url + "address/getByName/" + address.getName()));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    } else { // INSERCIÓN
                        if (rental.getAddress().getName().equals(address.getName())) {
                            putHttpDataTask = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                @Override
                                public void processFinish(Boolean output) {
                                    if (output) finish();
                                    else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                }
                            });
                            try {
                                putHttpDataTask.execute(new URL(ConnectionDB.url + "rental/" + etDescription.getText().toString() + "/" + etPrice.getText().toString() + "/" + rental.getAddress().getIdaddress() + "/" + rental.getIdrental()));
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        } else {
                            postHttpDataTask = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                @Override
                                public void processFinish(Integer output) {
                                    if (output != -1) {
                                        Log.i("cicle", output + "");
                                        address.setIdaddress(output);
                                        putHttpDataTask = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                            @Override
                                            public void processFinish(Boolean output) {
                                                if (output) finish();
                                                else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        try {
                                            putHttpDataTask.execute(new URL(ConnectionDB.url + "rental/" + etDescription.getText().toString() + "/" + etPrice.getText().toString() + "/" + address.getIdaddress() + "/" + rental.getIdrental()));
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                    } else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                }
                            });
                            try {
                                postHttpDataTask.execute(new URL(ConnectionDB.url + "address/" + address.getName() + "/" + address.getLat() + "/" + address.getLng()));
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });

        btnCancel.setOnClickListener(v -> finish());
    }

    private void fillData() {
        autocompleteFragment.setText(rental.getAddress().getName());
        etDescription.setText(rental.getDescription());
        etPrice.setText(String.valueOf(rental.getPrice()));
    }

    /**
     * Comprobamos que los campos han sido rellenados coerrectamente
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private boolean checkFields() {
        etDescription.setBackground(getDrawable(R.drawable.et_rounded_login));
        etPrice.setBackground(getDrawable(R.drawable.et_rounded_login));
        if (address != null) {
            if (etDescription.getText().toString().length() > 15 && etDescription.length() < 200) {
                if (etPrice.getText().toString().length() > 0 && Float.parseFloat(etPrice.getText().toString()) > 1) {
                    return true;
                } else {
                    Toast.makeText(getApplicationContext(), R.string.not_0_price, Toast.LENGTH_SHORT).show();
                    etPrice.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                }
            } else {
                Toast.makeText(getApplicationContext(), R.string.max_character_200, Toast.LENGTH_SHORT).show();
                etDescription.setBackground(getDrawable(R.drawable.et_rounded_login_error));
            }
        } else Toast.makeText(getApplicationContext(), R.string.address_necessary, Toast.LENGTH_SHORT).show();
        return false;
    }
}
