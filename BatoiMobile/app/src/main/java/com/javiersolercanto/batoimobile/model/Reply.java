package com.javiersolercanto.batoimobile.model;

import java.io.Serializable;

public class Reply implements Serializable {

    private int idreply;
    private String response;
    private int likes;
    private boolean correct;
    private User author;
    private Post post;

    public Reply(int idreply, String response, int likes, boolean correct, User author, Post post) {
        this.idreply = idreply;
        this.response = response;
        this.likes = likes;
        this.correct = correct;
        this.author = author;
        this.post = post;
    }

    public Reply(String response, User author, Post post) {
        this.response = response;
        this.author = author;
        this.post = post;
    }

    public int getIdreply() {
        return idreply;
    }

    public void setIdreply(int idreply) {
        this.idreply = idreply;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Override
    public String toString() {
        return "Reply{" +
                "idreply=" + idreply +
                ", response='" + response + '\'' +
                ", likes=" + likes +
                ", correct=" + correct +
                ", author=" + author.toString() +
                ", post=" + post.toString() +
                '}';
    }
}
