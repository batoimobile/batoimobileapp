package com.javiersolercanto.batoimobile.asyncTask.belong;

import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.model.Belong;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetBelongHttpDataTask extends GetHttpDataTask {

    public GetBelongHttpDataTask(Context appContext, ProgressBar progressBar, AsyncResponse asyncResponse) {
        super(appContext, progressBar, asyncResponse);
    }

    public GetBelongHttpDataTask(Context appContext, AsyncResponse asyncResponse) {
        super(appContext, asyncResponse);
    }

    @Override
    protected ArrayList<Object> doInBackground(Object... objects) {
        HttpURLConnection urlConnection = null;
        ArrayList<Object> searchResult = null;
        try {
            if ((urlConnection = stablishConnection((URL) objects[0])) != null) {
                String resultStream = readStream(urlConnection.getInputStream());

                searchResult = new ArrayList<>();

                JSONObject response = new JSONObject(resultStream);
                JSONArray belonges = response.getJSONArray("belonges");
                for (int i = 0; i < belonges.length(); i++) {
                    JSONObject belong = belonges.getJSONObject(i);
                    searchResult.add(
                            new Belong(
                                    belong.getInt("idgroup"),
                                    belong.getInt("iduser"),
                                    belong.getInt("confirmed") != 0
                            )
                    );
                    if (isCancelled()) break;
                }
            }
        } catch (IOException e) {
            Log.i("IOException", e.getMessage());
        } catch (JSONException e) {
            Log.i("JSONException", e.getMessage());
            e.printStackTrace();
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return searchResult;
    }
}
