package com.javiersolercanto.batoimobile.model;

public class Reaction {

    private int iduser;
    private int idreply;

    public Reaction(int iduser, int idreply) {
        this.iduser = iduser;
        this.idreply = idreply;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public int getIdreply() {
        return idreply;
    }

    public void setIdreply(int idreply) {
        this.idreply = idreply;
    }
}
