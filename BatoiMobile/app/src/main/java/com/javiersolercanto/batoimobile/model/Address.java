package com.javiersolercanto.batoimobile.model;

import java.io.Serializable;

public class Address implements Serializable {

    private int idaddress;
    private String name;
    private double lat;
    private double lng;

    public Address() {
    }

    public Address(int idaddress, String name, double lat, double lng) {
        this.idaddress = idaddress;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

    public Address(String name, double lat, double lng) {
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

    public int getIdaddress() {
        return idaddress;
    }

    public void setIdaddress(int idaddress) {
        this.idaddress = idaddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
