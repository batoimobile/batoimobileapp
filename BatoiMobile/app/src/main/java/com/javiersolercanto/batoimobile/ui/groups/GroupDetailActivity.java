package com.javiersolercanto.batoimobile.ui.groups;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.DeleteHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PostHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.belong.GetBelongHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.group.GetGroupHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.user.GetUserHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Group;
import com.javiersolercanto.batoimobile.tools.Tools;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class GroupDetailActivity extends AppCompatActivity {

    private TextView tvName;
    private TextView tvRoute;
    private TextView tvPrice;
    private TextView tvShift;
    private TextView tvDays;
    private TextView tvMembers;
    private TextView tvObservations;
    private Button btnEditContact;
    private ImageButton btnDeleteShare;
    private ImageButton btnLeave;
    private LinearLayout linearNotifications;
    private LinearLayout linearLeave;
    private TextView tvNotifications;

    private Group group;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);

        setUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setUI() {
        tvName = findViewById(R.id.tvName);
        tvRoute = findViewById(R.id.tvRoute);
        tvShift = findViewById(R.id.tvShift);
        tvDays = findViewById(R.id.tvDays);
        tvPrice = findViewById(R.id.tvPrice);
        tvMembers = findViewById(R.id.tvMembers);
        tvObservations = findViewById(R.id.tvObservations);
        btnEditContact = findViewById(R.id.btnEditContact);
        btnDeleteShare = findViewById(R.id.btnDeleteShare);
        btnLeave = findViewById(R.id.btnLeave);
        linearLeave = findViewById(R.id.linearLeave);
        linearNotifications = findViewById(R.id.linearNotifications);
        tvNotifications = findViewById(R.id.tvNotifications);

        group = (Group) getIntent().getSerializableExtra("group");

        fillData();

        // Botón contactar / editar
        btnEditContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnEditContact.getText().toString().equals(getString(R.string.contact))) {  // CONTACTAR
                    if (linearLeave.getVisibility() == View.VISIBLE) contact();
                    else {
                        // CREAMOS LA PETICIÓN
                        GetBelongHttpDataTask getBelongHttpDataTask = new GetBelongHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                            @Override
                            public void processFinish(ArrayList<?> output) {
                                if (output != null) {
                                    if (!output.isEmpty() && output.get(0) != null) {
                                        createDialogContact(getString(R.string.request_sent),getString(R.string.send_an_email), GroupDetailActivity.this);
                                    } else {
                                        createDialogRequest(getString(R.string.sure_about_group_request), null, GroupDetailActivity.this);
                                    }
                                }
                            }
                        });
                        try {
                            getBelongHttpDataTask.execute(new URL(ConnectionDB.url + "belong/getIfPetitionSent/" + group.getIdgroup() + "/" + CurrentUser.getCurrentUser().getIduser()));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                } else {    // EDITAR
                    Intent intent = new Intent(getApplicationContext(), NewGroupActivity.class);
                    intent.putExtra("group", group);
                    startActivity(intent);
                    finish();
                }
            }
        });

        // Borrar o compartir dependiendo del usuario actual
        btnDeleteShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentUser.getCurrentUser().getIduser() != group.getAdmin().getIduser()) share();
                else {
                   createDialogDelete(getString(R.string.sure_about_delete_grouo), null, GroupDetailActivity.this);
                }
            }
        });

        // Si somos los propietarios abrimos el panel de notificaciones
        tvNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (group.getAdmin().getIduser() == CurrentUser.getCurrentUser().getIduser() && Integer.parseInt(tvNotifications.getText().toString()) > 0) {
                    GetUserHttpDataTask getUserHttpDataTask = new GetUserHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                        @Override
                        public void processFinish(ArrayList<?> output) {
                            if (output != null) {
                                if (!output.isEmpty() && output.get(0) != null) {
                                    Intent intent = new Intent(getApplicationContext(), NotificationsActivity.class);
                                    intent.putExtra("group", group);
                                    intent.putExtra("users", output);
                                    startActivity(intent);

                                } else Toast.makeText(getApplicationContext(), R.string.no_notifications, Toast.LENGTH_SHORT).show();
                            } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                        }
                    });
                    try {
                        getUserHttpDataTask.execute(new URL(ConnectionDB.url + "user/getUsersPetitions/" + group.getIdgroup()));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        btnLeave.setOnClickListener(v -> {
            createDialogLeave(getString(R.string.sure_about_leaving_group), null, GroupDetailActivity.this);
        });

        // Si somos los propietarios abrimos el panel de miembros
        tvMembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (group.getAdmin().getIduser() == CurrentUser.getCurrentUser().getIduser() && group.getMembers() > 1) {
                    GetUserHttpDataTask getUserHttpDataTask = new GetUserHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                        @Override
                        public void processFinish(ArrayList<?> output) {
                            if (output != null) {
                                if (!output.isEmpty() && output.get(0) != null) {
                                    Intent intent = new Intent(getApplicationContext(), NotificationsActivity.class);
                                    intent.putExtra("group", group);
                                    intent.putExtra("users", output);
                                    intent.putExtra("mode", false);
                                    startActivity(intent);

                                } else Toast.makeText(getApplicationContext(), R.string.no_notifications, Toast.LENGTH_SHORT).show();
                            } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                        }
                    });
                    try {
                        getUserHttpDataTask.execute(new URL(ConnectionDB.url + "user/getUsersOfGroup/" + group.getIdgroup() + "/" + CurrentUser.getCurrentUser().getIduser()));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        // Ponemos los botones según si somos propietarios o no
        if (CurrentUser.getCurrentUser().getIduser() == group.getAdmin().getIduser()) {
            btnEditContact.setText(R.string.edit);
            btnDeleteShare.setImageDrawable(getDrawable(R.drawable.ic_delete_white_24dp));
            linearNotifications.setVisibility(View.VISIBLE);
            loadNotifications();
        }

        isUserAtGroup();
    }

    /**
     * Comprobaremos si el usuario está en el grupo para así tener la opción de abandonarlo
     */
    private void isUserAtGroup() {
        GetBelongHttpDataTask getBelongHttpDataTask = new GetBelongHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null && !output.isEmpty() && output.get(0) != null) linearLeave.setVisibility(View.VISIBLE);
            }
        });
        try {
            if (CurrentUser.getCurrentUser().getIduser() != group.getAdmin().getIduser()) getBelongHttpDataTask.execute(new URL(ConnectionDB.url + "belong/" + group.getIdgroup() + "/" + CurrentUser.getCurrentUser().getIduser()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        GetGroupHttpDataTask getGroupHttpDataTask = new GetGroupHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null && !output.isEmpty() && output.get(0) != null) {
                    group = (Group) output.get(0);
                    loadNotifications();
                    fillData();
                }
            }
        });
        try {
            getGroupHttpDataTask.execute(new URL(ConnectionDB.url + "group/" + group.getIdgroup()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cargaremos todas las peticiones que tenga este grupo no resueltas
     */
    private void loadNotifications() {
        GetBelongHttpDataTask getBelongHttpDataTask = new GetBelongHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    tvNotifications.setText(String.valueOf(output.size()));
                }
                else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
            }
        });
        try {
            getBelongHttpDataTask.execute(new URL(ConnectionDB.url + "belong/getNotifications/" + group.getIdgroup()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Rellenamos los datos
     */
    private void fillData() {
        tvName.setText(group.getName());
        tvRoute.setText(group.getFrom().getName() + " - " + group.getTo().getName());
        tvPrice.setText(String.valueOf(group.getPrice()));
        tvMembers.setText(String.valueOf(group.getMembers()));
        tvObservations.setText(group.getObservations());
        tvDays.setText(setDays(group.getDays()));
    }

    /**
     * Igual que en el adapter convertiremos los días
     * @param days
     * @return
     */
    private String setDays(String days) {
        String[] sDays = days.split("");
        days = "";
        for (int i = 0; i < sDays.length; i++) {
            if (!sDays[i].equals("0")) {
                String day = "";
                switch (i) {
                    case 0: day = "L"; break;
                    case 1: day = "M"; break;
                    case 2: day = "X"; break;
                    case 3: day = "J"; break;
                    case 4: day = "V"; break;
                }
                days += day + " ";
            }
        }

        return days;
    }

    public void contact() {
        Tools.composeEmail(new String[] {group.getAdmin().getEmail()}, "I want to join your group - " + CurrentUser.getCurrentUser().getName(), "I'm interested on your travel group from " + group.getFrom().getName() + " to " + group.getTo().getName(), this);
    }

    private void share() {
        Tools.composeEmail(new String[] {}, "Hey! Look at this - ", "This group may interest you: " + group.getFrom().getName() + " to " + group.getTo().getName(), this);
    }

    /**
     * Diálogo para abandonar el grupo
     * @param title
     * @param message
     * @param activity
     */
    public void createDialogLeave(String title, String message, Context activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);

        if (message != null) builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DeleteHttpDataTask deleteHttpDataTask = new DeleteHttpDataTask(getApplicationContext(), new DeleteHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(Boolean output) {
                        if (!output)
                            Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
                try {
                    deleteHttpDataTask.execute(new URL(ConnectionDB.url + "belong/" + group.getIdgroup() + "/" + CurrentUser.getCurrentUser().getIduser()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Diálogo para borrar el grupo
     * @param title
     * @param message
     * @param activity
     */
    public void createDialogDelete(String title, String message, Context activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);

        if (message != null) builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DeleteHttpDataTask deleteHttpDataTask = new DeleteHttpDataTask(getApplicationContext(), new DeleteHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(Boolean output) {
                        if (output) finish();
                        else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                    }
                });
                try {
                    deleteHttpDataTask.execute(new URL(ConnectionDB.url + "group/" + group.getIdgroup()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Diálogo para contactar con el admin del grupo
     * @param title
     * @param message
     * @param activity
     */
    public void createDialogContact(String title, String message, Context activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);

        if (message != null) builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                contact();
            }
        });
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Diálogo para mandar una petición de unirse al grupo
     * @param title
     * @param message
     * @param activity
     */
    public void createDialogRequest(String title, String message, Context activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);

        if (message != null) builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                PostHttpDataTask postHttpDataTask = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(Integer output) {
                        if (output != -1) {
                            createDialogContact(getString(R.string.great), getString(R.string.send_an_email), GroupDetailActivity.this);
                        }
                    }
                });
                try {
                    postHttpDataTask.execute(new URL(ConnectionDB.url + "belong/" + group.getIdgroup() + "/" + CurrentUser.getCurrentUser().getIduser() + "/0"));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
