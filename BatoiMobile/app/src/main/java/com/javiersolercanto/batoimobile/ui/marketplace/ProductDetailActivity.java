package com.javiersolercanto.batoimobile.ui.marketplace;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.DeleteHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Product;
import com.javiersolercanto.batoimobile.tools.Tools;

import java.net.MalformedURLException;
import java.net.URL;

public class ProductDetailActivity extends AppCompatActivity {

    private ImageView image;
    private TextView tvName;
    private TextView tvPrice;
    private TextView tvDescription;
    private Button btnEditContact;
    private ImageButton btnDeleteShare;

    private Product product;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        if (getIntent() != null) product = (Product) getIntent().getSerializableExtra("product");

        setUI();
    }

    /**
     * Dependiendo de si el usuario actual es el propietario podremos realizar más acciones
     */

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setUI() {
        image = findViewById(R.id.ivImage);
        tvName = findViewById(R.id.tvName);
        tvPrice = findViewById(R.id.tvPrice);
        tvDescription = findViewById(R.id.tvDescription);

        btnEditContact = findViewById(R.id.btnEditContact);
        btnDeleteShare = findViewById(R.id.btnDeleteShare);

        if (CurrentUser.getCurrentUser().getIduser() == product.getAuthor().getIduser()) {
            btnEditContact.setText(R.string.edit);
            btnDeleteShare.setImageDrawable(getDrawable(R.drawable.ic_delete_white_24dp));
        }

        fillData();

        /**
         * Acción editar o contactar dependiendo del usuario
         */
        btnEditContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnEditContact.getText().toString().equals(getString(R.string.contact))) Tools.composeEmail(new String[] {product.getAuthor().getEmail()}, "I'm interested on your " + product.getName() + "- " + CurrentUser.getCurrentUser().getName(), "I'm interested on your product " + product.getName() + ", can you please give me more information? Thanks", ProductDetailActivity.this);
                else {
                    Intent intent = new Intent(getApplicationContext(), NewProductActivity.class);
                    intent.putExtra("product", product);
                    startActivity(intent);
                    finish();
                }
            }
        });

        /**
         * Acción borrar o compartir dependiendo del usuario
         */
        btnDeleteShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentUser.getCurrentUser().getIduser() != product.getAuthor().getIduser()) Tools.composeEmail(new String[] {}, "Hey! Look at this " + product.getName() + " - " + CurrentUser.getCurrentUser().getName(), "This may interest you.", ProductDetailActivity.this);
                else {
                    createDialogDelete(getString(R.string.sure_about_delete_product), null, ProductDetailActivity.this);
                }
            }
        });
    }

    /**
     * Rellenamos los campos con los datos
     */
    private void fillData() {
        image.setImageResource(getImage(product.getCategory().getName()));
        tvDescription.setText(product.getDescription());
        tvName.setText(product.getName());
        tvPrice.setText(String.valueOf(product.getPrice()));
    }

    /**
     * Dependiendo de la categoria que tenga el producto cargaremos una imagen u otra
     * @param name
     * @return
     */
    private int getImage(String name) {
        switch (name) {
            case "Informática": return R.drawable.computer;
            case "Estética": return R.drawable.scissors;
            case "Salud": return R.drawable.health;
            case "Administración": return R.drawable.finance;
            case "Cocina": return R.drawable.kitchen;
            case "Servicios": return R.drawable.house;
            default: return R.drawable.stuff;
        }
    }

    /**
     * Crearemos el dialogo antes de borrar el producto
     * @param title
     * @param message
     * @param activity
     */
    public void createDialogDelete(String title, String message, Context activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);

        if (message != null) builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DeleteHttpDataTask deleteHttpDataTask = new DeleteHttpDataTask(getApplicationContext(), new DeleteHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(Boolean output) {
                        if (output) finish();
                        else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                    }
                });
                try {
                    deleteHttpDataTask.execute(new URL(ConnectionDB.url + "product/" + product.getIdproduct()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
