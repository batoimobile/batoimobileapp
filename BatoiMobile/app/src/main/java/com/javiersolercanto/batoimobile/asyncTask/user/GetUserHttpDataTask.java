package com.javiersolercanto.batoimobile.asyncTask.user;

import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetUserHttpDataTask extends GetHttpDataTask {

    public GetUserHttpDataTask(Context appContext, ProgressBar progressBar, AsyncResponse asyncResponse) {
        super(appContext, progressBar, asyncResponse);
    }

    public GetUserHttpDataTask(Context appContext, AsyncResponse asyncResponse) {
        super(appContext,asyncResponse);
    }

    @Override
    protected ArrayList<Object> doInBackground(Object... objects) {
        HttpURLConnection urlConnection = null;
        ArrayList<Object> searchResult = null;
        try {
            if ((urlConnection = stablishConnection((URL) objects[0])) != null) {
                String resultStream = readStream(urlConnection.getInputStream());

                searchResult = new ArrayList<>();

                JSONObject response = new JSONObject(resultStream);
                //if (response.getBoolean("response")) {
                JSONArray users = response.getJSONArray("users");
                for (int i = 0; i < users.length(); i++) {
                    JSONObject user = users.getJSONObject(i);
                    searchResult.add(new User(
                            user.getInt("iduser"),
                            user.getString("name"),
                            user.getString("nickname"),
                            user.getString("password"),
                            user.getString("email"),
                            user.getString("role")
                        )
                    );
                    if (isCancelled()) break;
                }
            }
            //} else {
                Log.i("conErr", "Connection error");
            //}
        } catch (IOException e) {
            Log.i("IOException", e.getMessage());
        } catch (JSONException e) {
            Log.i("JSONException", e.getMessage());
            e.printStackTrace();
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return searchResult;
    }
}
