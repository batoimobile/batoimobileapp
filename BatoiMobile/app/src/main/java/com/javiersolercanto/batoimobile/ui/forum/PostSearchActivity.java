package com.javiersolercanto.batoimobile.ui.forum;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.category.GetCategoryHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.post.GetPostHttpDataTask;
import com.javiersolercanto.batoimobile.model.Category;
import com.javiersolercanto.batoimobile.model.ConnectionDB;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class PostSearchActivity extends AppCompatActivity {

    private EditText etSubject;
    private CheckBox cbResolved;
    private Spinner spCategory;
    private Button btnSearch;

    private GetCategoryHttpDataTask getCategoryHttpDataTask;
    private GetPostHttpDataTask getPostHttpDataTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_search);

        setUI();
    }

    private void setUI() {
        etSubject = findViewById(R.id.etSubject);
        spCategory = findViewById(R.id.spCategory);
        cbResolved = findViewById(R.id.cbResolved);
        btnSearch = findViewById(R.id.btnSearch);

        // Cargamos el spinner de categorías
        getCategoryHttpDataTask = new GetCategoryHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    if (!output.isEmpty() && output.get(0) != null) {
                        ArrayAdapter<Category> spinnerAdapter = new ArrayAdapter<Category>(getApplicationContext(), android.R.layout.simple_list_item_1, (ArrayList<Category>) output);
                        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spCategory.setAdapter(spinnerAdapter);
                    }
                }
            }
        });
        try {
            getCategoryHttpDataTask.execute(new URL(ConnectionDB.url + "category/"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        // Nos permite buscar los post según los filtros elegidos
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subject = etSubject.getText().toString();
                boolean isResolved = cbResolved.isChecked();
                int category = ((Category) spCategory.getSelectedItem()).getIdcategory();

                getPostHttpDataTask = new GetPostHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(ArrayList<?> output) {
                        if (output != null) {
                            setResult(RESULT_OK, new Intent().putExtra("posts", output));
                            finish();
                        }
                    }
                });
                URL url;
                if ((url = getURL(subject, isResolved, category)) != null)
                    getPostHttpDataTask.execute(url);
                else
                    Toast.makeText(getApplicationContext(), R.string.filters_needed_posts, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Dependiendo los filtros escogidos nos creará una URL específica para poder buscar esos posts
     * @param subject
     * @param isResolved
     * @param category
     * @return
     */
    private URL getURL(String subject, boolean isResolved, int category) {
        try {
            if (subject.length() > 0 && category != 1 && isResolved) {
                return new URL(ConnectionDB.url + "post/getByFilters/" + subject + "/" + category + "/" + 1);
            } else if (subject.length() > 0 && category != 1) {
                return new URL(ConnectionDB.url + "post/getBySubjectAndCategory/" + subject + "/" + category);
            } else if (subject.length() > 0 && isResolved) {
                return new URL(ConnectionDB.url + "post/getBySubjectAndResolved/" + subject + "/" + 1);
            } else if (subject.length() > 0) {
                return new URL(ConnectionDB.url + "post/getBySubject/" + subject);
            } else if (category != 1 && isResolved) {
                return new URL(ConnectionDB.url + "post/getByCategoryAndResolved/" + category + "/" + 1);
            } else if (category != 1) {
                return new URL(ConnectionDB.url + "post/getByCategory/" + category);
            } else if (isResolved) {
                return new URL(ConnectionDB.url + "post/getByResolved/" + 1);
            } else {
                return null;
            }
        } catch (MalformedURLException e) {
            return null;
        }
    }
}
