package com.javiersolercanto.batoimobile.model;

import java.io.Serializable;

public class Product implements Serializable {

    private int idproduct;
    private String name;
    private int price;
    private String description;
    private Category category;
    private User author;

    public Product(int idproduct, String name, int price, String description, Category category, User author) {
        this.idproduct = idproduct;
        this.name = name;
        this.price = price;
        this.description = description;
        this.category = category;
        this.author = author;
    }

    public int getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
