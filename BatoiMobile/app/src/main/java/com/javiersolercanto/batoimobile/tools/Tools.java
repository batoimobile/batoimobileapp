package com.javiersolercanto.batoimobile.tools;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

public class Tools {

    /**
     * Con este método podemo crear un email.
     * @param addresses
     * @param subject
     * @param text
     * @param context
     */
    public static void composeEmail(String[] addresses, String subject, String text, Context context) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));

        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, text);

        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    /**
     * Accedemos al fichero SharedPreferences para poder guardar una id de usuario a recordar
     * @param context
     * @param id
     */
    public static void rememberUser(Context context, int id) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt("remember", id);
        edit.commit();
    }
}
