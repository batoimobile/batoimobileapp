package com.javiersolercanto.batoimobile.asyncTask.group;

import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.model.Address;
import com.javiersolercanto.batoimobile.model.Group;
import com.javiersolercanto.batoimobile.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetGroupHttpDataTask extends GetHttpDataTask {

    public GetGroupHttpDataTask(Context appContext, ProgressBar progressBar, AsyncResponse asyncResponse) {
        super(appContext, progressBar, asyncResponse);
    }

    public GetGroupHttpDataTask(Context appContext, AsyncResponse asyncResponse) {
        super(appContext,asyncResponse);
    }

    @Override
    protected ArrayList<Object> doInBackground(Object... objects) {
        HttpURLConnection urlConnection = null;
        ArrayList<Object> searchResult = null;
        try {
            if ((urlConnection = stablishConnection((URL) objects[0])) != null) {
                searchResult = new ArrayList<>();
                //if (super.isNetworkAvailable()) {
                    String resultStream = readStream(urlConnection.getInputStream());
                    JSONObject response = new JSONObject(resultStream);
                    JSONArray replies = response.getJSONArray("groups");
                    for (int i = 0; i < replies.length(); i++) {
                        JSONObject group = replies.getJSONObject(i);
                        searchResult.add(new Group
                                (
                                        group.getInt("idgroup"),
                                        group.getString("name"),
                                        new Address(
                                                group.getInt("idfrom"),
                                                group.getString("f_name"),
                                                group.getDouble("f_lat"),
                                                group.getDouble("f_lng")
                                        ),
                                        new Address(
                                                group.getInt("idto"),
                                                group.getString("t_name"),
                                                group.getDouble("t_lat"),
                                                group.getDouble("t_lng")
                                        ),
                                        group.getInt("shift") != 0,
                                        group.getString("days"),
                                        group.getInt("price"),
                                        group.getString("observations"),
                                        new User(
                                                group.getInt("iduser"),
                                                group.getString("name"),
                                                group.getString("nickname"),
                                                group.getString("password"),
                                                group.getString("email"),
                                                group.getString("role")
                                        ),
                                        group.getInt("members")
                                )
                        );
                        if (isCancelled()) break;
                        urlConnection.disconnect();
                    }
                //} else Log.i("cicle", "ERRORASO");
            }
        } catch (IOException e) {
            Log.i("IOException", e.getMessage());
            Log.i("cicle", "ERRORRRRR");
        } catch (JSONException e) {
            Log.i("JSONException", e.getMessage());
            Log.i("cicle", "ERRORRRRR");
            e.printStackTrace();
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return searchResult;
    }
}
