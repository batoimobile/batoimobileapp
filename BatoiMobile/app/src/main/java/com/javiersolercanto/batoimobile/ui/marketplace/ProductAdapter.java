package com.javiersolercanto.batoimobile.ui.marketplace;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.product.GetProductHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Product;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    public interface OnItemClickListener {

        void onItemClick(Product product);
    }

    private ArrayList<Product> myDataset;
    private ProductAdapter.OnItemClickListener listener;
    private Context appContext;
    private SwipeRefreshLayout swipeRefreshLayout;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvPrice;
        TextView tvName;
        ImageView ivImage;

        public MyViewHolder(View view) {
            super(view);

            this.tvPrice = view.findViewById(R.id.tvPrice);
            this.tvName = view.findViewById(R.id.tvName);
            this.ivImage = view.findViewById(R.id.imageView);
        }

        public void bind(final Product product, final OnItemClickListener listener) {
            this.tvName.setText(product.getName());
            this.tvPrice.setText(String.valueOf(product.getPrice()));
            this.ivImage.setBackgroundResource(getImage(product.getCategory().getName()));

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(product);
                }
            });
        }

        /**
         * Dependiendo de la categoria que tenga el producto cargaremos una imagen u otra
         * @param name
         * @return
         */
        private int getImage(String name) {
            switch (name) {
                case "Informática": return R.drawable.computer;
                case "Estética": return R.drawable.scissors;
                case "Salud": return R.drawable.health;
                case "Administración": return R.drawable.finance;
                case "Cocina": return R.drawable.kitchen;
                case "Servicios": return R.drawable.house;
                default: return R.drawable.stuff;
            }
        }
    }

    public ProductAdapter(ArrayList<Product> myDataset, ProductAdapter.OnItemClickListener listener, Context appContext, SwipeRefreshLayout swipeRefreshLayout) {

        this.myDataset = myDataset;
        this.listener = listener;
        this.appContext = appContext;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    public void loadList() {
        ProductsActivity.isFiltering = false;
        myDataset.clear();
        swipeRefreshLayout.setRefreshing(true);

        GetProductHttpDataTask getProductHttpDataTask =
                new GetProductHttpDataTask(this.appContext, new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    myDataset.addAll((ArrayList<Product>) output);
                    notifyDataSetChanged();
                    ProductsActivity.refreshView();
                } else Toast.makeText(appContext, R.string.server_lost
                        , Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        try {
            getProductHttpDataTask.execute(new URL(ConnectionDB.url + "product"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Este método nos permite saber que productos a creado el usuario actual
     */
    public void findByUser() {
        ArrayList<Product> newList = new ArrayList<>();
        for (Product product : myDataset) {
            if (product.getAuthor().getIduser() == CurrentUser.getCurrentUser().getIduser()) {
                newList.add(product);
            }
        }
        myDataset.clear();
        myDataset.addAll(newList);
        this.notifyDataSetChanged();
        ProductsActivity.refreshView();
    }

    /**
     * Ordenamos los productos de > o de <
     * @param mode
     */
    public void filterByPrice(boolean mode) {
        Collections.sort(myDataset, new MyPriceCompare(mode));

        notifyDataSetChanged();
        ProductsActivity.refreshView();
    }

    public void setData(ArrayList<Product> data) {
        myDataset.clear();
        myDataset.addAll(data);
        this.notifyDataSetChanged();
        ProductsActivity.refreshView();
    }

    /**
     * Nuestra clase que como en los grupos nos permite ordenar por precio
     */
    static class MyPriceCompare implements Comparator<Product> {

        private boolean mode;

        MyPriceCompare(boolean mode) {
            this.mode = mode;
        }

        @Override
        public int compare(Product p1, Product p2) {
            if (mode) {
                if(p1.getPrice() > p2.getPrice()){
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if(p1.getPrice() < p2.getPrice()){
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myDataset.get(position), listener);
    }

    @Override
    public int getItemCount() {

        return myDataset.size();
    }
}
