package com.javiersolercanto.batoimobile.ui.forum;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.post.GetPostHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Post;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {

    public interface OnItemClickListener {

        void onItemClick(Post post);
    }

    private ArrayList<Post> myDataset;
    private OnItemClickListener listener;
    private SwipeRefreshLayout swipeRefreshLayout;

    private static Context appContext;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvCategory;
        TextView tvSubject;
        TextView tvAuthor;
        TextView tvReplies;

        public MyViewHolder(View view) {
            super(view);

            this.tvCategory = view.findViewById(R.id.tvCategory);
            this.tvSubject = view.findViewById(R.id.tvSubject);
            this.tvAuthor = view.findViewById(R.id.tvAuthor);
            this.tvReplies = view.findViewById(R.id.tvReplies);
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public void bind(final Post post, final OnItemClickListener listener) {
            this.tvCategory.setText(post.getCategory().getName());
            this.tvSubject.setText(post.getSubject());
            this.tvAuthor.setText(post.getAuthor().getName());
            this.tvReplies.setText(String.valueOf(post.getReplies()));

            // Si está resuelto fondo azul
            if (post.isResolved()) this.itemView.setBackground(appContext.getDrawable(R.drawable.bg_correct_reply));

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(post);
                }
            });
        }
    }

    PostAdapter(ArrayList<Post> myDataset, OnItemClickListener listener, Context appContext, SwipeRefreshLayout swipeRefreshLayout) {
        this.myDataset = myDataset;
        this.listener = listener;
        PostAdapter.appContext = appContext;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    /**
     * Cargamos la lista realizando una petición a la base de datos
     */
    public void loadList() {
        myDataset.clear();
        swipeRefreshLayout.setRefreshing(true);

        GetPostHttpDataTask getPostHttpDataTask = new GetPostHttpDataTask(appContext, new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    myDataset.addAll((ArrayList<Post>) output);
                    notifyDataSetChanged();
                    PostsActivity.refreshView();
                } else Toast.makeText(appContext, R.string.server_lost, Toast.LENGTH_LONG).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        try {
            getPostHttpDataTask.execute(new URL(ConnectionDB.url + "post"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que nos permite dejar en la lista solo los post que pertenezcan al usuario actual
     */
    public void findByUser() {
        swipeRefreshLayout.setRefreshing(true);
        ArrayList<Post> newList = new ArrayList<>();
        for (Post post : myDataset) {
            if (post.getAuthor().getIduser() == CurrentUser.getCurrentUser().getIduser()) {
                newList.add(post);
            }
        }
        myDataset.clear();
        myDataset.addAll(newList);
        swipeRefreshLayout.setRefreshing(false);
        this.notifyDataSetChanged();
        PostsActivity.refreshView();
    }

    public void setData(ArrayList<Post> data) {
        myDataset.clear();
        myDataset.addAll(data);
        this.notifyDataSetChanged();
        PostsActivity.refreshView();
    }

    public void restoreList() {
        myDataset.clear();
        loadList();
        this.notifyDataSetChanged();
        PostsActivity.refreshView();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false);

        return new MyViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myDataset.get(position), listener);
    }

    @Override
    public int getItemCount() { return myDataset.size(); }
}
