package com.javiersolercanto.batoimobile.asyncTask;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class RSSAlert extends AsyncTask<Object, Void, ArrayList<?>> {

    private final int CONNECTION_TIMEOUT = 5000;
    private final int READ_TIMEOUT = 10000;

    protected WeakReference<Context> appContext;
    private GetHttpDataTask.AsyncResponse asyncResponse;

    public interface AsyncResponse {
        void processFinish(ArrayList<?> output);
    }

    public RSSAlert(Context context, GetHttpDataTask.AsyncResponse asyncResponse) {
        this.appContext = new WeakReference<>(context);
        this.asyncResponse = asyncResponse;
    }

    @Override
    protected ArrayList<?> doInBackground(Object... objects) {
        HttpURLConnection urlConnection = null;
        ArrayList<Object> searchResult = null;
        try {
            if ((urlConnection = stablishConnection((URL) objects[0])) != null) {
                searchResult = new ArrayList<>();
                InputStream inputStream = urlConnection.getInputStream();
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = builderFactory.newDocumentBuilder();

                Document xmlDoc = builder.parse(inputStream);

                Element root = xmlDoc.getDocumentElement();
                Node channel =  root.getChildNodes().item(1);

                searchResult.add(channel
                        .getChildNodes()
                        .item(30)
                        .getChildNodes()
                        .item(0)
                        .getTextContent());

                return searchResult;
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return searchResult;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ArrayList<?> objects) {
        super.onPostExecute(objects);

        asyncResponse.processFinish(objects);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    protected HttpURLConnection stablishConnection(URL url) {
        HttpURLConnection urlConnection = null;

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Connection", "close");
            urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
            urlConnection.setReadTimeout(READ_TIMEOUT);

            if (isNetworkAvailable(urlConnection)) return urlConnection;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    protected String readStream(InputStream in) {
        StringBuilder sb = new StringBuilder();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            String nextLine = "";

            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);
            }

        } catch (IOException e) {
            Log.i("IOException-ReadStream", e.getMessage());
        }

        return sb.toString();
    }

    protected boolean isNetworkAvailable(HttpURLConnection urlConnection) {
        ConnectivityManager cm =
                (ConnectivityManager) this.appContext.get().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            try {
                urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                urlConnection.connect();

                return urlConnection.getResponseCode() == 200;

            } catch (MalformedURLException e1) {
                Log.i("conErr", "Connection error");
            } catch (IOException e) {
                Log.i("conErr", "Connection error");
            }
        }
        return false;
    }
}
