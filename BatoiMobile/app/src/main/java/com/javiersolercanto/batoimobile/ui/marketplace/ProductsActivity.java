package com.javiersolercanto.batoimobile.ui.marketplace;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.model.Product;

import java.util.ArrayList;

public class ProductsActivity extends AppCompatActivity implements ProductAdapter.OnItemClickListener, View.OnClickListener{

    private RecyclerView recyclerView;
    private ProductAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private static final int RESULT_CODE = 0;
    public static boolean isFiltering = false;
    private static boolean priceMode = false;
    private static boolean filterMode = false;
    private static TextView backText;
    public static ArrayList<Product> myDataset;

    private SwipeRefreshLayout swipeRefreshProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.setTitle(getString(R.string.products));

        setContentView(R.layout.activity_products);

        setUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_menu, menu);
        return true;
    }

    private void setUI() {
        backText = findViewById(R.id.tvNotFound);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

        swipeRefreshProducts = findViewById(R.id.swipeRefreshProducts);
        swipeRefreshProducts.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshProducts.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.loadList();
                mAdapter.notifyDataSetChanged();
                refreshView();
                Toast.makeText(getApplicationContext(), R.string.products_refresh, Toast.LENGTH_SHORT).show();
                swipeRefreshProducts.setRefreshing(false);
            }
        });

        isFiltering = false;
        myDataset = new ArrayList<>();

        mAdapter = new ProductAdapter(myDataset, this, getApplicationContext(), swipeRefreshProducts);
        recyclerView.setAdapter(mAdapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NewProductActivity.class));
            }
        });
    }

    /**
     * Tratamos las opciones seleccionadas en el menú
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                startActivityForResult(new Intent(this, ProductSearchActivity.class), RESULT_CODE);
                return true;

            case R.id.price:
                mAdapter.filterByPrice(priceMode);
                priceMode = !priceMode;
                return true;

            case R.id.myProducts:
                if (!filterMode) {
                    mAdapter.findByUser();
                    item.setTitle(R.string.all_products);
                    item.setIcon(R.drawable.ic_all_inclusive_white_48dp);
                } else {
                    mAdapter.loadList();
                    item.setTitle(R.string.my_products);
                    item.setIcon(R.drawable.ic_shopping_basket_black_48dp);
                }
                filterMode = !filterMode;
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Obtenemos la lista de productos que hemos filtrado en la activity search
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        isFiltering = true;
        if (requestCode == RESULT_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                myDataset = (ArrayList<Product>) data.getSerializableExtra("products");
                mAdapter.setData(myDataset);
            }
        }
    }

    /**
     * Si clickamos un producto abriremos más información sobre él
     * @param product
     */
    @Override
    public void onItemClick(Product product) {
        Intent intent = new Intent(this, ProductDetailActivity.class);

        intent.putExtra("product", product);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isFiltering)
            mAdapter.loadList();
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static void refreshView() {
        if (myDataset.size() == 0) {
            backText.setVisibility(View.VISIBLE);

        } else {
            backText.setVisibility(View.INVISIBLE);
        }
    }
}