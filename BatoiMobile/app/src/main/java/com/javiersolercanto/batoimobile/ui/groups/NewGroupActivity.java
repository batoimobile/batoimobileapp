package com.javiersolercanto.batoimobile.ui.groups;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PostHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PutHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.address.GetAddressHttpDataTask;
import com.javiersolercanto.batoimobile.model.Address;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Group;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class NewGroupActivity extends AppCompatActivity {

    private TextView tvHead;
    private EditText etName;
    private CheckBox cbM;
    private CheckBox cbTu;
    private CheckBox cbW;
    private CheckBox cbTh;
    private CheckBox cbF;
    private EditText etPrice;
    private ToggleButton tbShift;
    private EditText etObservations;
    private Button btnAccept;
    private Button btnCancel;

    private AutocompleteSupportFragment fgTo;
    private AutocompleteSupportFragment fgFrom;

    private int[] daysSelected = {0,0,0,0,0};
    private boolean mode;
    private Group group;
    private Address to;
    private Address from;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);

        if (getIntent() != null) mode = (group = (Group) getIntent().getSerializableExtra("group")) != null;
        else mode = false;

        setUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setUI() {
        tvHead = findViewById(R.id.tvHead);
        etName = findViewById(R.id.etName);
        cbM = findViewById(R.id.cbMonday);
        cbTu = findViewById(R.id.cbTuesday);
        cbW = findViewById(R.id.cbWednesday);
        cbTh = findViewById(R.id.cbThursday);
        cbF = findViewById(R.id.cbFriday);
        tbShift = findViewById(R.id.tbShift);
        etPrice = findViewById(R.id.etPrice);
        etObservations = findViewById(R.id.etObservations);
        btnAccept = findViewById(R.id.btnAccept);
        btnCancel = findViewById(R.id.btnCancel);

        loadFragments();

        if (mode) fillData();

        cbM.setOnCheckedChangeListener((buttonView, isChecked) -> daysSelected[0] = isChecked ?  1 : 0);
        cbTu.setOnCheckedChangeListener((buttonView, isChecked) -> daysSelected[1] = isChecked ?  1 : 0);
        cbW.setOnCheckedChangeListener((buttonView, isChecked) -> daysSelected[2] = isChecked ?  1 : 0);
        cbTh.setOnCheckedChangeListener((buttonView, isChecked) -> daysSelected[3] = isChecked ?  1 : 0);
        cbF.setOnCheckedChangeListener((buttonView, isChecked) -> daysSelected[4] = isChecked ?  1 : 0);

        btnAccept.setOnClickListener(v -> saveChanges());
        btnCancel.setOnClickListener(v -> finish());
    }

    /**
     * Dependiendo del modo guardaremos o editaremos, siempre comprobaremos que las direcciones seleccionadas
     * no estén ya en la base de dataos para así evitar duiplicidad de datos por eso tenemos que realizar
     * tantas comprobaciones
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void saveChanges() {
        if (checkFields()) {
            if (!mode) {
                GetAddressHttpDataTask getFrom = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(ArrayList<?> output) {
                        if (output != null) {
                            if (output.size() > 0 && output.get(0) != null) {
                                from.setIdaddress(((Address) output.get(0)).getIdaddress());
                                GetAddressHttpDataTask getTo = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                                    @Override
                                    public void processFinish(ArrayList<?> output) {
                                        if (output != null) {
                                            if (output.size() > 0 && output.get(0) != null) {
                                                to.setIdaddress(((Address) output.get(0)).getIdaddress());
                                                PostHttpDataTask postGroupNoAddress = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                    @Override
                                                    public void processFinish(Integer output) {
                                                        if (output != -1) {
                                                            PostHttpDataTask postBelong = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                @Override
                                                                public void processFinish(Integer output) {
                                                                    if (output != -1) finish();
                                                                    else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                }
                                                            });
                                                            try {
                                                                postBelong.execute(new URL(ConnectionDB.url + "belong/" + output + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                            } catch (MalformedURLException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                        else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                                try {
                                                    Log.i("cicle", new URL(ConnectionDB.url + "group/" + etName.getText().toString() + "/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + getDaysSelected() + "/" + etPrice.getText().toString() + "/" + (!tbShift.getText().equals(getString(R.string.morning))) + "/" + etObservations.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()).toString());
                                                    postGroupNoAddress.execute(new URL(ConnectionDB.url + "group/" + etName.getText().toString() + "/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + getDaysSelected() + "/" + etPrice.getText().toString() + "/" + (!tbShift.getText().equals(getString(R.string.morning))) + "/" + etObservations.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                } catch (MalformedURLException e) {
                                                    e.printStackTrace();
                                                }
                                            } else {
                                                PostHttpDataTask postTo = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                    @Override
                                                    public void processFinish(Integer output) {
                                                        if (output != -1) {
                                                            to.setIdaddress(output);
                                                            PostHttpDataTask postGroup = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                @Override
                                                                public void processFinish(Integer output) {
                                                                    if (output != -1) {
                                                                        PostHttpDataTask postBelong = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                            @Override
                                                                            public void processFinish(Integer output) {
                                                                                if (output != -1) finish();
                                                                                else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        });
                                                                        try {
                                                                            postBelong.execute(new URL(ConnectionDB.url + "belong/" + output + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                                        } catch (MalformedURLException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                    else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                }
                                                            });
                                                            try {
                                                                postGroup.execute(new URL(ConnectionDB.url + "group/" + etName.getText().toString() + "/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + getDaysSelected() + "/" + etPrice.getText().toString() + "/" + (!tbShift.getText().equals(getString(R.string.morning))) + "/" + etObservations.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                            } catch (MalformedURLException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }
                                                });
                                                try {
                                                    postTo.execute(new URL(ConnectionDB.url + "address/" + to.getName() + "/" + to.getLat() + "/" + to.getLng()));
                                                } catch (MalformedURLException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                                    }
                                });
                                try {
                                    getTo.execute(new URL(ConnectionDB.url + "address/getByName/" + to.getName()));
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                PostHttpDataTask postFrom = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                    @Override
                                    public void processFinish(Integer output) {
                                        if (output != -1) {
                                            from.setIdaddress(output);
                                            GetAddressHttpDataTask getTo = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                                                @Override
                                                public void processFinish(ArrayList<?> output) {
                                                    if (output != null) {
                                                        if (output.size() > 0 && output.get(0) != null) {
                                                            to.setIdaddress(((Address) output.get(0)).getIdaddress());
                                                            PostHttpDataTask postGroupNoAddress = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                @Override
                                                                public void processFinish(Integer output) {
                                                                    if (output != -1) {
                                                                        PostHttpDataTask postBelong = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                            @Override
                                                                            public void processFinish(Integer output) {
                                                                                if (output != -1) finish();
                                                                                else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        });
                                                                        try {
                                                                            postBelong.execute(new URL(ConnectionDB.url + "belong/" + output + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                                        } catch (MalformedURLException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                    else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                }
                                                            });
                                                            try {
                                                                postGroupNoAddress.execute(new URL(ConnectionDB.url + "group/" + etName.getText().toString() + "/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + getDaysSelected() + "/" + etPrice.getText().toString() + "/" + (!tbShift.getText().equals(getString(R.string.morning))) + "/" + etObservations.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                            } catch (MalformedURLException e) {
                                                                e.printStackTrace();
                                                            }
                                                        } else {
                                                            PostHttpDataTask postTo = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                @Override
                                                                public void processFinish(Integer output) {
                                                                    if (output != -1) {
                                                                        to.setIdaddress(output);
                                                                        PostHttpDataTask postGroup = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                            @Override
                                                                            public void processFinish(Integer output) {
                                                                                if (output != -1) {
                                                                                    PostHttpDataTask postBelong = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                                        @Override
                                                                                        public void processFinish(Integer output) {
                                                                                            if (output != -1) finish();
                                                                                            else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                                        }
                                                                                    });
                                                                                    try {
                                                                                        postBelong.execute(new URL(ConnectionDB.url + "belong/" + output + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                                                    } catch (MalformedURLException e) {
                                                                                        e.printStackTrace();
                                                                                    }
                                                                                }
                                                                                else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        });
                                                                        try {
                                                                            postGroup.execute(new URL(ConnectionDB.url + "group/" + etName.getText().toString() + "/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + getDaysSelected() + "/" + etPrice.getText().toString() + "/" + (!tbShift.getText().equals(getString(R.string.morning))) + "/" + etObservations.getText().toString() + "/" + CurrentUser.getCurrentUser().getIduser()));
                                                                        } catch (MalformedURLException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                            try {
                                                                postTo.execute(new URL(ConnectionDB.url + "address/" + to.getName() + "/" + to.getLat() + "/" + to.getLng()));
                                                            } catch (MalformedURLException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                            try {
                                                getTo.execute(new URL(ConnectionDB.url + "address/getByName/" + to.getName()));
                                            } catch (MalformedURLException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                                try {
                                    postFrom.execute(new URL(ConnectionDB.url + "address/" + from.getName() + "/" + from.getLat() + "/" + from.getLng()));
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                    }
                });
                try {
                    getFrom.execute(new URL(ConnectionDB.url + "address/getByName/" + from.getName()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                GetAddressHttpDataTask getFrom = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(ArrayList<?> output) {
                        if (output != null) {
                            if (output.size() > 0 && output.get(0) != null) {
                                from.setIdaddress(((Address) output.get(0)).getIdaddress());
                                GetAddressHttpDataTask getTo = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                                    @Override
                                    public void processFinish(ArrayList<?> output) {
                                        if (output != null) {
                                            if (output.size() > 0 && output.get(0) != null) {
                                                to.setIdaddress(((Address) output.get(0)).getIdaddress());
                                                PutHttpDataTask putGroupNoAddress = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                                    @Override
                                                    public void processFinish(Boolean output) {
                                                        if (output) finish();
                                                        else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                                try {
                                                    Log.i("cicle", new URL(ConnectionDB.url + "group/" + etName.getText().toString() + "/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + getDaysSelected() + "/" + etPrice.getText().toString() + "/" + (!tbShift.getText().equals(getString(R.string.morning))) + "/" + etObservations.getText().toString() + "/" + group.getIdgroup()).toString());
                                                    putGroupNoAddress.execute(new URL(ConnectionDB.url + "group/" + etName.getText().toString() + "/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + getDaysSelected() + "/" + etPrice.getText().toString() + "/" + (!tbShift.getText().equals(getString(R.string.morning))) + "/" + etObservations.getText().toString() + "/" + group.getIdgroup()));
                                                } catch (MalformedURLException e) {
                                                    e.printStackTrace();
                                                }
                                            } else {
                                                PostHttpDataTask postTo = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                    @Override
                                                    public void processFinish(Integer output) {
                                                        if (output != -1) {
                                                            to.setIdaddress(output);
                                                            PutHttpDataTask putGroup = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                                                @Override
                                                                public void processFinish(Boolean output) {
                                                                    if (output) finish();
                                                                    else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                }
                                                            });
                                                            try {
                                                                putGroup.execute(new URL(ConnectionDB.url + "group/" + etName.getText().toString() + "/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + getDaysSelected() + "/" + etPrice.getText().toString() + "/" + (!tbShift.getText().equals(getString(R.string.morning))) + "/" + etObservations.getText().toString() + "/" + group.getIdgroup()));
                                                            } catch (MalformedURLException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }
                                                });
                                                try {
                                                    postTo.execute(new URL(ConnectionDB.url + "address/" + to.getName() + "/" + to.getLat() + "/" + to.getLng()));
                                                } catch (MalformedURLException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                                    }
                                });
                                try {
                                    getTo.execute(new URL(ConnectionDB.url + "address/getByName/" + to.getName()));
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                PostHttpDataTask postFrom = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                    @Override
                                    public void processFinish(Integer output) {
                                        if (output != -1) {
                                            from.setIdaddress(output);
                                            GetAddressHttpDataTask getTo = new GetAddressHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                                                @Override
                                                public void processFinish(ArrayList<?> output) {
                                                    if (output != null) {
                                                        if (output.size() > 0 && output.get(0) != null) {
                                                            to.setIdaddress(((Address) output.get(0)).getIdaddress());
                                                            PutHttpDataTask putGroupNoAddress = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                                                @Override
                                                                public void processFinish(Boolean output) {
                                                                    if (output) finish();
                                                                    else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                }
                                                            });
                                                            try {
                                                                putGroupNoAddress.execute(new URL(ConnectionDB.url + "group/" + etName.getText().toString() + "/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + getDaysSelected() + "/" + etPrice.getText().toString() + "/" + (!tbShift.getText().equals(getString(R.string.morning))) + "/" + etObservations.getText().toString() + "/" + group.getIdgroup()));
                                                            } catch (MalformedURLException e) {
                                                                e.printStackTrace();
                                                            }
                                                        } else {
                                                            PostHttpDataTask postTo = new PostHttpDataTask(getApplicationContext(), new PostHttpDataTask.AsyncResponse() {
                                                                @Override
                                                                public void processFinish(Integer output) {
                                                                    if (output != -1) {
                                                                        to.setIdaddress(output);
                                                                        PutHttpDataTask putGroup = new PutHttpDataTask(getApplicationContext(), new PutHttpDataTask.AsyncResponse() {
                                                                            @Override
                                                                            public void processFinish(Boolean output) {
                                                                                if (output) finish();
                                                                                else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        });
                                                                        try {
                                                                            putGroup.execute(new URL(ConnectionDB.url + "group/" + etName.getText().toString() + "/" + from.getIdaddress() + "/" + to.getIdaddress() + "/" + getDaysSelected() + "/" + etPrice.getText().toString() + "/" + (!tbShift.getText().equals(getString(R.string.morning))) + "/" + etObservations.getText().toString() + "/" + group.getIdgroup()));
                                                                        } catch (MalformedURLException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                            try {
                                                                postTo.execute(new URL(ConnectionDB.url + "address/" + to.getName() + "/" + to.getLat() + "/" + to.getLng()));
                                                            } catch (MalformedURLException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                            try {
                                                getTo.execute(new URL(ConnectionDB.url + "address/getByName/" + to.getName()));
                                            } catch (MalformedURLException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                                try {
                                    postFrom.execute(new URL(ConnectionDB.url + "address/" + from.getName() + "/" + from.getLat() + "/" + from.getLng()));
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_SHORT).show();
                    }
                });
                try {
                    getFrom.execute(new URL(ConnectionDB.url + "address/getByName/" + from.getName()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Comprobamos que los datos se han rellenado correctamente
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private boolean checkFields() {
        etName.setBackground(getDrawable(R.drawable.et_rounded_login));
        etPrice.setBackground(getDrawable(R.drawable.et_rounded_login));
        etObservations.setBackground(getDrawable(R.drawable.et_rounded_login));

        if (etName.getText().length() > 2 && etName.getText().length() <= 25) {
            if (etPrice.getText().length() > 0 && Integer.parseInt(etPrice.getText().toString()) > 0) {
                if (etObservations.getText().length() > 10 && etObservations.getText().length() <= 150) {
                    if (from != null && to != null && from.getName().length() > 0 && to.getName().length() > 0) {
                        if (isDaySelected()) return true;
                        else Toast.makeText(getApplicationContext(), R.string.day_necessary, Toast.LENGTH_SHORT).show();
                    } else Toast.makeText(getApplicationContext(), R.string.address_necessary, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.observations_lenght_error, Toast.LENGTH_SHORT).show();
                    etObservations.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                }
            } else {
                Toast.makeText(getApplicationContext(), R.string.not_0_price, Toast.LENGTH_SHORT).show();
                etPrice.setBackground(getDrawable(R.drawable.et_rounded_login_error));
            }
        } else {
            Toast.makeText(getApplicationContext(), R.string.err_group_name_lenght, Toast.LENGTH_SHORT).show();
            etName.setBackground(getDrawable(R.drawable.et_rounded_login_error));
        }

        return false;
    }

    /**
     * Comprobamos que almenos un día ha sido seleccionado
     * @return
     */
    private boolean isDaySelected() {
        for (int day : daysSelected) {
            if (day == 1) return true;
        }
        return false;
    }

    /**
     * Rellenamos los datos si estamos editando un grupo
     */
    private void fillData() {
        tvHead.setText(R.string.edit_group);
        etName.setText(group.getName());
        etPrice.setText(String.valueOf(group.getPrice()));
        etObservations.setText(group.getObservations());
        fgFrom.setText(group.getFrom().getName());
        fgTo.setText(group.getTo().getName());

        String[] days = group.getDays().split("");
        cbM.setChecked(days[0].equals("1"));
        cbTu.setChecked(days[1].equals("1"));
        cbW.setChecked(days[2].equals("1"));
        cbTh.setChecked(days[3].equals("1"));
        cbF.setChecked(days[4].equals("1"));

        daysSelected[0] = cbM.isChecked() ? 1 : 0;
        daysSelected[1] = cbTu.isChecked() ? 1 : 0;
        daysSelected[2] = cbW.isChecked() ? 1 : 0;
        daysSelected[3] = cbTh.isChecked() ? 1 : 0;
        daysSelected[4] = cbF.isChecked() ? 1 : 0;
    }

    /**
     * Obtenemos los datos seleccionados
     * @return
     */
    private String getDaysSelected() {
        StringBuilder days = new StringBuilder();

        for (int day : daysSelected) {
            days.append(day);
        }
        return days.toString();
    }

    /**
     * Cargamos los fragments y los preparamos para poder usarlos con la API Places
     */
    private void loadFragments() {
        // Initialize the AutocompleteSupportFragment.
        fgTo = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.fgTo);
        fgFrom = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.fgFrom);

        if (!Places.isInitialized()) Places.initialize(getApplicationContext(), "AIzaSyBFrnw6KbpN68Cv0odVA-AAAxPPll7_Oys");

        // Specify the types of place data to return.
        fgTo.setPlaceFields(Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME));
        fgTo.setHint(getString(R.string.to));
        fgTo.setCountry("ES");

        fgFrom.setPlaceFields(Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME));
        fgFrom.setHint(getString(R.string.from));
        fgFrom.setCountry("ES");

        // Set up a PlaceSelectionListener to handle the response.
        fgTo.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                to = new Address(place.getName(), place.getLatLng().latitude, place.getLatLng().longitude);
            }

            @Override
            public void onError(Status status) {
            }
        });

        fgFrom.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                from = new Address(place.getName(), place.getLatLng().latitude, place.getLatLng().longitude);
            }

            @Override
            public void onError(Status status) {
            }
        });
    }
}
