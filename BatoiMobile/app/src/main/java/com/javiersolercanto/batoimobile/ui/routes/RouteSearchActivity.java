package com.javiersolercanto.batoimobile.ui.routes;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.route.GetRouteHttpDataTask;
import com.javiersolercanto.batoimobile.model.Address;
import com.javiersolercanto.batoimobile.model.ConnectionDB;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class RouteSearchActivity extends AppCompatActivity {

    private EditText etDate;
    private Button btnSearch;

    private AutocompleteSupportFragment fgTo;
    private AutocompleteSupportFragment fgFrom;

    private static Address from;
    private static Address to;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_search);

        setUI();
    }

    private void setUI() {
        etDate = findViewById(R.id.etDate);
        btnSearch = findViewById(R.id.btnSearch);

        from = new Address();
        to = new Address();
        from.setIdaddress(-1);
        to.setIdaddress(-1);

        loadFragments();

        btnSearch.setOnClickListener(v -> search());
    }

    /**
     * Le pasaremos a la activity principal la lista obtenida mediante los filtros
     */
    private void search() {
        GetRouteHttpDataTask getRouteHttpDataTask = new GetRouteHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    setResult(RESULT_OK, new Intent().putExtra("routes", output));
                    finish();
                }
            }
        });
        URL url;
        if ((url = getURL(from, to, etDate.getText().toString())) != null) {
            getRouteHttpDataTask.execute(url);
        } else
            Toast.makeText(getApplicationContext(), R.string.error_no_filter_selected, Toast.LENGTH_SHORT).show();
    }

    /**
     * Cargaremos los fragments y sus opciones para la API Places
     */
    private void loadFragments() {
        // Initialize the AutocompleteSupportFragment.
        fgTo = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.fgTo);
        fgFrom = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.fgFrom);

        if (!Places.isInitialized()) Places.initialize(getApplicationContext(), "AIzaSyBFrnw6KbpN68Cv0odVA-AAAxPPll7_Oys");

        // Specify the types of place data to return.
        fgTo.setPlaceFields(Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME));
        fgTo.setHint(getString(R.string.to));
        fgTo.setCountry("ES");

        fgFrom.setPlaceFields(Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME));
        fgFrom.setHint(getString(R.string.from));
        fgFrom.setCountry("ES");

        // Set up a PlaceSelectionListener to handle the response.
        fgTo.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                to = new Address(place.getName(), place.getLatLng().latitude, place.getLatLng().longitude);
            }

            @Override
            public void onError(Status status) {
            }
        });

        fgFrom.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                from = new Address(place.getName(), place.getLatLng().latitude, place.getLatLng().longitude);
            }

            @Override
            public void onError(Status status) {
            }
        });
    }

    /**
     * Cuando clickamos el editText de la fecha se nos abrirán los pickers de día y de hora para poder seleccionarlo
     * @param view
     */
    public void showDateTimeDialog(View view) {
        final Calendar calendar=Calendar.getInstance();
        DatePickerDialog.OnDateSetListener dateSetListener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                TimePickerDialog.OnTimeSetListener timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);

                        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("EEE d MMM HH:mm");

                        etDate.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                };

                new TimePickerDialog(RouteSearchActivity.this,timeSetListener,calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE),false).show();
            }
        };

        new DatePickerDialog(RouteSearchActivity.this,dateSetListener,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    /**
     * Dependiendo de los filtros seleccionados crearemos una URL para buscar diferente
     * @param from
     * @param to
     * @param date
     * @return
     */
    private URL getURL(Address from, Address to, String date) {
        try {
            if (from.getName() != null && from.getName().length() > 0 && to.getName() != null && to.getName().length() > 0 && (date.length() > 0)) {
                return new URL(ConnectionDB.url + "route/getByFilters/" + from.getName() + "/" + to.getName() + "/" + date);
            } else if (from.getName() != null && from.getName().length() > 0 && to.getName() != null && to.getName().length() > 0) {
                return new URL(ConnectionDB.url + "route/getByFromAndTo/" + from.getName() + "/" + to.getName());
            } else if (from.getName() != null && from.getName().length() > 0 && date.length() > 0) {
                return new URL(ConnectionDB.url + "route/getByFromAndDate/" + from.getName() + "/" + date);
            } else if (from.getName() != null && from.getName().length() > 0) {
                return new URL(ConnectionDB.url + "route/getByFrom/" + from.getName());
            } else if (to.getName() != null && to.getName().length() > 0 && date.length() > 0) {
                return new URL(ConnectionDB.url + "route/getByToAndDate/" + to.getName() + "/" + date);
            } else if (to.getName() != null && to.getName().length() > 0) {
                return new URL(ConnectionDB.url + "route/getByTo/" + to.getName());
            } else if (date.length() > 0) {
                return new URL(ConnectionDB.url + "route/getByDate/" + date);
            } else {
                return null;
            }
        } catch (MalformedURLException e) {
            return null;
        }
    }
}
