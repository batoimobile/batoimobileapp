package com.javiersolercanto.batoimobile.ui.routes;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.route.GetRouteHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Route;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.MyViewHolder> {

    public interface OnItemClickListener {

        void onItemClick(Route route);
    }

    private ArrayList<Route> myDataset;
    private OnItemClickListener listener;
    private Context appContext;
    private SwipeRefreshLayout swipeRefreshLayout;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvRoute;
        TextView tvPrice;
        TextView tvAuthor;
        TextView tvSeats;
        TextView tvDate;

        public MyViewHolder(View view) {
            super(view);

            this.tvAuthor = view.findViewById(R.id.tvAuthor);
            this.tvRoute = view.findViewById(R.id.tvRoute);
            this.tvPrice = view.findViewById(R.id.tvPrice);
            this.tvSeats = view.findViewById(R.id.tvSeats);
            this.tvDate = view.findViewById(R.id.tvDate);
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public void bind(final Route route, final OnItemClickListener listener) {

            this.tvAuthor.setText(route.getAuthor().getName());
            this.tvPrice.setText(String.valueOf(route.getPrice()));
            this.tvSeats.setText(String.valueOf(route.getSeats()));
            this.tvDate.setText(route.getDate().format(DateTimeFormatter.ofPattern("EEE d MMM HH:mm")));
            this.tvRoute.setText(route.getFrom().getName() + " - " + route.getTo().getName());

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(route);
                }
            });
        }
    }

    public RouteAdapter(ArrayList<Route> myDataset, OnItemClickListener listener, Context appContext, SwipeRefreshLayout swipeRefreshLayout) {

        this.myDataset = myDataset;
        this.listener = listener;
        this.appContext = appContext;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    public void loadList() {
        myDataset.clear();
        swipeRefreshLayout.setRefreshing(true);
        GetRouteHttpDataTask getRouteHttpDataTask = new GetRouteHttpDataTask(this.appContext, new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    myDataset.addAll((ArrayList<Route>) output);
                    notifyDataSetChanged();
                    RoutesActivity.refreshView();
                } else Toast.makeText(appContext, R.string.server_lost, Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        try {
            getRouteHttpDataTask.execute(new URL(ConnectionDB.url + "route"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void findByUser() {
        swipeRefreshLayout.setRefreshing(true);
        ArrayList<Route> newList = new ArrayList<>();
        for (Route route : myDataset) {
            if (route.getAuthor().getIduser() == CurrentUser.getCurrentUser().getIduser()) {
                newList.add(route);
            }
        }
        myDataset.clear();
        myDataset.addAll(newList);
        swipeRefreshLayout.setRefreshing(false);
        this.notifyDataSetChanged();
        RoutesActivity.refreshView();
    }

    public void filterByPrice(boolean mode) {
        Collections.sort(myDataset, new MyPriceCompare(mode));

        notifyDataSetChanged();
        RoutesActivity.refreshView();
    }

    public void setData(ArrayList<Route> data) {
        myDataset.clear();
        myDataset.addAll(data);
        Log.i("cicle", myDataset.size() + " =");
        this.notifyDataSetChanged();
        RoutesActivity.refreshView();
    }

    static class MyPriceCompare implements Comparator<Route> {

        private boolean mode;

        MyPriceCompare(boolean mode) {
            this.mode = mode;
        }

        @Override
        public int compare(Route r1, Route r2) {
            if (mode) {
                if(r1.getPrice() > r2.getPrice()){
                    return 1;
                } else {
                    return -1;
                }
            } else {
                if(r1.getPrice() < r2.getPrice()){
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.route_item, parent, false);

        return new MyViewHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myDataset.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return myDataset.size();
    }
}
