package com.javiersolercanto.batoimobile.ui.forum;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.DeleteHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Post;
import com.javiersolercanto.batoimobile.model.Reply;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class PostDetailActivity extends AppCompatActivity implements ReplyAdapter.OnItemClickListener {

    private TextView tvCategory;
    private TextView tvSubject;
    private TextView tvBody;
    private TextView tvAuthor;
    private TextView tvReplies;

    private DeleteHttpDataTask deleteHttpDataTask;

    private RecyclerView recyclerView;
    private ReplyAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private static ArrayList<Reply> myDataset;
    private SwipeRefreshLayout swipeRefreshReplies;
    private Button btnReply;

    public static Post post;
    private static TextView backText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_post_detail);

        post = (Post) getIntent().getSerializableExtra("post");
        if (post != null) this.setTitle(post.getSubject());

        setUI();
    }

    private void setUI() {
        tvCategory = findViewById(R.id.tvCategory);
        tvAuthor = findViewById(R.id.tvAuthor);
        tvSubject = findViewById(R.id.tvSubject);
        tvBody = findViewById(R.id.tvBody);
        tvReplies = findViewById(R.id.tvReplies);
        backText = findViewById(R.id.tvNotFound);
        btnReply = findViewById(R.id.btnReply);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

        myDataset = new ArrayList<>();

        swipeRefreshReplies = findViewById(R.id.swipeRefreshReplies);
        swipeRefreshReplies.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshReplies.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.loadList();
                mAdapter.notifyDataSetChanged();
                refreshView();
                Toast.makeText(getApplicationContext(), R.string.replies_refreshed, Toast.LENGTH_SHORT).show();
                swipeRefreshReplies.setRefreshing(false);
            }
        });

        mAdapter = new ReplyAdapter(post, myDataset, this, getApplicationContext(), swipeRefreshReplies);
        recyclerView.setAdapter(mAdapter);

        btnReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NewResponseActivity.class);
                intent.putExtra("post", post);
                intent.putExtra("reply", (Reply) null);

                startActivity(intent);
            }
        });

        // Si el usuario actual no es el propietario se le permite escribir una respuesta
        if (CurrentUser.getCurrentUser().getIduser() != post.getAuthor().getIduser()) {
            btnReply.setVisibility(View.VISIBLE);
        }

        setMainPostData();
    }

    /**
     * Cargamos los datos del post
     */
    private void setMainPostData() {
        tvCategory.setText(post.getCategory().getName());
        tvSubject.setText(post.getSubject());
        tvBody.setText(post.getBody());
        tvAuthor.setText(post.getAuthor().getName());
        tvReplies.setText(String.valueOf(post.getReplies()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (CurrentUser.getCurrentUser().getIduser() == post.getAuthor().getIduser()) getMenuInflater().inflate(R.menu.reply_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.delete) {
            AlertDialog.Builder builder = new AlertDialog.Builder(PostDetailActivity.this);
            builder.setMessage(R.string.alert_delete_post)
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            deleteHttpDataTask = new DeleteHttpDataTask(getApplicationContext(), new DeleteHttpDataTask.AsyncResponse() {
                                @Override
                                public void processFinish(Boolean output) {
                                    if (output) {
                                        finish();
                                    } else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_LONG).show();
                                }
                            });
                            try {
                                deleteHttpDataTask.execute(new URL(ConnectionDB.url + "post/" + post.getId()));
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        }
                    })
                    .setNeutralButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();

            return true;
        } else if (item.getItemId() == R.id.edit) {
            Intent intent = new Intent(getApplicationContext(), NewPostActivity.class);
            intent.putExtra("post", post);
            startActivity(intent);

            return true;
        }
        else return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.restoreList();
        setMainPostData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     * Comprobamos cada vez que se modifica la lista si aún quedan ítems para mostrar el texto
     */
    public static void refreshView() {
        if (myDataset.size() == 0) {
            backText.setVisibility(View.VISIBLE);

        } else {
            backText.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onItemClick(Reply reply) {
    }

    /**
     * Si somos los propietarios del post o el de la respuesta nos permitirá o marcarla correcta o editarla
     * @param reply
     */
    @Override
    public void onItemLongClick(Reply reply) {
        if (CurrentUser.getCurrentUser().getIduser() == reply.getAuthor().getIduser() || CurrentUser.getCurrentUser().getIduser() == post.getAuthor().getIduser()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(PostDetailActivity.this);
            builder.setMessage(reply.getAuthor().getIduser() == CurrentUser.getCurrentUser().getIduser() ? getString(R.string.want_to_delete_reply_or_edit) : (!reply.isCorrect() ? getString(R.string.correct_reply) : getString(R.string.incorrect_reply)))
                    .setCancelable(reply.getAuthor().getIduser() == CurrentUser.getCurrentUser().getIduser())
                    .setPositiveButton(reply.getAuthor().getIduser() == CurrentUser.getCurrentUser().getIduser() ? getString(R.string.delete) : getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (CurrentUser.getCurrentUser().getIduser() == reply.getAuthor().getIduser()) {
                                deleteHttpDataTask = new DeleteHttpDataTask(getApplicationContext(), new DeleteHttpDataTask.AsyncResponse() {
                                    @Override
                                    public void processFinish(Boolean output) {
                                        if (output) mAdapter.deleteItem(reply);
                                        else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                                    }
                                });
                                try {
                                    deleteHttpDataTask.execute(new URL(ConnectionDB.url + "reply/" + reply.getIdreply()));
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                            }
                            finish();
                            startActivity(getIntent());
                        }
                    })
                    .setNeutralButton(reply.getAuthor().getIduser() == CurrentUser.getCurrentUser().getIduser() ? getString(R.string.edit) : getString(R.string.sVoid), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (CurrentUser.getCurrentUser().getIduser() == reply.getAuthor().getIduser()) {
                                Intent intent = new Intent(getApplicationContext(), NewResponseActivity.class);
                                intent.putExtra("reply", reply);
                                intent.putExtra("post", post);
                                startActivity(intent);
                            }
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }
}
