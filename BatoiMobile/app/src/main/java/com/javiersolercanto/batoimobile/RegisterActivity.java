package com.javiersolercanto.batoimobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.asyncTask.DeleteHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.PostHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.user.GetUserHttpDataTask;
import com.javiersolercanto.batoimobile.model.Code;
import com.javiersolercanto.batoimobile.model.ConnectionDB;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etUser;
    private EditText etMail;
    private EditText etFirstPassword;
    private EditText etSecondPassword;
    private TextView tvError;
    private ProgressBar progressBar;

    private static Code code;

    private static PostHttpDataTask postHttpDataTask;
    private static GetUserHttpDataTask getUserHttpDataTask;
    private static DeleteHttpDataTask deleteHttpDataTask;

    private static final int SERVER_LOST = -2;
    private static final int POST_FAILED = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);

        // Hiding the action bar
        ActionBar ab = getSupportActionBar();
        if(ab!=null)
            ab.hide();

        code = (Code) getIntent().getSerializableExtra("code");

        setUI();
    }

    private void setUI() {
        etName = findViewById(R.id.etName);
        etUser = findViewById(R.id.etUsername);
        etMail = findViewById(R.id.etMail);
        etFirstPassword = findViewById(R.id.etPassword1);
        etSecondPassword = findViewById(R.id.etPassword2);
        tvError = findViewById(R.id.tvError);
        progressBar = findViewById(R.id.progressBar);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean checkFields() {
        tvError.setText("");
        etName.setBackground(getDrawable(R.drawable.et_rounded_login));
        etUser.setBackground(getDrawable(R.drawable.et_rounded_login));
        etMail.setBackground(getDrawable(R.drawable.et_rounded_login));
        etFirstPassword.setBackground(getDrawable(R.drawable.et_rounded_login));
        etSecondPassword.setBackground(getDrawable(R.drawable.et_rounded_login));

        if (etName.length() > 6 && etName.length() <= 25) {
            if (etUser.length() > 3 && etName.length() <= 25) {
                if (etMail.length() > 9 && etMail.length() <= 50) {
                    if (etFirstPassword.length() > 7 && etFirstPassword.length() <= 25) {
                        if (!etFirstPassword.getText().toString().equals(etSecondPassword.getText().toString())) {
                            etSecondPassword.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                            tvError.setText(R.string.err_pass_ident);
                            etSecondPassword.getText().clear();
                        } else return true;
                    } else {
                        etFirstPassword.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                        tvError.setText(R.string.err_pass_lenght);
                        etSecondPassword.getText().clear();
                    }
                } else {
                    etMail.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                    tvError.setText(R.string.err_email_lenght);
                }
            } else {
                etUser.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                tvError.setText(R.string.err_nickanme_length);
            }
        } else {
            etName.setBackground(getDrawable(R.drawable.et_rounded_login_error));
            tvError.setText(R.string.err_name_lenght);
        }

        return false;
    }


    /**
     * En este método realizaremos los pasos ensenciales para completar el registro.
     * Borraremos el código para que no pueda ser usado y guardaremos en las preferencias
     * que ya no es la primera vez que abrimos la aplicación
     */
    public void completeRegistry() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(getString(R.string.firstTime), Boolean.FALSE);
        edit.commit();

        deleteHttpDataTask = new DeleteHttpDataTask(getApplicationContext(), new DeleteHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(Boolean output) {
            }
        });
        try {
            deleteHttpDataTask.execute(new URL(ConnectionDB.url + "code/" + code.getCode()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }

    /**
     * En este método lanzaremos la petición a la base de datos para que nos guarde el nuevo usuario
     * @param view
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void confirm(View view) {
        if (checkFields()) {
            try {
                // Primero comprobamos que el nombre de usuario no exista
                getUserHttpDataTask = new GetUserHttpDataTask(getApplicationContext(), progressBar, new GetHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(ArrayList<?> output) {
                        if (output != null) {
                            if (output.isEmpty()) {
                                postHttpDataTask = new PostHttpDataTask(getApplicationContext(), progressBar, new PostHttpDataTask.AsyncResponse() {
                                    @Override
                                    public void processFinish(Integer output) {
                                        if (output != SERVER_LOST) {
                                            if (output != POST_FAILED) {
                                                completeRegistry();
                                            } else {
                                                Toast.makeText(getApplicationContext(), R.string.error_while_insert_data,Toast.LENGTH_SHORT).show();
                                                etSecondPassword.getText().clear();
                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_LONG).show();
                                            etSecondPassword.getText().clear();
                                        }
                                    }
                                });
                                try {
                                    postHttpDataTask.execute(new URL(ConnectionDB.url + "user/" + etName.getText().toString() + "/" + etUser.getText().toString() + "/" + etMail.getText().toString() + "/" + etFirstPassword.getText().toString()));
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                tvError.setText(R.string.nickname_taken);
                                etUser.setBackground(getDrawable(R.drawable.et_rounded_login_error));
                                etSecondPassword.getText().clear();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.server_lost, Toast.LENGTH_LONG).show();
                            etSecondPassword.getText().clear();
                        }
                    }
                });
                getUserHttpDataTask.execute(new URL(ConnectionDB.url + "user/nickname/" + etUser.getText().toString()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }
}
