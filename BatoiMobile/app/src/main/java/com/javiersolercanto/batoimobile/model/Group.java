package com.javiersolercanto.batoimobile.model;

import java.io.Serializable;

public class Group implements Serializable {

    private int idgroup;
    private String name;
    private Address from;
    private Address to;
    private boolean shift;
    private String days;
    private int price;
    private String observations;
    private User admin;
    private int members;

    public Group(int idgroup, String name, Address from, Address to, boolean shift, String days, int price, String observations, User admin, int members) {
        this.idgroup = idgroup;
        this.name = name;
        this.from = from;
        this.to = to;
        this.shift = shift;
        this.days = days;
        this.price = price;
        this.observations = observations;
        this.admin = admin;
        this.members = members;
    }

    public int getIdgroup() {
        return idgroup;
    }

    public void setIdgroup(int idgroup) {
        this.idgroup = idgroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getFrom() {
        return from;
    }

    public void setFrom(Address from) {
        this.from = from;
    }

    public Address getTo() {
        return to;
    }

    public void setTo(Address to) {
        this.to = to;
    }

    public boolean isShift() {
        return shift;
    }

    public void setShift(boolean shift) {
        this.shift = shift;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public User getAdmin() {
        return admin;
    }

    public void setAdmin(User admin) {
        this.admin = admin;
    }

    public int getMembers() {
        return members;
    }

    public void setMembers(int members) {
        this.members = members;
    }
}
