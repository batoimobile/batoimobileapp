package com.javiersolercanto.batoimobile.model;

import java.io.Serializable;

public class Code implements Serializable {

    private int code;

    public Code(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
