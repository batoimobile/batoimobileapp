package com.javiersolercanto.batoimobile.ui.rentals;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.DeleteHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Rental;
import com.javiersolercanto.batoimobile.tools.Tools;

import java.net.MalformedURLException;
import java.net.URL;

public class RentalDetailActivity extends AppCompatActivity {

    private TextView tvAddressName;
    private TextView tvAuthor;
    private TextView tvPrice;
    private TextView tvKms;
    private EditText etDescription;

    private Button btnEditContact;
    private ImageButton btnDeleteShare;

    private static Rental rental;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rental_detail);

        if (getIntent() != null) rental = (Rental) getIntent().getSerializableExtra("rental");

        setUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setUI() {
        tvAddressName = findViewById(R.id.tvAddressName);
        tvAuthor = findViewById(R.id.tvAuthor);
        tvPrice = findViewById(R.id.tvPrice);
        tvKms = findViewById(R.id.tvKms);
        etDescription = findViewById(R.id.etDescription);
        btnEditContact = findViewById(R.id.btnEditContact);
        btnDeleteShare = findViewById(R.id.btnDeleteShare);

        /**
         * Dependiendo del rol o si es el autor podrá borrar el anuncio. Los administradores también pueden borrar
         */
        btnDeleteShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentUser.getCurrentUser().getRole().equals("ROLE_USER") && CurrentUser.getCurrentUser().getIduser() != rental.getAuthor().getIduser())
                    Tools.composeEmail(new String[] {}, "Hey! Look at this rental on " + rental.getAddress().getName() + " - " + CurrentUser.getCurrentUser().getName(), "This may interest you.", RentalDetailActivity.this);
                else
                    createDialog(getString(R.string.sure_about_delete_rental), null, RentalDetailActivity.this);
            }
        });

        if (CurrentUser.getCurrentUser().getIduser() == rental.getAuthor().getIduser()) {
            btnEditContact.setText(R.string.edit);
            btnDeleteShare.setImageDrawable(getDrawable(R.drawable.ic_delete_white_24dp));
        }

        fillData();
    }

    /**
     * Rellenamos los datos del alquiler
     */
    private void fillData() {
        tvAddressName.setText(rental.getAddress().getName());
        tvAuthor.setText(rental.getAuthor().getName());
        tvPrice.setText(String.valueOf(rental.getPrice()));
        etDescription.setText(rental.getDescription());
        btnEditContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnEditContact.getText().equals(getString(R.string.contact))) Tools.composeEmail(new String[] {rental.getAuthor().getEmail()}, "I'm interested on your rental - " + CurrentUser.getCurrentUser().getName(), "I'm interested on your rental on " + rental.getAddress().getName() + " please contact with me for give me more information. Thanks", RentalDetailActivity.this);
                else {
                    Intent intent = new Intent(getApplicationContext(), NewRentalActivity.class);
                    intent.putExtra("rental", rental);
                    startActivity(intent);
                    finish();
                }
            }
        });

        float[] distance = new float[1];
        Location.distanceBetween(rental.getAddress().getLat(), rental.getAddress().getLng(), 38.690825, -0.496971, distance);
        tvKms.setText(Math.round(distance[0]/1000 * 100.0) / 100.0 + "Km");
    }

    /**
     * Creamos un dialog para preguntar por el borrado del alquiler
     * @param title
     * @param message
     * @param activity
     */
    public void createDialog(String title, String message, Context activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);

        if (message != null) builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DeleteHttpDataTask deleteHttpDataTask = new DeleteHttpDataTask(getApplicationContext(), new DeleteHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(Boolean output) {
                        if (output) finish();
                        else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                    }
                });
                try {
                    deleteHttpDataTask.execute(new URL(ConnectionDB.url + "rental/" + rental.getIdrental()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
