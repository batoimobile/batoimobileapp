package com.javiersolercanto.batoimobile.model;

import java.io.Serializable;

public class Post implements Serializable {

    private int id;
    private String subject;
    private String body;
    private boolean resolved;
    private int replies;
    private User author;
    private Category category;

    public Post(int id, String subject, String body, User author) {
        this.id = id;
        this.subject = subject;
        this.body = body;
        this.author = author;
    }

    public Post(int id, String subject, String body, boolean resolved, User author, Category category, int replies) {
        this.id = id;
        this.subject = subject;
        this.body = body;
        this.resolved = resolved;
        this.author = author;
        this.category = category;
        this.replies = replies;
    }

    public int getReplies() {
        return replies;
    }

    public void setReplies(int replies) {
        this.replies = replies;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }


    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                ", resolved=" + resolved +
                ", author=" + author.toString() +
                ", category=" + category.toString() +
                '}';
    }
}
