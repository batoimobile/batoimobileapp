package com.javiersolercanto.batoimobile.ui.rentals;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.model.Rental;

import java.util.ArrayList;

public class RentalsActivity extends AppCompatActivity implements RentalAdapter.OnItemClickListener, View.OnClickListener{

    public static final int REQUEST_LOCATION_PERMISSION = 1;
    public static String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION};

    private RecyclerView recyclerView;
    private RentalAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private static TextView backText;


    private static boolean filterMode = false;
    private static boolean priceMode = false;
    private static boolean distanceMode = false;
    private static ArrayList<Rental> myDataset;
    private SwipeRefreshLayout swipeRefreshRentals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.setTitle("Rentals");

        setContentView(R.layout.activity_rentals);

        setUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rental_menu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.loadList();
    }

    private void setUI() {
        backText = findViewById(R.id.tvNotFound);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

        myDataset = new ArrayList<>();

        swipeRefreshRentals = findViewById(R.id.swipeRefreshRentals);
        swipeRefreshRentals.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshRentals.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.loadList();
                Toast.makeText(getApplicationContext(), R.string.post_refreshed, Toast.LENGTH_SHORT).show();
            }
        });

        mAdapter = new RentalAdapter(myDataset, this, getApplicationContext(), swipeRefreshRentals);
        recyclerView.setAdapter(mAdapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NewRentalActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Tratamos las opciones seleccionadas en el menú
     * @param item
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Mostrar los alquileres del usuario o todos
            case R.id.myRentals:
                if (!filterMode) {
                    mAdapter.findByUser();
                    item.setTitle(R.string.my_rentals);
                    item.setIcon(R.drawable.ic_all_inclusive_white_48dp);
                } else {
                    mAdapter.loadList();
                    item.setTitle(R.string.all_rentals);
                    item.setIcon(R.drawable.ic_local_hotel_white_48dp);
                }
                filterMode = !filterMode;
                return true;
            // Ordenar por ditancia
            case R.id.distance:
                mAdapter.filterByDistance(distanceMode);
                distanceMode = !distanceMode;
                return true;
            // Ordenar por precio
            case R.id.price:
                mAdapter.filterByPrice(priceMode);
                priceMode = !priceMode;
                return true;
            // Abrimos el mapa
            case R.id.map:
                startActivity(new Intent(this, MapActivity.class));
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Si clickamos abriremos la pantalla con más información
     * @param rental
     */
    @Override
    public void onItemClick(Rental rental) {
        Intent intent = new Intent(this, RentalDetailActivity.class);

        intent.putExtra("rental", rental);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static void refreshView() {
        if (myDataset.size() == 0) {
            backText.setVisibility(View.VISIBLE);

        } else {
            backText.setVisibility(View.INVISIBLE);
        }
    }
}