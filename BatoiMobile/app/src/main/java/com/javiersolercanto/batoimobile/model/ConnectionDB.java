package com.javiersolercanto.batoimobile.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {

    private static final String JDBC_URL = "jdbc:mysql://127.0.0.1:3306/bdbatoimobile?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=GMT";

    private static Connection con = null;

    public final static String url = "http://192.168.1.112:3000/";


    public static Connection getConexion() throws SQLException {

        if(con == null) {
            con = DriverManager.getConnection(JDBC_URL, "root", "1234");
        }

        return con;
    }

    public static void cerrar() throws SQLException {

        if(con != null) {
            con.close();
        }
    }

}
