package com.javiersolercanto.batoimobile.ui.routes;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.DeleteHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.Route;
import com.javiersolercanto.batoimobile.tools.Tools;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.format.DateTimeFormatter;

public class RouteDetailActivity extends AppCompatActivity {

    private TextView tvRoute;
    private TextView tvDate;
    private TextView tvPrice;
    private TextView tvSeats;
    private TextView tvObservations;
    private Button btnEditContact;
    private ImageButton btnDeleteShare;

    private Route route;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_detail);

        if (getIntent() != null) route = (Route) getIntent().getSerializableExtra("route");

        setUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setUI() {
        tvRoute = findViewById(R.id.tvRoute);
        tvDate = findViewById(R.id.tvDate);
        tvPrice = findViewById(R.id.tvPrice);
        tvSeats = findViewById(R.id.tvSeats);
        tvObservations = findViewById(R.id.tvObservations);

        btnEditContact = findViewById(R.id.btnEditContact);
        btnDeleteShare = findViewById(R.id.btnDeleteShare);

        btnDeleteShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentUser.getCurrentUser().getIduser() != route.getAuthor().getIduser()) Tools.composeEmail(new String[] {}, "Hey! Look at this " + route.getFrom().getName() + " - " + route.getTo().getName() + " - " + CurrentUser.getCurrentUser().getName(), "This may interest you.", RouteDetailActivity.this);
                else {
                    createDialogDelete(getString(R.string.sure_about_delete_route), null, RouteDetailActivity.this);
                }
            }
        });

        if (CurrentUser.getCurrentUser().getIduser() == route.getAuthor().getIduser()) {
            btnEditContact.setText(R.string.edit);
            btnDeleteShare.setImageDrawable(getDrawable(R.drawable.ic_delete_white_24dp));
        }

        fillData();

        btnEditContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnEditContact.getText().toString().equals(getString(R.string.contact))) Tools.composeEmail(new String[] {route.getAuthor().getEmail()}, "I want to travel with you - " + CurrentUser.getCurrentUser().getName(), "I'm interested on your travel from " + route.getFrom() + " to " + route.getTo(), RouteDetailActivity.this);
                else {
                    Intent intent = new Intent(getApplicationContext(), NewRouteActivity.class);
                    intent.putExtra("route", route);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void fillData() {
        tvRoute.setText(route.getFrom().getName() + " - " + route.getTo().getName());
        this.tvDate.setText(route.getDate().format(DateTimeFormatter.ofPattern("EEE d MMM HH:mm")));
        tvPrice.setText(String.valueOf(route.getPrice()));
        tvSeats.setText(String.valueOf(route.getSeats()));
        tvObservations.setText(route.getObservation());
    }

    public void createDialogDelete(String title, String message, Context activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);

        if (message != null) builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DeleteHttpDataTask deleteHttpDataTask = new DeleteHttpDataTask(getApplicationContext(), new DeleteHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(Boolean output) {
                        if (output) finish();
                        else Toast.makeText(getApplicationContext(), R.string.error_during_action, Toast.LENGTH_SHORT).show();
                    }
                });
                try {
                    deleteHttpDataTask.execute(new URL(ConnectionDB.url + "route/" + route.getIdroute()));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
