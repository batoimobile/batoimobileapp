package com.javiersolercanto.batoimobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.user.GetUserHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.CurrentUser;
import com.javiersolercanto.batoimobile.model.User;
import com.javiersolercanto.batoimobile.tools.Tools;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

public class LoginActivity extends AppCompatActivity {

    private static final int INTERNET_PERMISSION = 1;

    private TextView tvWelcome;
    private EditText etUsername;
    private EditText etPassword;
    private CheckBox cbRemember;
    private Button btnLogin;
    private ProgressBar progressBar;

    private static User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        // Hiding the action bar
        ActionBar ab = getSupportActionBar();
        if(ab!=null)
            ab.hide();

        setUI();
    }

    private void setUI() {
        tvWelcome = findViewById(R.id.tvWelcome);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        cbRemember = findViewById(R.id.cbRemember);
        btnLogin = findViewById(R.id.btnLogin);
        progressBar = findViewById(R.id.progressBar);

        refreshWelcome();
        isUserRemembered();
    }

    /**
     * Comprobamos la hora del día para así poner un mensaje adecuado
     */
    private void refreshWelcome() {
        if (new Date().getHours() >= 18) {
            tvWelcome.setText(R.string.evening);

        } else if (new Date().getHours() >= 12) {
            tvWelcome.setText(R.string.good_afternoon);

        } else if (new Date().getHours() >= 8) {
            tvWelcome.setText(R.string.good_morning);

        } else {
            tvWelcome.setText(R.string.night);
        }
    }

    /**
     * Método que nos permite comprobar las credenciales para poder iniciar sesión
     * @param view
     */
    public void login(View view) {
        if (etUsername.getText().length() > 0 && etPassword.getText().length() > 0) {
            try {
                GetUserHttpDataTask getUserHttpDataTask = new GetUserHttpDataTask(this, progressBar, new GetHttpDataTask.AsyncResponse() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void processFinish(ArrayList<?> output) {
                        checkUserResponse(output);
                    }
                });
                getUserHttpDataTask.execute(new URL(ConnectionDB.url + "user/" + etUsername.getText().toString() + "/" + etPassword.getText().toString()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else
            Toast.makeText(this, R.string.fill_the_fields,Toast.LENGTH_SHORT).show();
    }

    /**
     * Método donde tratamos la respuesta de la base de datos
     * @param output
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void checkUserResponse(ArrayList<?> output) {
        if (output != null) {
            if (!output.isEmpty() && (user = (User) output.get(0)) != null) {
                CurrentUser.setCurrentUser(user);
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();

                if (cbRemember.isChecked())
                    Tools.rememberUser(getBaseContext(), CurrentUser.getCurrentUser().getIduser());
            } else {
                Toast.makeText(getApplicationContext()
                        , R.string.user_or_password_incorrect,Toast.LENGTH_SHORT).show();
                etPassword.getText().clear();
                etPassword.setBackground(getDrawable(R.drawable.et_rounded_login_error));
            }
        } else {
            Toast.makeText(getApplicationContext()
                    , R.string.server_lost, Toast.LENGTH_LONG).show();
            etPassword.getText().clear();
        }
    }

    /**
     * Este método comprobará si hay algun usuario recordado en el fichero SharedPreferences para así
     * directamente iniciar sesión con esa id
     */
    private void isUserRemembered() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        int id = prefs.getInt("remember", -1);
        if (id != -1) {
            GetUserHttpDataTask getUserHttpDataTask = new GetUserHttpDataTask(getApplicationContext()
                    , new GetHttpDataTask.AsyncResponse() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void processFinish(ArrayList<?> output) {
                    if (output != null) {
                        if (!output.isEmpty() && output.get(0) != null) {
                            checkUserResponse(output);
                        }
                    } else Toast.makeText(getApplicationContext()
                            , R.string.server_lost, Toast.LENGTH_SHORT).show();
                }
            });
            try {
                getUserHttpDataTask.execute(new URL(ConnectionDB.url + "user/" + id));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Método para abrir la ventana de verificación
     * @param view
     */
    public void openVerification(View view) {
        startActivity(new Intent(this, VerificationActivity.class));
        finish();
    }

    /**
     * Si le damos al botón de atrás cerraremos la activity
     */
    @Override
    public void onBackPressed() {
        finish();
    }
}
