package com.javiersolercanto.batoimobile.asyncTask.reply;

import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.model.Reply;
import com.javiersolercanto.batoimobile.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetReplyHttpDataTask extends GetHttpDataTask {

    public GetReplyHttpDataTask(Context appContext, ProgressBar progressBar, AsyncResponse asyncResponse) {
        super(appContext, progressBar, asyncResponse);
    }

    public GetReplyHttpDataTask(Context appContext, AsyncResponse asyncResponse) {
        super(appContext,asyncResponse);
    }

    @Override
    protected ArrayList<Object> doInBackground(Object... objects) {
        HttpURLConnection urlConnection = null;
        ArrayList<Object> searchResult = null;
        try {
            if ((urlConnection = stablishConnection((URL) objects[0])) != null) {
                String resultStream = readStream(urlConnection.getInputStream());

                searchResult = new ArrayList<>();

                JSONObject response = new JSONObject(resultStream);
                //if (response.getBoolean("response")) {
                JSONArray replies = response.getJSONArray("replies");
                for (int i = 0; i < replies.length(); i++) {
                    JSONObject reply = replies.getJSONObject(i);
                    searchResult.add(new Reply(
                            reply.getInt("idreply"),
                            reply.getString("response"),
                            reply.getInt("likes"),
                            (reply.getInt("correct") != 0),
                            (new User(
                                reply.getInt("iduser"),
                                reply.getString("name"),
                                reply.getString("nickname"),
                                reply.getString("password"),
                                reply.getString("email"),
                                reply.getString("role")
                            )),
                            null
                        )
                    );
                    if (isCancelled()) break;
                }
            }
            //} else {
                Log.i("conErr", "Connection error");
            //}
        } catch (IOException e) {
            Log.i("IOException", e.getMessage());
        } catch (JSONException e) {
            Log.i("JSONException", e.getMessage());
            e.printStackTrace();
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return searchResult;
    }
}
