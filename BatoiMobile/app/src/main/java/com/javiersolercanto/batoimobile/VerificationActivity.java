package com.javiersolercanto.batoimobile;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.code.GetCodeHttpDataTask;
import com.javiersolercanto.batoimobile.model.Code;
import com.javiersolercanto.batoimobile.model.ConnectionDB;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class VerificationActivity extends AppCompatActivity {

    private EditText etOne;
    private EditText etTwo;
    private EditText etThree;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_verification);

        ActionBar ab = getSupportActionBar();
        if(ab!=null)
            ab.hide();

        setUI();
    }

    private void setUI() {
        etOne = findViewById(R.id.etOne);
        etTwo = findViewById(R.id.etTwo);
        etThree = findViewById(R.id.etThree);

        // Al escribir el número pasaremos el FOCUS al siguiente EditText

        etOne.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                etTwo.requestFocus();
            }
        });
        etTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                etThree.requestFocus();
            }
        });
    }

    public void verify(View view) {

        // Comprobamos que se han rellenado los campos

        if (etOne.getText().length() > 0 && etTwo.getText().length() > 0 && etThree.getText().length() > 0) {
            try {
                int entryCode = Integer.parseInt(etOne.getText().toString()
                        + etTwo.getText().toString()
                        + etThree.getText().toString());

                // Buscaremos en la base de datos si existe ese código

                GetCodeHttpDataTask getCodeHttpDataTask = new GetCodeHttpDataTask(getApplicationContext(), new GetHttpDataTask.AsyncResponse() {
                    @Override
                    public void processFinish(ArrayList<?> output) {
                        if (output != null) {
                            if (!output.isEmpty() && output.get(0) != null) {

                                // En caso de que exista seguiremos con el proceso de registro

                                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                                intent.putExtra("code", (Code) output.get(0));
                                startActivity(intent);
                                finish();
                            } else
                                Toast.makeText(getApplicationContext(), R.string.code_error,
                                        Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    R.string.server_lost, Toast.LENGTH_LONG).show();
                        }
                    }
                });

                // Ejecutamos la petición a nuestra API

                getCodeHttpDataTask.execute(new URL(ConnectionDB.url + "code/" + entryCode));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, R.string.fill_the_fields, Toast.LENGTH_LONG).show();
        }
    }

    public void openLogin(View view) {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
