package com.javiersolercanto.batoimobile.ui.rentals;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.javiersolercanto.batoimobile.R;
import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.asyncTask.rental.GetRentalHttpDataTask;
import com.javiersolercanto.batoimobile.model.ConnectionDB;
import com.javiersolercanto.batoimobile.model.Rental;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

public class MapFragment extends SupportMapFragment implements OnMapReadyCallback {

    private static final String TAG = MapFragment.class.getSimpleName();
    private static final int REQUEST_LOCATION_PERMISSION = 1;

    private View root;
    private GoogleMap mMap;

    private Snackbar info;

    private static Marker clickedMarker;
    private static boolean doubleClick = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = super.onCreateView(inflater, container, savedInstanceState);

        getMapAsync(this);
        return root;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);

        // Zoom Camera.
        LatLng cipfpbatoi = new LatLng(38.6910773, -0.4984189);
        mMap.addMarker(new MarkerOptions().position(cipfpbatoi)
                .title(getString(R.string.cipfpbatoi)).icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        // Apply zoom.
        float zoomLevel = 14F;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cipfpbatoi, zoomLevel));

        GroundOverlayOptions homeOverlay = new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromResource(R.drawable.logo_blue))
                .position(cipfpbatoi, 100);
        mMap.addGroundOverlay(homeOverlay);
        mMap.setMyLocationEnabled(true);

        setPoiClick(mMap);

        // Night mode.
        if ((getResources().getConfiguration().uiMode &
                Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES) {
            setMapStyle(mMap);
        }

        setupPoints();

    }

    private boolean checkPermissions() {
        // check for permissions granted before setting listView Adapter data
        if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext())
                , Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext()
                , Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext()
                , Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // opens Dialog: requests user to grant permission.
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), RentalsActivity.PERMISSIONS,
                    RentalsActivity.REQUEST_LOCATION_PERMISSION);
            return false;
        } else
            return true;
    }

    /**
     * Comprobamos si la localización del dispositivo está activada
     * @return
     */
    private boolean isLocationEnabled() {
        boolean location = true;
        LocationManager locationManager = (LocationManager) Objects.requireNonNull(getContext())
                .getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null && !(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER))) {

            Toast.makeText(getContext(), R.string.must_enable_location
                    , Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            location = false;
        }

        return location;
    }

    /**
     * Tratamos la peticiónd de permisos
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions
            , @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            // We have requested one READ permission for contacts, so only need [0] to be checked.
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext()
                        , R.string.location_permission_necessary, Toast.LENGTH_LONG).show();
                Objects.requireNonNull(getActivity()).finish();
            }
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Seteamos todos los puntos que hacen referencia a cada rental de la base de datoss
     * para guardar el objeto rental en el marker haremos uso de los tags del marker
     */
    private void setupPoints() {
        GetRentalHttpDataTask getRentalHttpDataTask = new GetRentalHttpDataTask(getContext()
                , new GetHttpDataTask.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<?> output) {
                if (output != null) {
                    for (int i = 0; i < output.size(); i++) {
                        LatLng point = new LatLng(((Rental) output.get(i)).getAddress().getLat(),
                                ((Rental) output.get(i)).getAddress().getLng());
                        Marker marker = mMap.addMarker(new MarkerOptions()
                                .position(point)
                                .title(
                                        ((Rental) output.get(i)).getPrice() + "€")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                                .snippet(((Rental) output.get(i)).getAddress().getName()));
                        marker.setTag(output.get(i));
                    }
                } else
                    Toast.makeText(getContext()
                            , R.string.server_lost, Toast.LENGTH_SHORT).show();
            }
        });
        try {
            getRentalHttpDataTask.execute(new URL(ConnectionDB.url + "rental"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        info = Snackbar.make(root, getString(R.string.loading_information), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.loading_information), null);
        info.show();
    }

    /**
     * Aquí trataremos los clicks sobre los markers
     * @param map
     */
    private void setPoiClick(final GoogleMap map) {
        map.setOnPoiClickListener(poi ->
        {
            Marker poiMarker = map.addMarker(new MarkerOptions()
                    .position(poi.latLng)
                    .title(poi.name));
            poiMarker.showInfoWindow();
            poiMarker.setTag(getString(R.string.poi));

        });

        /**
         * Gracias a unas variables globales podemos saber si un dobleclick
         */
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (doubleClick && marker.equals(clickedMarker) && !marker.getTitle().equals(getString(R.string.cipfpbatoi))) {
                    Intent intent = new Intent(getContext(), RentalDetailActivity.class);
                    intent.putExtra("rental", (Rental) marker.getTag());
                    startActivity(intent);
                }
                doubleClick = !doubleClick;
                clickedMarker = marker;

                return false;
            }
        });
    }

    private void setMapStyle(GoogleMap map) {
        try {
            map.setMapStyle(MapStyleOptions.loadRawResourceStyle(Objects
                    .requireNonNull(getContext()), R.raw.map_style));
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
    }

    private boolean isNetworkAvailable() {
        boolean networkAvailable = false;

        ConnectivityManager cm = (ConnectivityManager) Objects.requireNonNull(getContext())
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected())
                networkAvailable = true;
        }

        return networkAvailable;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!isNetworkAvailable())
        {
            Toast.makeText(getContext(), R.string.internet_must_be_available , Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
        }
        else if(checkPermissions() && !isLocationEnabled())
        {
            Toast.makeText(getContext(),
                    getString(R.string.location_permission_necessary), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (info != null)
            info.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}