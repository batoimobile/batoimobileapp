package com.javiersolercanto.batoimobile.asyncTask;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class PostHttpDataTask extends AsyncTask<Object, Void, Integer> {

    private final int CONNECTION_TIMEOUT = 5000;
    private final int READ_TIMEOUT = 10000;

    protected WeakReference<Context> appContext;
    private WeakReference<ProgressBar> progress;
    private AsyncResponse asyncResponse;

    public interface AsyncResponse
    {
        void processFinish(Integer output);
    }

    public PostHttpDataTask(Context context, ProgressBar pb, AsyncResponse asyncResponse) {
        this.appContext = new WeakReference<>(context);
        this.progress = new WeakReference<>(pb);
        this.asyncResponse = asyncResponse;
    }

    public PostHttpDataTask(Context context, AsyncResponse asyncResponse) {
        this.appContext = new WeakReference<>(context);
        this.asyncResponse = asyncResponse;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (progress != null) progress.get().setVisibility(View.VISIBLE);
    }

    /**
     * Leeremos la respuesta y en caso de que nos devuelva una id =! a -1 habrá salido bien
     * @param objects
     * @return
     */
    @Override
    protected Integer doInBackground(Object... objects) {
        HttpURLConnection urlConnection = null;
        int searchResult = 2;
        try {
            if ((urlConnection = stablishConnection((URL) objects[0])) != null) {
                String resultStream = readStream(urlConnection.getInputStream());

                JSONObject response = new JSONObject(resultStream);
                searchResult = response.getInt("id");
            }
        } catch (IOException e) {
            Log.i("IOException", e.getMessage());
        } catch (JSONException e) {
            Log.i("JSONException", e.getMessage());
            e.printStackTrace();
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return searchResult;
    }

    @Override
    protected void onPostExecute(Integer objects) {
        super.onPostExecute(objects);
        if (progress != null) progress.get().setVisibility(View.GONE);
        asyncResponse.processFinish(objects);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (progress != null) progress.get().setVisibility(View.GONE);
    }

    /**
     * Establecemos la conexión y le decimos que es de tipo post
     * @param url
     * @return
     */
    protected HttpURLConnection stablishConnection(URL url) {
        HttpURLConnection urlConnection = null;

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
            urlConnection.setReadTimeout(READ_TIMEOUT);

            if (isNetworkAvailable(urlConnection)) return urlConnection;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return null;
    }

    protected String readStream(InputStream in) {
        StringBuilder sb = new StringBuilder();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            String nextLine = "";

            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);
            }

        } catch (IOException e) {
            Log.i("IOException-ReadStream", e.getMessage());
        }

        return sb.toString();
    }

    protected boolean isNetworkAvailable(HttpURLConnection urlConnection) {
        ConnectivityManager cm = (ConnectivityManager) this.appContext.get().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            try {
                urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                urlConnection.connect();

                return urlConnection.getResponseCode() == 200;

            } catch (MalformedURLException e1) {
                //Toast.makeText(this.appContext.get(), "There was an error connecting to server, please try it later", Toast.LENGTH_LONG).show();
                Log.i("conErr", "Connection error");
            } catch (IOException e) {
                //Toast.makeText(this.appContext.get(), "There was an error connecting to server, please try it later", Toast.LENGTH_LONG).show();
                Log.i("conErr", "Connection error");
            }
        }
        return false;
    }
}