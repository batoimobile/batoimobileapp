package com.javiersolercanto.batoimobile.asyncTask.reaction;

import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.model.Reaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GetReactionHttpDataTask extends GetHttpDataTask{

    private WeakReference<Context> appContext;

    public GetReactionHttpDataTask(Context appContext, ProgressBar progressBar, GetHttpDataTask.AsyncResponse asyncResponse) {
        super(appContext, progressBar, asyncResponse);
    }

    public GetReactionHttpDataTask(Context appContext, GetHttpDataTask.AsyncResponse asyncResponse) {
        super(appContext,asyncResponse);
    }

    @Override
    protected ArrayList<Object> doInBackground(Object... objects) {
        HttpURLConnection urlConnection = null;
        ArrayList<Object> searchResult = null;
        try {
            if ((urlConnection = stablishConnection((URL) objects[0])) != null) {
                String resultStream = readStream(urlConnection.getInputStream());

                searchResult = new ArrayList<>();

                JSONObject response = new JSONObject(resultStream);
                //if (response.getBoolean("response")) {
                JSONArray reactions = response.getJSONArray("reactions");
                for (int i = 0; i < reactions.length(); i++) {
                    JSONObject reaction = reactions.getJSONObject(i);
                    searchResult.add(
                            new Reaction(
                                reaction.getInt("iduser"),
                                reaction.getInt("idreply")
                            )
                    );
                    if (isCancelled()) break;
                }
            }
        } catch (IOException e) {
            Log.i("IOException", e.getMessage());
        } catch (JSONException e) {
            Log.i("JSONException", e.getMessage());
            e.printStackTrace();
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return searchResult;
    }
}
