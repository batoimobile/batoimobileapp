package com.javiersolercanto.batoimobile.asyncTask.route;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.ProgressBar;

import androidx.annotation.RequiresApi;

import com.javiersolercanto.batoimobile.asyncTask.GetHttpDataTask;
import com.javiersolercanto.batoimobile.model.Address;
import com.javiersolercanto.batoimobile.model.Route;
import com.javiersolercanto.batoimobile.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class GetRouteHttpDataTask extends GetHttpDataTask {

    public GetRouteHttpDataTask(Context appContext, ProgressBar progressBar, AsyncResponse asyncResponse) {
        super(appContext, progressBar, asyncResponse);
    }

    public GetRouteHttpDataTask(Context appContext, AsyncResponse asyncResponse) {
        super(appContext,asyncResponse);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected ArrayList<Object> doInBackground(Object... objects) {
        HttpURLConnection urlConnection = null;
        ArrayList<Object> searchResult = null;
        try {
            if ((urlConnection = stablishConnection((URL) objects[0])) != null) {
                String resultStream = readStream(urlConnection.getInputStream());

                searchResult = new ArrayList<>();

                JSONObject response = new JSONObject(resultStream);
                JSONArray replies = response.getJSONArray("routes");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                for (int i = 0; i < replies.length(); i++) {
                    JSONObject route = replies.getJSONObject(i);
                    searchResult.add(new Route(
                            route.getInt("idroute"),
                            (new Address(
                                    route.getInt("idaddress_f"),
                                    route.getString("name_f"),
                                    route.getDouble("lat_f"),
                                    route.getDouble("lng_f"))
                            ),
                            (new Address(
                                    route.getInt("idaddress_t"),
                                    route.getString("name_t"),
                                    route.getDouble("lat_t"),
                                    route.getDouble("lng_t"))
                            ),
                            (LocalDateTime.parse(route.getString("date"), formatter)),
                            route.getInt("price"),
                            route.getInt("seats"),
                            route.getString("observations"),
                            (new User(
                                    route.getInt("iduser"),
                                    route.getString("name"),
                                    route.getString("nickname"),
                                    route.getString("password"),
                                    route.getString("email"),
                                    route.getString("role")
                            )))
                    );
                    if (isCancelled()) break;
                }
            }
            //} else {
            Log.i("conErr", "Connection error");
            //}
        } catch (IOException e) {
            Log.i("IOException", e.getMessage());
        } catch (JSONException e) {
            Log.i("JSONException", e.getMessage());
            e.printStackTrace();
        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return searchResult;
    }
}
