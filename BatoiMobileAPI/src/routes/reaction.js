const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

const SQL_LIKE = 'INSERT INTO reaction SET iduser = ?, idreply = ?;';
const SQL_DISLIKE = 'DELETE FROM reaction WHERE iduser = ? && idreply = ?;';
const SQL_UPDATE_LIKE = 'UPDATE reply SET likes = (likes + 1) WHERE idreply = ?;';
const SQL_UPDATE_DISLIKE = 'UPDATE reply SET likes = (likes - 1) WHERE idreply = ?';

router.get('/reaction/', (req, res) => {
    mysqlConnection.query('SELECT * FROM reaction', (err, rows, fields) => {
        if(!err) res.json({'response': true, 'reactions': rows});
        else console.log(err);
    });
});

router.get('/reaction/:user/:reply', (req, res) => {
    const { user, reply } = req.params;
    
    mysqlConnection.query('SELECT * FROM reaction WHERE iduser = ? && idreply = ?', [user, reply], (err, rows, fields) => {
        if(!err) res.json({'response': true, 'reactions': rows});
        else console.log(err);
    });
});

router.post('/reaction/:user/:reply', (req, res) => {
    const { user, reply } = req.params;

    mysqlConnection.query(SQL_LIKE, [user, reply, reply], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) {
                var value = rows.insertId
                mysqlConnection.query(SQL_UPDATE_LIKE, [reply],(err, rows, field) => {
                    if(!err) {
                        if(rows.affectedRows > 0) res.json({"response": true, 'id': value});
                        else res.json({'response': false, 'id': -1});
                    }
                    else console.log(err);
                });
            }
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.delete('/reaction/:user/:reply', (req, res) => {
    const { user, reply } = req.params;
    
    mysqlConnection.query(SQL_DISLIKE, [user, reply],(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) {
                mysqlConnection.query(SQL_UPDATE_DISLIKE, [reply],(err, rows, field) => {
                    if(!err) {
                        if(rows.affectedRows > 0) res.json({"response": true});
                        else res.json({'response': false});
                    }
                    else console.log(err);
                });
            }
            else res.json({'response': false});
        }
        else console.log(err);
    });
});

module.exports = router;