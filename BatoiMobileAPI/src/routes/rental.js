const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

const SQL_GET_RENTALS = 'SELECT user.*, rental.*, address.idaddress, address.name as address_name, address.lat, address.lng FROM user INNER JOIN rental ON user.iduser = rental.author INNER JOIN address ON rental.address = address.idaddress GROUP BY rental.idrental';
const SQL_GET_RENTALS_BYID = 'SELECT user.*, rental.*, address.idaddress, address.name as address_name, address.lat, address.lng FROM user INNER JOIN rental ON user.iduser = rental.author INNER JOIN address ON rental.address = address.idaddress WHERE user.iduser = ? WHERE rental.idrental = ? GROUP BY rental.idrental';
const SQL_GET_RENTALS_BYAUTHOR = 'SELECT user.*, rental.*, address.idaddress, address.name as address_name, address.lat, address.lng FROM user INNER JOIN rental ON user.iduser = rental.author INNER JOIN address ON rental.address = address.idaddress WHERE user.iduser = ? GROUP BY rental.idrental';

router.get('/rental/', (req, res) => {
    mysqlConnection.query(SQL_GET_RENTALS, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'rentals': rows});
        else console.log(err);
    });
});

router.get('/rental/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query(SQL_GET_RENTALS_BYID, id, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'rentals': rows});
        else console.log(err);
    });
});

router.get('/rental/getByAuthor/:author', (req, res) => {
    const { author } = req.params;
    
    mysqlConnection.query(SQL_GET_RENTALS_BYAUTHOR, author, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'rentals': rows});
        else console.log(err);
    });
});

router.put('/rental/:description/:price/:address/:id', (req, res) => {
    const { description, price, address, id } = req.params

    mysqlConnection.query('UPDATE rental SET description = ?, price = ?, address = ? WHERE idrental = ?', [description, price, address, id], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.post('/rental/:description/:price/:author/:address', (req, res) => {
    const { description, price, author, address } = req.params

    mysqlConnection.query('INSERT INTO rental SET description = ?, price = ?, author = ?, address = ?', [description, price, author, address], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true, 'id': rows.insertId});
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.delete('/rental/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('DELETE FROM rental WHERE idrental = ?', id,(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) res.json({"response": true});
            else res.json({'response': false});
        }
        else console.log(err);
    });
});


module.exports = router;