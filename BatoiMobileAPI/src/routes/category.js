const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

router.get('/category/', (req, res) => {
    mysqlConnection.query('SELECT * FROM category', (err, rows, fields) => {
        if(!err) res.json({'response': true, 'categories': rows});
        else console.log(err);
    });
});

router.get('/category/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('SELECT * FROM category WHERE idcategory = ?', id, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'categories': rows});
        else console.log(err);
    });
});

router.post('/category/', (req, res) => {
    console.log(req.body);

    mysqlConnection.query('INSERT INTO category SET ?', req.body, (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true, 'id': rows.insertId});
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.delete('/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('DELETE FROM category WHERE idcategory = ?', id,(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) res.json({"response": true});
            else res.json({'response': false});
        }
        else console.log(err);
    });
});


module.exports = router;