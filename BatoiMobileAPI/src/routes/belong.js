const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

router.get('/belong/', (req, res) => {
    mysqlConnection.query('SELECT * FROM belong', (err, rows, fields) => {
        if(!err) res.json({'response': true, 'belonges': rows});
        else console.log(err);
    });
});

router.get('/belong/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('SELECT * FROM belong WHERE idbelong = ?', id, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'belonges': rows});
        else console.log(err);
    });
});

router.get('/belong/getNotifications/:group', (req, res) => {
    const { group } = req.params;
    
    mysqlConnection.query('SELECT * FROM belong WHERE idgroup = ? AND confirmed = FALSE', group, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'belonges': rows});
        else console.log(err);
    });
});

router.get('/belong/getIfPetitionSent/:group/:user', (req, res) => {
    const { group, user } = req.params;
    
    mysqlConnection.query('SELECT * FROM belong WHERE idgroup = ? AND iduser = ? AND confirmed = FALSE', [group, user], (err, rows, fields) => {
        if(!err) res.json({'response': true, 'belonges': rows});
        else console.log(err);
    });
});

router.get('/belong/:group/:user', (req, res) => {
    const { group, user } = req.params;
    
    mysqlConnection.query('SELECT * FROM belong WHERE idgroup = ? AND iduser = ? AND confirmed = TRUE', [group, user], (err, rows, fields) => {
        if(!err) res.json({'response': true, 'belonges': rows});
        else console.log(err);
    });
});

router.put('/belong/:group/:user/:confirmed', (req, res) => {
    const { group, user, confirmed } = req.params

    mysqlConnection.query('UPDATE belong SET confirmed = ? WHERE idgroup= ? && iduser = ?', [confirmed, group, user], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.post('/belong/:group/:admin', (req, res) => {
    const { group, admin } = req.params

    mysqlConnection.query('INSERT INTO belong SET idgroup = ?, iduser = ?', [group, admin], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true, 'id': rows.insertId});
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.post('/belong/:group/:admin/:confirmed', (req, res) => {
    const { group, admin, confirmed } = req.params

    mysqlConnection.query('INSERT INTO belong SET idgroup = ?, iduser = ?, confirmed = ?', [group, admin, confirmed], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true, 'id': rows.insertId});
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.delete('/belong/:group/:user', (req, res) => {
    const { group, user } = req.params;
    
    mysqlConnection.query('DELETE FROM belong WHERE idgroup = ? && iduser = ?', [group, user],(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) res.json({"response": true});
            else res.json({'response': false});
        }
        else console.log(err);
    });
});


module.exports = router;