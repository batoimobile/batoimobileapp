const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');


const SQL_GET_ROUTES = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.to = t.idaddress INNER JOIN user ON route.author = user.iduser GROUP BY idroute';
const SQL_GET_ROUTES_LOW_TO_HIGH = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.from = f.idaddress INNER JOIN user ON route.author = user.iduser GROUP BY idroute ORDER BY price DESC';
const SQL_GET_ROUTES_HIGH_TO_LOW = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.from = f.idaddress INNER JOIN user ON route.author = user.iduser GROUP BY idroute ORDER BY price ASC';
const SQL_GET_ROUTES_BYID = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.from = f.idaddress INNER JOIN user ON route.author = user.iduser WHERE route.idroute = ? GROUP BY idroute';
const SQL_GET_ROUTES_BYAUTHOR = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.from = f.idaddress INNER JOIN user ON route.author = user.iduser WHERE route.author = ? GROUP BY idroute';
const SQL_GET_ROUTES_ALL_FILTERS = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.from = f.idaddress INNER JOIN user ON route.author = user.iduser WHERE f.name = ? && t.name= ? && route.date = ? GROUP BY idroute';
const SQL_GET_ROUTES_BYDATE = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.from = f.idaddress INNER JOIN user ON route.author = user.iduser WHERE route.date = ? GROUP BY idroute';
const SQL_GET_ROUTES_BYFROM = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.from = f.idaddress INNER JOIN user ON route.author = user.iduser WHERE f.name = ? GROUP BY idroute';
const SQL_GET_ROUTES_BYTO = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.from = f.idaddress INNER JOIN user ON route.author = user.iduser WHERE t.name = ? GROUP BY idroute';
const SQL_GET_ROUTES_BY_FROM_AND_TO = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.from = f.idaddress INNER JOIN user ON route.author = user.iduser WHERE f.name = ? && t.name = ? GROUP BY idroute';
const SQL_GET_ROUTES_BY_FROM_AND_DATE = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.from = f.idaddress INNER JOIN user ON route.author = user.iduser WHERE f.name = ? && route.date = ? GROUP BY idroute';
const SQL_GET_ROUTES_BY_TO_AND_DATE = 'SELECT route.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN route ON f.idaddress = route.from INNER JOIN address t ON route.from = f.idaddress INNER JOIN user ON route.author = user.iduser WHERE t.name = ? && route.date = ? GROUP BY idroute';

router.get('/route/', (req, res) => {
    mysqlConnection.query(SQL_GET_ROUTES, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'routes': rows});
        else console.log(err);
    });
});

router.get('/route/lowToHigh/', (req, res) => {
    mysqlConnection.query(SQL_GET_ROUTES_LOW_TO_HIGH, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'routes': rows});
        else console.log(err);
    });
});

router.get('/route/highToLow/', (req, res) => {
    mysqlConnection.query(SQL_GET_ROUTES_HIGH_TO_LOW, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'routes': rows});
        else console.log(err);
    });
});

router.get('/route/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query(SQL_GET_ROUTES_BYID, id, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'routes': rows});
        else console.log(err);
    });
});

router.get('/route/:author', (req, res) => {
    const { author } = req.params;
    
    mysqlConnection.query(SQL_GET_ROUTES_BYAUTHOR, author, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'routes': rows});
        else console.log(err);
    });
});

router.get('/route/getByFromAndTo/:from/:to', (req, res) => {
    const { from, to } = req.params;

    mysqlConnection.query(SQL_GET_ROUTES_BY_FROM_AND_TO, [from, to], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                res.json({'response': true, 'routes': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/route/getByFromAndDate/:from/:date', (req, res) => {
    const { from, date } = req.params;

    mysqlConnection.query(SQL_GET_ROUTES_BY_FROM_AND_DATE, [from, date], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                res.json({'response': true, 'routes': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/route/getByToAndDate/:to/:date', (req, res) => {
    const { to, date } = req.params;

    mysqlConnection.query(SQL_GET_ROUTES_BY_TO_AND_DATE, [to, date], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                res.json({'response': true, 'routes': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/route/getByFilters/:from/:to/:date', (req, res) => {
    const { from, to, date } = req.params;

    mysqlConnection.query(SQL_GET_ROUTES_ALL_FILTERS, [from, to, date], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                res.json({'response': true, 'routes': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/route/getByFrom/:from', (req, res) => {
    const { from } = req.params;

    mysqlConnection.query(SQL_GET_ROUTES_BYFROM, [from], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                res.json({'response': true, 'routes': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/route/getByTo/:to', (req, res) => {
    const { to } = req.params;

    mysqlConnection.query(SQL_GET_ROUTES_BYTO, [to], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                res.json({'response': true, 'routes': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/route/getByDate/:date', (req, res) => {
    const { date } = req.params;

    mysqlConnection.query(SQL_GET_ROUTES_BYDATE, [date], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                res.json({'response': true, 'routes': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.put('/route/:from/:to/:date/:price/:seats/:observations/:id', (req, res) => {
    const { from, to, date, price, seats, observations, id } = req.params

    mysqlConnection.query('UPDATE route SET route.from = ?, route.to = ?, route.date = ?, price = ?, seats = ?, observations = ? WHERE idroute = ?', [from, to, date, price, seats, observations, id], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.post('/route/:from/:to/:date/:price/:seats/:observations/:author', (req, res) => {
    const { from, to, date, price, seats, observations, author } = req.params

    mysqlConnection.query('INSERT INTO route SET route.from = ?, route.to = ?, route.date = ?, price = ?, seats = ?, observations = ?, author = ?', [from, to, date, price, seats, observations, author], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true, 'id': rows.insertId});
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.delete('/route/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('DELETE FROM route WHERE idroute = ?', id,(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) res.json({"response": true});
            else res.json({'response': false});
        }
        else console.log(err);
    });
});


module.exports = router;