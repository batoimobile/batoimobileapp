const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

const SQL_REPLIES_FROM_POST = 'SELECT reply.*, user.* FROM reply INNER JOIN user ON reply.author = user.iduser WHERE post = ? ORDER BY correct DESC'
const SQL_FIND_CORRECT = 'SELECT reply.*, user.* FROM reply INNER JOIN user ON reply.author = user.iduser WHERE post = ? && correct = 1 && idreply != ?'

router.get('/reply/', (req, res) => {
    mysqlConnection.query('SELECT * FROM reply', (err, rows, fields) => {
        if(!err) res.json({'response': true, 'replies': rows});
        else console.log(err);
    });
});

router.get('/reply/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('SELECT * FROM reply WHERE idreply = ?', id, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'replies': rows});
        else console.log(err);
    });
});

router.get('/reply/getByPost/:post', (req, res) => {
    const { post } = req.params;
    
    mysqlConnection.query(SQL_REPLIES_FROM_POST, post, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'replies': rows});
        else console.log(err);
    });
});

router.get('/reply/findByPost/:idpost', (req, res) => {
    const { idpost } = req.params;

    mysqlConnection.query('SELECT * FROM reply WHERE post = ?', idpost, (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) res.json({'response': true, 'replies': rows});
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/reply/findCorrect/:post/:reply', (req, res) => {
    const { post, reply } = req.params;

    mysqlConnection.query(SQL_FIND_CORRECT, [post, reply], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) res.json({'response': true, 'replies': rows});
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.post('/reply/:response/:author/:post', (req, res) => {
    const { response, author, post } = req.params

    mysqlConnection.query('INSERT INTO reply SET response = ?, author = ?, post = ?', [response, author, post], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true, 'id': rows.insertId});
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.put('/reply/:isCorrect/:reply', (req, res) => {
    const { isCorrect, reply } = req.params;

    mysqlConnection.query('UPDATE reply SET correct = ? WHERE idreply = ?', [isCorrect, reply], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.put('/reply/edit/:response/:reply', (req, res) => {
    const { response, reply } = req.params;

    mysqlConnection.query('UPDATE reply SET response = ? WHERE idreply = ?', [response, reply], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.delete('/reply/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('DELETE FROM reply WHERE idreply = ?', id,(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) res.json({"response": true});
            else res.json({'response': false});
        }
        else console.log(err);
    });
});


module.exports = router;