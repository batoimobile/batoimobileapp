const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');


const SQL_GET_GROUPS = 'SELECT g.*, user.iduser, user.name as u_name, user.nickname, user.password, user.email, user.role, f.idaddress as idfrom, f.name as f_name, f.lat as f_lat, f.lng as f_lng, t.idaddress as idto, t.name as t_name, t.lat as t_lat, t.lng as t_lng, (SELECT COUNT(*) FROM belong WHERE idgroup = g.idgroup AND confirmed = TRUE) as members FROM batoimobilebd.group g INNER JOIN user ON g.admin = user.iduser INNER JOIN address f ON f.idaddress = g.from INNER JOIN address t ON t.idaddress = g.to GROUP BY g.idgroup';
const SQL_GET_GROUPS_LOW_TO_HIGH = 'SELECT g.*, user.iduser, user.name as u_name, user.nickname, user.password, user.email, user.role, f.idaddress as idfrom, f.name as f_name, f.lat as f_lat, f.lng as f_lng, t.idaddress as idto, t.name as t_name, t.lat as t_lat, t.lng as t_lng, (SELECT COUNT(*) FROM belong WHERE idgroup = g.idgroup AND confirmed = TRUE) as members FROM batoimobilebd.group g INNER JOIN user ON g.admin = user.iduser INNER JOIN address f ON f.idaddress = g.from INNER JOIN address t ON t.idaddress = g.to GROUP BY g.idgroup ORDER BY price DESC';
const SQL_GET_GROUPS_HIGH_TO_LOW = 'SELECT g.*, user.iduser, user.name as u_name, user.nickname, user.password, user.email, user.role, f.idaddress as idfrom, f.name as f_name, f.lat as f_lat, f.lng as f_lng, t.idaddress as idto, t.name as t_name, t.lat as t_lat, t.lng as t_lng, (SELECT COUNT(*) FROM belong WHERE idgroup = g.idgroup AND confirmed = TRUE) as members FROM batoimobilebd.group g INNER JOIN user ON g.admin = user.iduser INNER JOIN address f ON f.idaddress = g.from INNER JOIN address t ON t.idaddress = g.to GROUP BY g.idgroup ORDER BY price ASC';
const SQL_GET_GROUPS_BYID = 'SELECT g.*, user.iduser, user.name as u_name, user.nickname, user.password, user.email, user.role, f.idaddress as idfrom, f.name as f_name, f.lat as f_lat, f.lng as f_lng, t.idaddress as idto, t.name as t_name, t.lat as t_lat, t.lng as t_lng, (SELECT COUNT(*) FROM belong WHERE idgroup = g.idgroup AND confirmed = TRUE) as members FROM batoimobilebd.group g INNER JOIN user ON g.admin = user.iduser INNER JOIN address f ON f.idaddress = g.from INNER JOIN address t ON t.idaddress = g.to WHERE g.idgroup = ? GROUP BY g.idgroup';
const SQL_GET_GROUPS_BYAUTHOR = 'SELECT g.*, user.iduser, user.name as u_name, user.nickname, user.password, user.email, user.role, f.idaddress as idfrom, f.name as f_name, f.lat as f_lat, f.lng as f_lng, t.idaddress as idto, t.name as t_name, t.lat as t_lat, t.lng as t_lng, (SELECT COUNT(*) FROM belong WHERE idgroup = g.idgroup AND confirmed = TRUE) as members FROM batoimobilebd.group g INNER JOIN user ON g.admin = user.iduser INNER JOIN address f ON f.idaddress = g.from INNER JOIN address t ON t.idaddress = g.to WHERE g.admin = ? GROUP BY g.idgroup';

router.get('/group/', (req, res) => {
    mysqlConnection.query(SQL_GET_GROUPS, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'groups': rows});
        else console.log(err);
    });
});

router.get('/group/lowToHigh/', (req, res) => {
    mysqlConnection.query(SQL_GET_GROUPS_LOW_TO_HIGH, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'groups': rows});
        else console.log(err);
    });
});

router.get('/group/highToLow/', (req, res) => {
    mysqlConnection.query(SQL_GET_GROUPS_HIGH_TO_LOW, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'groups': rows});
        else console.log(err);
    });
});

router.get('/group/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query(SQL_GET_GROUPS_BYID, id, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'groups': rows});
        else console.log(err);
    });
});

router.get('/group/getByAuthor/:author', (req, res) => {
    const { author } = req.params;
    
    mysqlConnection.query(SQL_GET_GROUPS_BYAUTHOR, author, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'groups': rows});
        else console.log(err);
    });
});

router.put('/group/:name/:from/:to/:days/:price/:shift/:observations/:id', (req, res) => {
    const { name, from, to, days, price, shift, observations, id } = req.params

    mysqlConnection.query('UPDATE batoimobilebd.group SET name = ?, batoimobilebd.group.from = ?, batoimobilebd.group.to = ?, days = ?, price = ?, shift = ?, observations = ? WHERE idgroup = ?', [name, from, to, days, price, shift, observations, id], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.post('/group/:name/:from/:to/:days/:price/:shift/:observations/:admin', (req, res) => {
    const { name, from, to, days, price, shift, observations, admin } = req.params

    mysqlConnection.query('INSERT INTO batoimobilebd.group SET name = ?, batoimobilebd.group.from = ?, batoimobilebd.group.to = ?, days = ?, price = ?, shift = ?, observations = ?, batoimobilebd.group.admin = ?', [name, from, to, days, price, shift, observations, admin], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true, 'id': rows.insertId});
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.delete('/group/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('DELETE FROM batoimobilebd.group WHERE idgroup = ?', id,(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) res.json({"response": true});
            else res.json({'response': false});
        }
        else console.log(err);
    });
});


module.exports = router;