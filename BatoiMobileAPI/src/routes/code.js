const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

router.get('/code/', (req, res) => {
    mysqlConnection.query('SELECT * FROM code', (err, rows, fields) => {
        if(!err) res.json({'response': true, 'codes': rows});
        else console.log(err);
    });
});

router.get('/code/:code', (req, res) => {
    const { code } = req.params;

    mysqlConnection.query('SELECT * FROM code WHERE code = ?', code, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'codes': rows});
        else console.log(err);
    });
});

router.delete('/code/:code', (req, res) => {
    const { code } = req.params;
    
    mysqlConnection.query('DELETE FROM code WHERE code = ?', code,(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) res.json({"response": true});
            else res.json({'response': false});
        }
        else console.log(err);
    });
});


module.exports = router;