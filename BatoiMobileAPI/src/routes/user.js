const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

router.get('/user/', (req, res) => {
    mysqlConnection.query('SELECT * FROM user', (err, rows, fields) => {
        if(!err) res.json({'response': true, 'users': rows});
        else console.log(err);
    });
});

router.get('/user/:id', (req, res) => {
    const { id } = req.params;

    mysqlConnection.query('SELECT * FROM user WHERE iduser = ?', id, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'users': rows});
        else console.log(err);
    });
});

router.get('/user/nickname/:nickname', (req, res) => {
    const { nickname } = req.params;

    mysqlConnection.query('SELECT * FROM user WHERE nickname = ?', nickname, (err, rows, fields) => {
        if(!err) {
            res.json({'response': true, 'users': rows});
            
        } else console.log(err);
    });
});

router.get('/user/getUsersPetitions/:group', (req, res) => {
    const { group } = req.params;

    mysqlConnection.query('SELECT * FROM user INNER JOIN belong ON user.iduser = belong.iduser WHERE belong.confirmed = FALSE && belong.idgroup = ?', group, (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) res.json({'response': true, 'users': rows});
            else res.json({'response': false, 'users': null});
        } else console.log(err);
    });
});

router.get('/user/getUsersOfGroup/:group/:admin', (req, res) => {
    const { group, admin } = req.params;
    
    mysqlConnection.query('SELECT * FROM user INNER JOIN belong ON user.iduser = belong.iduser WHERE belong.confirmed = TRUE && belong.idgroup = ? && user.iduser != ?', [group, admin], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) res.json({'response': true, 'users': rows});
            else res.json({'response': false, 'users': null});
        } else console.log(err);
    });
});

router.get('/user/:nickname/:password', (req, res) => {
    const { nickname, password } = req.params;

    mysqlConnection.query('SELECT * FROM user WHERE nickname = ? && password = (SELECT (SHA1(?)))', [nickname, password], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) res.json({'response': true, 'users': rows});
            else res.json({'response': false, 'users': null});
        } else console.log(err);
    });
});

router.put('/user/:name/:nickname/:email/:password/:id', (req, res) => {
    const { name, nickname, email, password, id } = req.params

    mysqlConnection.query('UPDATE user SET name = ?, nickname = ?, email = ?, password = (SELECT (SHA1(?))) WHERE iduser = ?', [name, nickname, email, password, id], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.put('/user/:name/:nickname/:email/:id', (req, res) => {
    const { name, nickname, email, password, id } = req.params

    mysqlConnection.query('UPDATE user SET name = ?, nickname = ?, email = ? WHERE iduser = ?', [name, nickname, email, id], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.put('/user/:password/:id', (req, res) => {
    const { password, id } = req.params

    mysqlConnection.query('UPDATE user SET password = (SELECT (SHA1(?))) WHERE iduser = ?', [password, id], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.post('/user/:name/:nickname/:email/:password', (req, res) => {
    const { name, nickname, email, password } = req.params

    mysqlConnection.query('INSERT INTO user SET name = ?, nickname = ?, email = ?, password = (SELECT (SHA1(?)))', [name, nickname, email, password], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true, 'id': rows.insertId});
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.delete('/user/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('DELETE FROM user WHERE iduser = ?', id,(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) res.json({"response": true});
            else res.json({'response': false});
        }
        else console.log(err);
    });
});


module.exports = router;