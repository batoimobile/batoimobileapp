const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');


const SQL_GET_PRODUCTS = 'SELECT product.*, category.idcategory, category.name as c_name, user.iduser, user.name as u_name, user.nickname, user.password, user.email, user.role FROM product INNER JOIN category ON product.category = category.idcategory INNER JOIN user ON product.author = user.iduser GROUP BY product.idproduct';
const SQL_GET_PRODUCTS_LOW_TO_HIGH = 'SELECT product.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN product ON f.idaddress = product.from INNER JOIN address t ON product.from = f.idaddress INNER JOIN user ON product.author = user.iduser GROUP BY idproduct ORDER BY price DESC';
const SQL_GET_PRODUCTS_HIGH_TO_LOW = 'SELECT product.*, f.idaddress as idaddress_f, f.name as name_f, f.lat as lat_f, f.lng as lng_f, t.idaddress as idaddress_t, t.name as name_t, t.lat as lat_t, t.lng as lng_t, user.* FROM address f INNER JOIN product ON f.idaddress = product.from INNER JOIN address t ON product.from = f.idaddress INNER JOIN user ON product.author = user.iduser GROUP BY idproduct ORDER BY price ASC';
const SQL_GET_PRODUCTS_BYID = 'SELECT product.*, category.idcategory, category.name as c_name, user.iduser, user.name as u_name, user.nickname, user.password, user.email, user.role FROM product INNER JOIN category ON product.category = category.idcategory INNER JOIN user ON product.author = user.iduser WHERE product.idproduct = ? GROUP BY product.idproduct';
const SQL_GET_PRODUCTS_BYAUTHOR = 'SELECT product.*, category.idcategory, category.name as c_name, user.iduser, user.name as u_name, user.nickname, user.password, user.email, user.role FROM product INNER JOIN category ON product.category = category.idcategory INNER JOIN user ON product.author = user.iduser WHERE product.author = ? GROUP BY product.idproduct';
const SQL_GET_PRODUCTS_BYNAME = 'SELECT product.*, category.idcategory, category.name as c_name, user.iduser, user.name as u_name, user.nickname, user.password, user.email, user.role FROM product INNER JOIN category ON product.category = category.idcategory INNER JOIN user ON product.author = user.iduser WHERE product.name LIKE ? GROUP BY product.idproduct';
const SQL_GET_PRODUCTS_BYCATEGORY = 'SELECT product.*, category.idcategory, category.name as c_name, user.iduser, user.name as u_name, user.nickname, user.password, user.email, user.role FROM product INNER JOIN category ON product.category = category.idcategory INNER JOIN user ON product.author = user.iduser WHERE product.category = ? GROUP BY product.idproduct';
const SQL_GET_PRODUCTS_BYCATEGORY_AND_NAME = 'SELECT product.*, category.idcategory, category.name as c_name, user.iduser, user.name as u_name, user.nickname, user.password, user.email, user.role FROM product INNER JOIN category ON product.category = category.idcategory INNER JOIN user ON product.author = user.iduser WHERE product.category = ? && product.name LIKE ? GROUP BY product.idproduct';

router.get('/product/', (req, res) => {
    mysqlConnection.query(SQL_GET_PRODUCTS, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'products': rows});
        else console.log(err);
    });
});

router.get('/product/lowToHigh/', (req, res) => {
    mysqlConnection.query(SQL_GET_PRODUCTS_LOW_TO_HIGH, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'products': rows});
        else console.log(err);
    });
});

router.get('/product/highToLow/', (req, res) => {
    mysqlConnection.query(SQL_GET_PRODUCTS_HIGH_TO_LOW, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'products': rows});
        else console.log(err);
    });
});

router.get('/product/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query(SQL_GET_PRODUCTS_BYID, id, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'products': rows});
        else console.log(err);
    });
});

router.get('/product/getByAuthor/:author', (req, res) => {
    const { author } = req.params;
    
    mysqlConnection.query(SQL_GET_PRODUCTS_BYAUTHOR, author, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'products': rows});
        else console.log(err);
    });
});

router.get('/product/getByName/:name', (req, res) => {
    let { name } = req.params;
    name = '%'+name+'%';w
    
    mysqlConnection.query(SQL_GET_PRODUCTS_BYNAME, name, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'products': rows});
        else console.log(err);
    });
});

router.get('/product/getByCategory/:category', (req, res) => {
    const { category } = req.params;
    
    mysqlConnection.query(SQL_GET_PRODUCTS_BYCATEGORY, category, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'products': rows});
        else console.log(err);
    });
});

router.get('/product/getByNameAndCategory/:name/:category', (req, res) => {
    let { name, category } = req.params;
    name = '%'+name+'%';
    
    mysqlConnection.query(SQL_GET_PRODUCTS_BYCATEGORY_AND_NAME, [category, name], (err, rows, fields) => {
        if(!err) res.json({'response': true, 'products': rows});
        else console.log(err);
    });
});

router.put('/product/:name/:price/:description/:category/:id', (req, res) => {
    const { name, price, description, category, id } = req.params

    mysqlConnection.query('UPDATE product SET name = ?, price = ?, description = ?, category = ? WHERE idproduct = ?', [name, price, description, category, id], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.post('/product/:name/:price/:description/:category/:author', (req, res) => {
    const { name, price, description, category, author } = req.params

    mysqlConnection.query('INSERT INTO product SET name = ?, price = ?, description = ?, category = ?, author = ?', [name, price, description, category, author], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true, 'id': rows.insertId});
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.delete('/product/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('DELETE FROM product WHERE idproduct = ?', id,(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) res.json({"response": true});
            else res.json({'response': false});
        }
        else console.log(err);
    });
});


module.exports = router;