const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

const ALL_POST = 'SELECT post.*, user.iduser, user.name as user_name, user.nickname, user.password, user.email, user.role, category.idcategory, category.name as category_name, IFNULL((SELECT count(*) from reply where reply.post = post.idpost group by reply.post), 0) as replies FROM user INNER JOIN post ON user.iduser = post.author INNER JOIN category ON post.category = category.idcategory GROUP BY post.idpost';
const GET_BY_AUTHOR = 'SELECT post.*, user.iduser, user.name as user_name, user.nickname, user.password, user.email, user.role, category.idcategory, category.name as category_name, IFNULL((SELECT count(*) from reply where reply.post = post.idpost group by reply.post), 0) as replies FROM user INNER JOIN post ON user.iduser = post.author INNER JOIN category ON post.category = category.idcategory WHERE author = ? GROUP BY post.idpost'
const GET_BY_SUBJECT_AND_CATEGORY = 'SELECT post.*, user.iduser, user.name as user_name, user.nickname, user.password, user.email, user.role, category.idcategory, category.name as category_name, IFNULL((SELECT count(*) from reply where reply.post = post.idpost group by reply.post), 0) as replies FROM user INNER JOIN post ON user.iduser = post.author INNER JOIN category ON post.category = category.idcategory WHERE subject LIKE ? AND category = ? GROUP BY post.idpost'
const GET_BY_SUBJECT = 'SELECT post.*, user.iduser, user.name as user_name, user.nickname, user.password, user.email, user.role, category.idcategory, category.name as category_name, IFNULL((SELECT count(*) from reply where reply.post = post.idpost group by reply.post), 0) as replies FROM user INNER JOIN post ON user.iduser = post.author INNER JOIN category ON post.category = category.idcategory WHERE subject LIKE ? GROUP BY post.idpost'
const GET_BY_CATEGORY = 'SELECT post.*, user.iduser, user.name as user_name, user.nickname, user.password, user.email, user.role, category.idcategory, category.name as category_name, IFNULL((SELECT count(*) from reply where reply.post = post.idpost group by reply.post), 0) as replies FROM user INNER JOIN post ON user.iduser = post.author INNER JOIN category ON post.category = category.idcategory WHERE category = ? GROUP BY post.idpost'
const GET_BY_RESOLVED = 'SELECT post.*, user.iduser, user.name as user_name, user.nickname, user.password, user.email, user.role, category.idcategory, category.name as category_name, IFNULL((SELECT count(*) from reply where reply.post = post.idpost group by reply.post), 0) as replies FROM user INNER JOIN post ON user.iduser = post.author INNER JOIN category ON post.category = category.idcategory WHERE resolved = ? GROUP BY post.idpost'
const GET_BY_CATEGORY_AND_RESOLVED = 'SELECT post.*, user.iduser, user.name as user_name, user.nickname, user.password, user.email, user.role, category.idcategory, category.name as category_name, IFNULL((SELECT count(*) from reply where reply.post = post.idpost group by reply.post), 0) as replies FROM user INNER JOIN post ON user.iduser = post.author INNER JOIN category ON post.category = category.idcategory WHERE category = ? && resolved = ? GROUP BY post.idpost'
const GET_BY_SUBJECT_AND_RESOLVED = 'SELECT post.*, user.iduser, user.name as user_name, user.nickname, user.password, user.email, user.role, category.idcategory, category.name as category_name, IFNULL((SELECT count(*) from reply where reply.post = post.idpost group by reply.post), 0) as replies FROM user INNER JOIN post ON user.iduser = post.author INNER JOIN category ON post.category = category.idcategory WHERE subject LIKE ? && resolved = ? GROUP BY post.idpost'
const GET_BY_FILTERS = 'SELECT post.*, user.iduser, user.name as user_name, user.nickname, user.password, user.email, user.role, category.idcategory, category.name as category_name, IFNULL((SELECT count(*) from reply where reply.post = post.idpost group by reply.post), 0) as replies FROM user INNER JOIN post ON user.iduser = post.author INNER JOIN category ON post.category = category.idcategory WHERE category = ? && resolved = ? && subject LIKE ? GROUP BY post.idpost'

router.get('/post/', (req, res) => {
    mysqlConnection.query(ALL_POST, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'posts': rows});
        else console.log(err);
    });
});

router.get('/post/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('SELECT * FROM post WHERE idpost = ?', id, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'posts': {user}});
        else console.log(err);
    });
});

router.get('/post/getByAuthor/:author', (req, res) => {
    const { author } = req.params;
    
    mysqlConnection.query(GET_BY_AUTHOR, author, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'posts': rows});
        else console.log(err);
    });
});

router.get('/post/category/:category', (req, res) => {
    const { category } = req.params;

    mysqlConnection.query('SELECT * FROM post WHERE category = ?', category, (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                res.json({'response': true, 'posts': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

///////////////

router.get('/post/getBySubjectAndCategory/:subject/:category', (req, res) => {
    let { subject, category } = req.params;
    subject = '%'+subject+'%';

    mysqlConnection.query(GET_BY_SUBJECT_AND_CATEGORY, [subject, category], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                res.json({'response': true, 'posts': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/post/getBySubjectAndResolved/:subject/:resolved', (req, res) => {
    let { subject, resolved } = req.params;
    subject = '%'+subject+'%';

    mysqlConnection.query(GET_BY_SUBJECT_AND_RESOLVED, [subject, resolved], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                console.log(rows.length);
                res.json({'response': true, 'posts': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/post/getByCategoryAndResolved/:category/:resolved', (req, res) => {
    const { category, resolved} = req.params;

    mysqlConnection.query(GET_BY_CATEGORY_AND_RESOLVED, [category, resolved], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                console.log(rows.length);
                res.json({'response': true, 'posts': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/post/getByFilters/:subject/:category/:resolved', (req, res) => {
    let { subject, category, resolved} = req.params;
    subject = '%'+subject+'%';

    mysqlConnection.query(GET_BY_FILTERS, [subject, category, resolved], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                console.log(rows.length);
                res.json({'response': true, 'posts': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/post/getBySubject/:subject', (req, res) => {
    let { subject } = req.params;
    subject = '%'+subject+'%';

    mysqlConnection.query(GET_BY_SUBJECT, [subject], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                console.log(rows.length);
                res.json({'response': true, 'posts': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/post/getByCategory/:category', (req, res) => {
    const { category } = req.params;

    mysqlConnection.query(GET_BY_CATEGORY, [category], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                console.log(rows.length);
                res.json({'response': true, 'posts': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

router.get('/post/getByResolved/:resolved', (req, res) => {
    const { resolved } = req.params;

    mysqlConnection.query(GET_BY_RESOLVED, [resolved], (err, rows, fields) => {
        if(!err) {
            if (rows.length > 0) {
                console.log(rows.length);
                res.json({'response': true, 'posts': rows});
            }
            else res.json({'response': false});

        } else console.log(err);
    });
});

///////////////////////////

router.post('/post/:subject/:body/:author/:category', (req, res) => {
    const { subject, body, author, category } = req.params;

    mysqlConnection.query('INSERT INTO post SET subject = ?, body = ? ,author = ?, category = ?', [subject, body, author, category], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true, 'id': rows.insertId});
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.put('/post/:isResolved/:post', (req, res) => {
    const { isResolved, post } = req.params;
    mysqlConnection.query('UPDATE post SET resolved = ? WHERE idpost = ?', [isResolved, post], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.put('/post/:subject/:body/:category/:post', (req, res) => {
    const { subject, body, category, post } = req.params;
    mysqlConnection.query('UPDATE post SET subject = ?, body = ?, category = ? WHERE idpost = ?', [subject, body, category, post], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.delete('/post/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('DELETE FROM post WHERE idpost = ?', id,(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) res.json({"response": true});
            else res.json({'response': false});
        }
        else console.log(err);
    });
});


module.exports = router;