const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

router.get('/address/', (req, res) => {
    mysqlConnection.query('SELECT * FROM address', (err, rows, fields) => {
        if(!err) res.json({'response': true, 'addresses': rows});
        else console.log(err);
    });
});

router.get('/address/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('SELECT * FROM address WHERE idaddress = ?', id, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'addresses': rows});
        else console.log(err);
    });
});

router.get('/address/getByName/:name', (req, res) => {
    const { name } = req.params;
    
    mysqlConnection.query('SELECT * FROM address WHERE name = ?', name, (err, rows, fields) => {
        if(!err) res.json({'response': true, 'addresses': rows});
        else console.log(err);
    });
});

router.put('/address/:name/:lat/:lng/:id', (req, res) => {
    const { name, lat, lng, id } = req.params

    mysqlConnection.query('UPDATE address SET name = ?, lat = ?, lng = ? WHERE idaddress = ?', [name, lat, lng, id], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true});
            else res.json({'response': false});

        } else {
            console.log(err);
            res.json({'response': false});
        }
    });
});

router.post('/address/:name/:lat/:lng', (req, res) => {
    const { name, lat, lng } = req.params

    mysqlConnection.query('INSERT INTO address SET name = ?, lat = ?, lng = ?', [name, lat, lng ], (err, rows, fields) => {
        if(!err) {
            if (res.statusCode == 200) res.json({'response': true, 'id': rows.insertId});
            else res.json({'response': false, 'id': -1});

        } else {
            console.log(err);
            res.json({'response': false, 'id': -1});
        }
    });
});

router.delete('/address/:id', (req, res) => {
    const { id } = req.params;
    
    mysqlConnection.query('DELETE FROM address WHERE idaddress = ?', id,(err, rows, field) => {
        if(!err) {
            if(rows.affectedRows > 0) res.json({"response": true});
            else res.json({'response': false});
        }
        else console.log(err);
    });
});

module.exports = router;