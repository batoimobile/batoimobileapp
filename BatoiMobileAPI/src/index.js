const express = require('express');
const app = express();

// Settings
app.set('port', process.env.port || 3000);

// Middlewares
app.use(express.json());

// Routes
app.use(require('./routes/user'));
app.use(require('./routes/post'));
app.use(require('./routes/category'));
app.use(require('./routes/reply'));
app.use(require('./routes/reaction'));
app.use(require('./routes/code'));
app.use(require('./routes/rental'));
app.use(require('./routes/address'));
app.use(require('./routes/route'));
app.use(require('./routes/product'));
app.use(require('./routes/group'));
app.use(require('./routes/belong'));

app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'))
});